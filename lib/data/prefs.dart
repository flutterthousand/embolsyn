import 'dart:convert';
import 'dart:developer';

import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Prefs {
  final SharedPreferences? sharedPreferences;
  Prefs({
    this.sharedPreferences,
  });
  Future<void> logOutUser() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future<void> saveToken(String token) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', token);
  }

  Future<String?> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? token = prefs.getString('access_token');
    return token;
  }

  Future<void> saveDeviceToken(String deviceToken) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('device_token', deviceToken);
  }

  Future<void> saveDeviceType({required bool deviceType}) async {
    final prefs = await SharedPreferences.getInstance();
    log('token from prefs saveDeviceType:: $deviceType');
    prefs.setBool('deviceType', deviceType);
  }

  Future<bool?> getDeviceType() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final bool? deviceType = prefs.getBool('deviceType');
    if (deviceType == null) {
      return null;
    }
    return deviceType;
  }

//  Future<String?> getDeviceToken() async {
//     final prefs = await SharedPreferences.getInstance();
//     final value = prefs.get('device_token');
//     if (value != null) {
//       log('device token:: $value');
//       return value as String;
//     }
//     return null;
//   }
  Future<void> saveCityId(int cityId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('city_id', cityId);
    await getCityId();
  }

  Future<int?> getCityId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int? cityId = prefs.getInt('city_id');
    return cityId;
  }

  // Future<void> saveNumber(int number) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.setInt('number', number);
  //   await getCityId();
  // }

  // Future<int?> getNumber() async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   final int? number = prefs.getInt('number');
  //   return number;
  // }

  Future<User?> getUserFromCacheNull() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();

    try {
      final user = sharedPreferences.get('user');
      if (user != null) {
        return User.fromJson(
          jsonDecode(user.toString()) as Map<String, dynamic>,
        );
      }
      return null;
    } catch (e) {
      log('AuthLocalDSImpl getUserFromCacheNull:: $e');
      throw ErrorException(message: 'В кэше нет запрашиваемые данные');
    }
  }

  Future<void> saveUserToCache({required User user}) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    sharedPreferences.setString('user', jsonEncode(user.toJson()));
  }

  Future<int?> getCountAdults() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int? countAdults = prefs.getInt('count_adults');

    return countAdults;
  }

  Future<int?> getCountChildren() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int? countChildren = prefs.getInt('count_children');

    return countChildren;
  }
}
