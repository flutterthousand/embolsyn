// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'users_dto.freezed.dart';
part 'users_dto.g.dart';

@unfreezed
class UsersAdultsDto with _$UsersAdultsDto {
  factory UsersAdultsDto({
    required String name,
    required String surname,
    @JsonKey(name: 'date_birth') required String dateBirth,
    required String gender,
    required String type,
  }) = _UserAdultsDto;

   factory UsersAdultsDto.fromJson(Map<String, dynamic> json) => _$UsersAdultsDtoFromJson(json);
}
