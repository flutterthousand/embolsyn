// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserAdultsDto _$$_UserAdultsDtoFromJson(Map<String, dynamic> json) =>
    _$_UserAdultsDto(
      name: json['name'] as String,
      surname: json['surname'] as String,
      dateBirth: json['date_birth'] as String,
      gender: json['gender'] as String,
      type: json['type'] as String,
    );

Map<String, dynamic> _$$_UserAdultsDtoToJson(_$_UserAdultsDto instance) =>
    <String, dynamic>{
      'name': instance.name,
      'surname': instance.surname,
      'date_birth': instance.dateBirth,
      'gender': instance.gender,
      'type': instance.type,
    };
