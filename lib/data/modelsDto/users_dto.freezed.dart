// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'users_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UsersAdultsDto _$UsersAdultsDtoFromJson(Map<String, dynamic> json) {
  return _UserAdultsDto.fromJson(json);
}

/// @nodoc
mixin _$UsersAdultsDto {
  String get name => throw _privateConstructorUsedError;
  set name(String value) => throw _privateConstructorUsedError;
  String get surname => throw _privateConstructorUsedError;
  set surname(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'date_birth')
  String get dateBirth => throw _privateConstructorUsedError;
  @JsonKey(name: 'date_birth')
  set dateBirth(String value) => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  set gender(String value) => throw _privateConstructorUsedError;
  String get type => throw _privateConstructorUsedError;
  set type(String value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UsersAdultsDtoCopyWith<UsersAdultsDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersAdultsDtoCopyWith<$Res> {
  factory $UsersAdultsDtoCopyWith(
          UsersAdultsDto value, $Res Function(UsersAdultsDto) then) =
      _$UsersAdultsDtoCopyWithImpl<$Res, UsersAdultsDto>;
  @useResult
  $Res call(
      {String name,
      String surname,
      @JsonKey(name: 'date_birth') String dateBirth,
      String gender,
      String type});
}

/// @nodoc
class _$UsersAdultsDtoCopyWithImpl<$Res, $Val extends UsersAdultsDto>
    implements $UsersAdultsDtoCopyWith<$Res> {
  _$UsersAdultsDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? surname = null,
    Object? dateBirth = null,
    Object? gender = null,
    Object? type = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      surname: null == surname
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String,
      dateBirth: null == dateBirth
          ? _value.dateBirth
          : dateBirth // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserAdultsDtoCopyWith<$Res>
    implements $UsersAdultsDtoCopyWith<$Res> {
  factory _$$_UserAdultsDtoCopyWith(
          _$_UserAdultsDto value, $Res Function(_$_UserAdultsDto) then) =
      __$$_UserAdultsDtoCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      String surname,
      @JsonKey(name: 'date_birth') String dateBirth,
      String gender,
      String type});
}

/// @nodoc
class __$$_UserAdultsDtoCopyWithImpl<$Res>
    extends _$UsersAdultsDtoCopyWithImpl<$Res, _$_UserAdultsDto>
    implements _$$_UserAdultsDtoCopyWith<$Res> {
  __$$_UserAdultsDtoCopyWithImpl(
      _$_UserAdultsDto _value, $Res Function(_$_UserAdultsDto) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? surname = null,
    Object? dateBirth = null,
    Object? gender = null,
    Object? type = null,
  }) {
    return _then(_$_UserAdultsDto(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      surname: null == surname
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String,
      dateBirth: null == dateBirth
          ? _value.dateBirth
          : dateBirth // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserAdultsDto implements _UserAdultsDto {
  _$_UserAdultsDto(
      {required this.name,
      required this.surname,
      @JsonKey(name: 'date_birth') required this.dateBirth,
      required this.gender,
      required this.type});

  factory _$_UserAdultsDto.fromJson(Map<String, dynamic> json) =>
      _$$_UserAdultsDtoFromJson(json);

  @override
  String name;
  @override
  String surname;
  @override
  @JsonKey(name: 'date_birth')
  String dateBirth;
  @override
  String gender;
  @override
  String type;

  @override
  String toString() {
    return 'UsersAdultsDto(name: $name, surname: $surname, dateBirth: $dateBirth, gender: $gender, type: $type)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserAdultsDtoCopyWith<_$_UserAdultsDto> get copyWith =>
      __$$_UserAdultsDtoCopyWithImpl<_$_UserAdultsDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserAdultsDtoToJson(
      this,
    );
  }
}

abstract class _UserAdultsDto implements UsersAdultsDto {
  factory _UserAdultsDto(
      {required String name,
      required String surname,
      @JsonKey(name: 'date_birth') required String dateBirth,
      required String gender,
      required String type}) = _$_UserAdultsDto;

  factory _UserAdultsDto.fromJson(Map<String, dynamic> json) =
      _$_UserAdultsDto.fromJson;

  @override
  String get name;
  set name(String value);
  @override
  String get surname;
  set surname(String value);
  @override
  @JsonKey(name: 'date_birth')
  String get dateBirth;
  @JsonKey(name: 'date_birth')
  set dateBirth(String value);
  @override
  String get gender;
  set gender(String value);
  @override
  String get type;
  set type(String value);
  @override
  @JsonKey(ignore: true)
  _$$_UserAdultsDtoCopyWith<_$_UserAdultsDto> get copyWith =>
      throw _privateConstructorUsedError;
}
