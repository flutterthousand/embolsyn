// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'comforts.freezed.dart';
part 'comforts.g.dart';

@freezed
 class Comforts with _$Comforts {
  const factory Comforts({
    required int id,
    required String name,
    String? image,
    // City city,
  }) = _Comforts;

  factory Comforts.fromJson(Map<String, dynamic> json) => _$ComfortsFromJson(json);
}
