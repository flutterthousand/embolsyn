// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'comforts.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Comforts _$ComfortsFromJson(Map<String, dynamic> json) {
  return _Comforts.fromJson(json);
}

/// @nodoc
mixin _$Comforts {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ComfortsCopyWith<Comforts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ComfortsCopyWith<$Res> {
  factory $ComfortsCopyWith(Comforts value, $Res Function(Comforts) then) =
      _$ComfortsCopyWithImpl<$Res, Comforts>;
  @useResult
  $Res call({int id, String name, String? image});
}

/// @nodoc
class _$ComfortsCopyWithImpl<$Res, $Val extends Comforts>
    implements $ComfortsCopyWith<$Res> {
  _$ComfortsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? image = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ComfortsCopyWith<$Res> implements $ComfortsCopyWith<$Res> {
  factory _$$_ComfortsCopyWith(
          _$_Comforts value, $Res Function(_$_Comforts) then) =
      __$$_ComfortsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String name, String? image});
}

/// @nodoc
class __$$_ComfortsCopyWithImpl<$Res>
    extends _$ComfortsCopyWithImpl<$Res, _$_Comforts>
    implements _$$_ComfortsCopyWith<$Res> {
  __$$_ComfortsCopyWithImpl(
      _$_Comforts _value, $Res Function(_$_Comforts) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? image = freezed,
  }) {
    return _then(_$_Comforts(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Comforts implements _Comforts {
  const _$_Comforts({required this.id, required this.name, this.image});

  factory _$_Comforts.fromJson(Map<String, dynamic> json) =>
      _$$_ComfortsFromJson(json);

  @override
  final int id;
  @override
  final String name;
  @override
  final String? image;

  @override
  String toString() {
    return 'Comforts(id: $id, name: $name, image: $image)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Comforts &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.image, image) || other.image == image));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name, image);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ComfortsCopyWith<_$_Comforts> get copyWith =>
      __$$_ComfortsCopyWithImpl<_$_Comforts>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ComfortsToJson(
      this,
    );
  }
}

abstract class _Comforts implements Comforts {
  const factory _Comforts(
      {required final int id,
      required final String name,
      final String? image}) = _$_Comforts;

  factory _Comforts.fromJson(Map<String, dynamic> json) = _$_Comforts.fromJson;

  @override
  int get id;
  @override
  String get name;
  @override
  String? get image;
  @override
  @JsonKey(ignore: true)
  _$$_ComfortsCopyWith<_$_Comforts> get copyWith =>
      throw _privateConstructorUsedError;
}
