// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'show_sanatorium.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ShowSanatorium _$$_ShowSanatoriumFromJson(Map<String, dynamic> json) =>
    _$_ShowSanatorium(
      id: json['id'] as int,
      city: json['city'] as String?,
      title: json['title'] as String?,
      description: json['description'] as String?,
      price: json['price'] as int?,
      address: json['address'] as String?,
      phone: json['phone'] as String?,
      site: json['site'] as String?,
      email: json['email'] as String?,
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
      rooms: (json['rooms'] as List<dynamic>?)
          ?.map((e) => Rooms.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ShowSanatoriumToJson(_$_ShowSanatorium instance) =>
    <String, dynamic>{
      'id': instance.id,
      'city': instance.city,
      'title': instance.title,
      'description': instance.description,
      'price': instance.price,
      'address': instance.address,
      'phone': instance.phone,
      'site': instance.site,
      'email': instance.email,
      'images': instance.images,
      'rooms': instance.rooms,
    };
