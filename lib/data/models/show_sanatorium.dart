import 'package:embolsyn/data/models/rooms.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'show_sanatorium.freezed.dart';

part 'show_sanatorium.g.dart';

@freezed
class ShowSanatorium with _$ShowSanatorium {
  const factory ShowSanatorium({
    required int id,
    String? city,
    String? title,
    String? description,
    int? price,
    String? address,
    String? phone,
    String? site,
    String? email,
    List<String>? images,
    List<Rooms>? rooms,
    

    // City city,
  }) = _ShowSanatorium;

  factory ShowSanatorium.fromJson(Map<String, dynamic> json) =>
      _$ShowSanatoriumFromJson(json);
}
