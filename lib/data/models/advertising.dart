// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'advertising.freezed.dart';
part 'advertising.g.dart';

@freezed
 class Advertising with _$Advertising {
  const factory Advertising({
    required int id,
    @JsonKey(name: 'sanatorium_id') int? sanatoriumId,
    String? description,
    String? image,
    String?sanatorium ,
    int? price,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'updated_at') String? updateddAt,
  }) = _Advertising;

  factory Advertising.fromJson(Map<String, dynamic> json) =>
      _$AdvertisingFromJson(json);
}
