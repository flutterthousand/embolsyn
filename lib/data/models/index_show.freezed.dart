// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'index_show.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IndexShow _$IndexShowFromJson(Map<String, dynamic> json) {
  return _IndexShow.fromJson(json);
}

/// @nodoc
mixin _$IndexShow {
  List<ShowSanatorium>? get rooms => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IndexShowCopyWith<IndexShow> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IndexShowCopyWith<$Res> {
  factory $IndexShowCopyWith(IndexShow value, $Res Function(IndexShow) then) =
      _$IndexShowCopyWithImpl<$Res, IndexShow>;
  @useResult
  $Res call({List<ShowSanatorium>? rooms});
}

/// @nodoc
class _$IndexShowCopyWithImpl<$Res, $Val extends IndexShow>
    implements $IndexShowCopyWith<$Res> {
  _$IndexShowCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? rooms = freezed,
  }) {
    return _then(_value.copyWith(
      rooms: freezed == rooms
          ? _value.rooms
          : rooms // ignore: cast_nullable_to_non_nullable
              as List<ShowSanatorium>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_IndexShowCopyWith<$Res> implements $IndexShowCopyWith<$Res> {
  factory _$$_IndexShowCopyWith(
          _$_IndexShow value, $Res Function(_$_IndexShow) then) =
      __$$_IndexShowCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ShowSanatorium>? rooms});
}

/// @nodoc
class __$$_IndexShowCopyWithImpl<$Res>
    extends _$IndexShowCopyWithImpl<$Res, _$_IndexShow>
    implements _$$_IndexShowCopyWith<$Res> {
  __$$_IndexShowCopyWithImpl(
      _$_IndexShow _value, $Res Function(_$_IndexShow) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? rooms = freezed,
  }) {
    return _then(_$_IndexShow(
      rooms: freezed == rooms
          ? _value._rooms
          : rooms // ignore: cast_nullable_to_non_nullable
              as List<ShowSanatorium>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_IndexShow implements _IndexShow {
  const _$_IndexShow({final List<ShowSanatorium>? rooms}) : _rooms = rooms;

  factory _$_IndexShow.fromJson(Map<String, dynamic> json) =>
      _$$_IndexShowFromJson(json);

  final List<ShowSanatorium>? _rooms;
  @override
  List<ShowSanatorium>? get rooms {
    final value = _rooms;
    if (value == null) return null;
    if (_rooms is EqualUnmodifiableListView) return _rooms;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'IndexShow(rooms: $rooms)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_IndexShow &&
            const DeepCollectionEquality().equals(other._rooms, _rooms));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_rooms));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_IndexShowCopyWith<_$_IndexShow> get copyWith =>
      __$$_IndexShowCopyWithImpl<_$_IndexShow>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_IndexShowToJson(
      this,
    );
  }
}

abstract class _IndexShow implements IndexShow {
  const factory _IndexShow({final List<ShowSanatorium>? rooms}) = _$_IndexShow;

  factory _IndexShow.fromJson(Map<String, dynamic> json) =
      _$_IndexShow.fromJson;

  @override
  List<ShowSanatorium>? get rooms;
  @override
  @JsonKey(ignore: true)
  _$$_IndexShowCopyWith<_$_IndexShow> get copyWith =>
      throw _privateConstructorUsedError;
}
