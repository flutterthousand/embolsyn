// ignore_for_file: invalid_annotation_target

import 'package:embolsyn/data/models/comforts.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'rooms.freezed.dart';
part 'rooms.g.dart';

@unfreezed
class Rooms with _$Rooms {
   factory Rooms({
    required int id,
    // @JsonKey(name: 'sanatorium_id') required int sanatoriumId,
    String? title,
    String? description,
    @JsonKey(name: 'room_left') int? roomLeft,
    @JsonKey(name: 'is_busy') bool? isBusy,
    @JsonKey(name: 'count_adults') int? countAdults,
    @JsonKey(name: 'count_children') int? countChildren,
    @JsonKey(name: 'total_price') int? totalPrice,
    int? price,
    @JsonKey(name: 'free_places') int? freePlaces,
    @JsonKey(name: 'from_date') String? fromDate,
    @JsonKey(name: 'to_date') String? toDate,
    List<String>? images,
    List<Comforts>? comforts,
    @JsonKey(defaultValue: false) bool? showDetail,

    // City city,
  }) = _Rooms;

  factory Rooms.fromJson(Map<String, dynamic> json) => _$RoomsFromJson(json);
}
