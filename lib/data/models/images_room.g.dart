// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images_room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ImagesRoom _$$_ImagesRoomFromJson(Map<String, dynamic> json) =>
    _$_ImagesRoom(
      id: json['id'] as int,
      sanatoriumRoomId: json['sanatorium_room_id'] as int?,
      path: json['path'] as String?,
    );

Map<String, dynamic> _$$_ImagesRoomToJson(_$_ImagesRoom instance) =>
    <String, dynamic>{
      'id': instance.id,
      'sanatorium_room_id': instance.sanatoriumRoomId,
      'path': instance.path,
    };
