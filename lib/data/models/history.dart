// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'history.freezed.dart';
part 'history.g.dart';

@freezed
class History with _$History {
  const factory History({
    @JsonKey(name: 'order_id') int? orderId,
    @JsonKey(name: 'sanatorium_name') String? sanatoriumName,
    @JsonKey(name: 'sanatorium_room_name') String? sanatoriumRoomName,
    @JsonKey(name: 'count_adults') int? countAdults,
    @JsonKey(name: 'count_children') int? countChildren,
    @JsonKey(name: 'from_date') String? fromDate,
    @JsonKey(name: 'to_date') String? toDate,
    @JsonKey(name: 'total_price') int? totalPrice,
  }) = _History;

  factory History.fromJson(Map<String, dynamic> json) =>
      _$HistoryFromJson(json);
}
