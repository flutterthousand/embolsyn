// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'booked_user.freezed.dart';
part 'booked_user.g.dart';

@freezed
 class BookedUser with _$BookedUser {
  const factory BookedUser({
    required int id,
    @JsonKey(name: 'booking_id') int? bookingId,
    String? name,
    String? surname,
    String? gender,
    @JsonKey(name: 'date_birth') String? dateBirth,
      String? nationality,
      String? validity,
      String? iin,
      String? type,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updateddAt,
  }) = _BookedUser;

  factory BookedUser.fromJson(Map<String, dynamic> json) =>
      _$BookedUserFromJson(json);
}
