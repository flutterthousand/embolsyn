// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'images_room.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ImagesRoom _$ImagesRoomFromJson(Map<String, dynamic> json) {
  return _ImagesRoom.fromJson(json);
}

/// @nodoc
mixin _$ImagesRoom {
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'sanatorium_room_id')
  int? get sanatoriumRoomId => throw _privateConstructorUsedError;
  String? get path => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ImagesRoomCopyWith<ImagesRoom> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImagesRoomCopyWith<$Res> {
  factory $ImagesRoomCopyWith(
          ImagesRoom value, $Res Function(ImagesRoom) then) =
      _$ImagesRoomCopyWithImpl<$Res, ImagesRoom>;
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'sanatorium_room_id') int? sanatoriumRoomId,
      String? path});
}

/// @nodoc
class _$ImagesRoomCopyWithImpl<$Res, $Val extends ImagesRoom>
    implements $ImagesRoomCopyWith<$Res> {
  _$ImagesRoomCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sanatoriumRoomId = freezed,
    Object? path = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sanatoriumRoomId: freezed == sanatoriumRoomId
          ? _value.sanatoriumRoomId
          : sanatoriumRoomId // ignore: cast_nullable_to_non_nullable
              as int?,
      path: freezed == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ImagesRoomCopyWith<$Res>
    implements $ImagesRoomCopyWith<$Res> {
  factory _$$_ImagesRoomCopyWith(
          _$_ImagesRoom value, $Res Function(_$_ImagesRoom) then) =
      __$$_ImagesRoomCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'sanatorium_room_id') int? sanatoriumRoomId,
      String? path});
}

/// @nodoc
class __$$_ImagesRoomCopyWithImpl<$Res>
    extends _$ImagesRoomCopyWithImpl<$Res, _$_ImagesRoom>
    implements _$$_ImagesRoomCopyWith<$Res> {
  __$$_ImagesRoomCopyWithImpl(
      _$_ImagesRoom _value, $Res Function(_$_ImagesRoom) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sanatoriumRoomId = freezed,
    Object? path = freezed,
  }) {
    return _then(_$_ImagesRoom(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sanatoriumRoomId: freezed == sanatoriumRoomId
          ? _value.sanatoriumRoomId
          : sanatoriumRoomId // ignore: cast_nullable_to_non_nullable
              as int?,
      path: freezed == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ImagesRoom implements _ImagesRoom {
  const _$_ImagesRoom(
      {required this.id,
      @JsonKey(name: 'sanatorium_room_id') this.sanatoriumRoomId,
      this.path});

  factory _$_ImagesRoom.fromJson(Map<String, dynamic> json) =>
      _$$_ImagesRoomFromJson(json);

  @override
  final int id;
  @override
  @JsonKey(name: 'sanatorium_room_id')
  final int? sanatoriumRoomId;
  @override
  final String? path;

  @override
  String toString() {
    return 'ImagesRoom(id: $id, sanatoriumRoomId: $sanatoriumRoomId, path: $path)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ImagesRoom &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.sanatoriumRoomId, sanatoriumRoomId) ||
                other.sanatoriumRoomId == sanatoriumRoomId) &&
            (identical(other.path, path) || other.path == path));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, sanatoriumRoomId, path);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ImagesRoomCopyWith<_$_ImagesRoom> get copyWith =>
      __$$_ImagesRoomCopyWithImpl<_$_ImagesRoom>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ImagesRoomToJson(
      this,
    );
  }
}

abstract class _ImagesRoom implements ImagesRoom {
  const factory _ImagesRoom(
      {required final int id,
      @JsonKey(name: 'sanatorium_room_id') final int? sanatoriumRoomId,
      final String? path}) = _$_ImagesRoom;

  factory _ImagesRoom.fromJson(Map<String, dynamic> json) =
      _$_ImagesRoom.fromJson;

  @override
  int get id;
  @override
  @JsonKey(name: 'sanatorium_room_id')
  int? get sanatoriumRoomId;
  @override
  String? get path;
  @override
  @JsonKey(ignore: true)
  _$$_ImagesRoomCopyWith<_$_ImagesRoom> get copyWith =>
      throw _privateConstructorUsedError;
}
