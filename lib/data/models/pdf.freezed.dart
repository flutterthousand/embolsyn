// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pdf.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Pdf _$PdfFromJson(Map<String, dynamic> json) {
  return _Pdf.fromJson(json);
}

/// @nodoc
mixin _$Pdf {
  String? get path => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PdfCopyWith<Pdf> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PdfCopyWith<$Res> {
  factory $PdfCopyWith(Pdf value, $Res Function(Pdf) then) =
      _$PdfCopyWithImpl<$Res, Pdf>;
  @useResult
  $Res call({String? path});
}

/// @nodoc
class _$PdfCopyWithImpl<$Res, $Val extends Pdf> implements $PdfCopyWith<$Res> {
  _$PdfCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? path = freezed,
  }) {
    return _then(_value.copyWith(
      path: freezed == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PdfCopyWith<$Res> implements $PdfCopyWith<$Res> {
  factory _$$_PdfCopyWith(_$_Pdf value, $Res Function(_$_Pdf) then) =
      __$$_PdfCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? path});
}

/// @nodoc
class __$$_PdfCopyWithImpl<$Res> extends _$PdfCopyWithImpl<$Res, _$_Pdf>
    implements _$$_PdfCopyWith<$Res> {
  __$$_PdfCopyWithImpl(_$_Pdf _value, $Res Function(_$_Pdf) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? path = freezed,
  }) {
    return _then(_$_Pdf(
      path: freezed == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Pdf implements _Pdf {
  const _$_Pdf({this.path});

  factory _$_Pdf.fromJson(Map<String, dynamic> json) => _$$_PdfFromJson(json);

  @override
  final String? path;

  @override
  String toString() {
    return 'Pdf(path: $path)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Pdf &&
            (identical(other.path, path) || other.path == path));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, path);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PdfCopyWith<_$_Pdf> get copyWith =>
      __$$_PdfCopyWithImpl<_$_Pdf>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PdfToJson(
      this,
    );
  }
}

abstract class _Pdf implements Pdf {
  const factory _Pdf({final String? path}) = _$_Pdf;

  factory _Pdf.fromJson(Map<String, dynamic> json) = _$_Pdf.fromJson;

  @override
  String? get path;
  @override
  @JsonKey(ignore: true)
  _$$_PdfCopyWith<_$_Pdf> get copyWith => throw _privateConstructorUsedError;
}
