// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_booked.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserBook _$$_UserBookFromJson(Map<String, dynamic> json) => _$_UserBook(
      id: json['id'] as int,
      cityId: json['city_id'] as int?,
      phone: json['phone'] as int?,
      deviceToken: json['device_token'] as String?,
      name: json['name'] as String?,
      surname: json['surname'] as String?,
      rememberToken: json['remember_token'] as String?,
      created: json['created_at'] as String?,
      updated: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_UserBookToJson(_$_UserBook instance) =>
    <String, dynamic>{
      'id': instance.id,
      'city_id': instance.cityId,
      'phone': instance.phone,
      'device_token': instance.deviceToken,
      'name': instance.name,
      'surname': instance.surname,
      'remember_token': instance.rememberToken,
      'created_at': instance.created,
      'updated_at': instance.updated,
    };
