// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_booked.freezed.dart';
part 'user_booked.g.dart';

@freezed
 class UserBook with _$UserBook {
  const factory UserBook({
    required int id,
  @JsonKey(name: 'city_id') int? cityId,
    int? phone,
     @JsonKey(name: 'device_token') String? deviceToken,
     String? name,
     String? surname,
    @JsonKey(name: 'remember_token') String? rememberToken,
    @JsonKey(name: 'created_at') String? created,
    @JsonKey(name: 'updated_at') String? updated,
   
  }) = _UserBook;

  factory UserBook.fromJson(Map<String, dynamic> json) =>
      _$UserBookFromJson(json);
}
