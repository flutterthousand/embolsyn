// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'index_show.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_IndexShow _$$_IndexShowFromJson(Map<String, dynamic> json) => _$_IndexShow(
      rooms: (json['rooms'] as List<dynamic>?)
          ?.map((e) => ShowSanatorium.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_IndexShowToJson(_$_IndexShow instance) =>
    <String, dynamic>{
      'rooms': instance.rooms,
    };
