// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sanatorium_room_booked.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SanatoriumRoomBooked _$SanatoriumRoomBookedFromJson(Map<String, dynamic> json) {
  return _SanatoriumRoomBooked.fromJson(json);
}

/// @nodoc
mixin _$SanatoriumRoomBooked {
  int get id => throw _privateConstructorUsedError;
  String? get sanatorium => throw _privateConstructorUsedError;
  int? get price => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_adults')
  int? get countAdults => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_children')
  int? get countChildren => throw _privateConstructorUsedError;
  List<ImagesRoom>? get images => throw _privateConstructorUsedError;
  List<String>? get comforts => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SanatoriumRoomBookedCopyWith<SanatoriumRoomBooked> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SanatoriumRoomBookedCopyWith<$Res> {
  factory $SanatoriumRoomBookedCopyWith(SanatoriumRoomBooked value,
          $Res Function(SanatoriumRoomBooked) then) =
      _$SanatoriumRoomBookedCopyWithImpl<$Res, SanatoriumRoomBooked>;
  @useResult
  $Res call(
      {int id,
      String? sanatorium,
      int? price,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      List<ImagesRoom>? images,
      List<String>? comforts});
}

/// @nodoc
class _$SanatoriumRoomBookedCopyWithImpl<$Res,
        $Val extends SanatoriumRoomBooked>
    implements $SanatoriumRoomBookedCopyWith<$Res> {
  _$SanatoriumRoomBookedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sanatorium = freezed,
    Object? price = freezed,
    Object? countAdults = freezed,
    Object? countChildren = freezed,
    Object? images = freezed,
    Object? comforts = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sanatorium: freezed == sanatorium
          ? _value.sanatorium
          : sanatorium // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      countAdults: freezed == countAdults
          ? _value.countAdults
          : countAdults // ignore: cast_nullable_to_non_nullable
              as int?,
      countChildren: freezed == countChildren
          ? _value.countChildren
          : countChildren // ignore: cast_nullable_to_non_nullable
              as int?,
      images: freezed == images
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<ImagesRoom>?,
      comforts: freezed == comforts
          ? _value.comforts
          : comforts // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SanatoriumRoomBookedCopyWith<$Res>
    implements $SanatoriumRoomBookedCopyWith<$Res> {
  factory _$$_SanatoriumRoomBookedCopyWith(_$_SanatoriumRoomBooked value,
          $Res Function(_$_SanatoriumRoomBooked) then) =
      __$$_SanatoriumRoomBookedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String? sanatorium,
      int? price,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      List<ImagesRoom>? images,
      List<String>? comforts});
}

/// @nodoc
class __$$_SanatoriumRoomBookedCopyWithImpl<$Res>
    extends _$SanatoriumRoomBookedCopyWithImpl<$Res, _$_SanatoriumRoomBooked>
    implements _$$_SanatoriumRoomBookedCopyWith<$Res> {
  __$$_SanatoriumRoomBookedCopyWithImpl(_$_SanatoriumRoomBooked _value,
      $Res Function(_$_SanatoriumRoomBooked) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sanatorium = freezed,
    Object? price = freezed,
    Object? countAdults = freezed,
    Object? countChildren = freezed,
    Object? images = freezed,
    Object? comforts = freezed,
  }) {
    return _then(_$_SanatoriumRoomBooked(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sanatorium: freezed == sanatorium
          ? _value.sanatorium
          : sanatorium // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      countAdults: freezed == countAdults
          ? _value.countAdults
          : countAdults // ignore: cast_nullable_to_non_nullable
              as int?,
      countChildren: freezed == countChildren
          ? _value.countChildren
          : countChildren // ignore: cast_nullable_to_non_nullable
              as int?,
      images: freezed == images
          ? _value._images
          : images // ignore: cast_nullable_to_non_nullable
              as List<ImagesRoom>?,
      comforts: freezed == comforts
          ? _value._comforts
          : comforts // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SanatoriumRoomBooked implements _SanatoriumRoomBooked {
  const _$_SanatoriumRoomBooked(
      {required this.id,
      this.sanatorium,
      this.price,
      @JsonKey(name: 'count_adults') this.countAdults,
      @JsonKey(name: 'count_children') this.countChildren,
      final List<ImagesRoom>? images,
      final List<String>? comforts})
      : _images = images,
        _comforts = comforts;

  factory _$_SanatoriumRoomBooked.fromJson(Map<String, dynamic> json) =>
      _$$_SanatoriumRoomBookedFromJson(json);

  @override
  final int id;
  @override
  final String? sanatorium;
  @override
  final int? price;
  @override
  @JsonKey(name: 'count_adults')
  final int? countAdults;
  @override
  @JsonKey(name: 'count_children')
  final int? countChildren;
  final List<ImagesRoom>? _images;
  @override
  List<ImagesRoom>? get images {
    final value = _images;
    if (value == null) return null;
    if (_images is EqualUnmodifiableListView) return _images;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String>? _comforts;
  @override
  List<String>? get comforts {
    final value = _comforts;
    if (value == null) return null;
    if (_comforts is EqualUnmodifiableListView) return _comforts;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'SanatoriumRoomBooked(id: $id, sanatorium: $sanatorium, price: $price, countAdults: $countAdults, countChildren: $countChildren, images: $images, comforts: $comforts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SanatoriumRoomBooked &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.sanatorium, sanatorium) ||
                other.sanatorium == sanatorium) &&
            (identical(other.price, price) || other.price == price) &&
            (identical(other.countAdults, countAdults) ||
                other.countAdults == countAdults) &&
            (identical(other.countChildren, countChildren) ||
                other.countChildren == countChildren) &&
            const DeepCollectionEquality().equals(other._images, _images) &&
            const DeepCollectionEquality().equals(other._comforts, _comforts));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      sanatorium,
      price,
      countAdults,
      countChildren,
      const DeepCollectionEquality().hash(_images),
      const DeepCollectionEquality().hash(_comforts));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SanatoriumRoomBookedCopyWith<_$_SanatoriumRoomBooked> get copyWith =>
      __$$_SanatoriumRoomBookedCopyWithImpl<_$_SanatoriumRoomBooked>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SanatoriumRoomBookedToJson(
      this,
    );
  }
}

abstract class _SanatoriumRoomBooked implements SanatoriumRoomBooked {
  const factory _SanatoriumRoomBooked(
      {required final int id,
      final String? sanatorium,
      final int? price,
      @JsonKey(name: 'count_adults') final int? countAdults,
      @JsonKey(name: 'count_children') final int? countChildren,
      final List<ImagesRoom>? images,
      final List<String>? comforts}) = _$_SanatoriumRoomBooked;

  factory _SanatoriumRoomBooked.fromJson(Map<String, dynamic> json) =
      _$_SanatoriumRoomBooked.fromJson;

  @override
  int get id;
  @override
  String? get sanatorium;
  @override
  int? get price;
  @override
  @JsonKey(name: 'count_adults')
  int? get countAdults;
  @override
  @JsonKey(name: 'count_children')
  int? get countChildren;
  @override
  List<ImagesRoom>? get images;
  @override
  List<String>? get comforts;
  @override
  @JsonKey(ignore: true)
  _$$_SanatoriumRoomBookedCopyWith<_$_SanatoriumRoomBooked> get copyWith =>
      throw _privateConstructorUsedError;
}
