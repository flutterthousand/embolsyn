// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pdf.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Pdf _$$_PdfFromJson(Map<String, dynamic> json) => _$_Pdf(
      path: json['path'] as String?,
    );

Map<String, dynamic> _$$_PdfToJson(_$_Pdf instance) => <String, dynamic>{
      'path': instance.path,
    };
