// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comforts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Comforts _$$_ComfortsFromJson(Map<String, dynamic> json) => _$_Comforts(
      id: json['id'] as int,
      name: json['name'] as String,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$$_ComfortsToJson(_$_Comforts instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
    };
