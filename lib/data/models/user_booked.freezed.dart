// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_booked.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserBook _$UserBookFromJson(Map<String, dynamic> json) {
  return _UserBook.fromJson(json);
}

/// @nodoc
mixin _$UserBook {
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'city_id')
  int? get cityId => throw _privateConstructorUsedError;
  int? get phone => throw _privateConstructorUsedError;
  @JsonKey(name: 'device_token')
  String? get deviceToken => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get surname => throw _privateConstructorUsedError;
  @JsonKey(name: 'remember_token')
  String? get rememberToken => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get created => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updated => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserBookCopyWith<UserBook> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserBookCopyWith<$Res> {
  factory $UserBookCopyWith(UserBook value, $Res Function(UserBook) then) =
      _$UserBookCopyWithImpl<$Res, UserBook>;
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'city_id') int? cityId,
      int? phone,
      @JsonKey(name: 'device_token') String? deviceToken,
      String? name,
      String? surname,
      @JsonKey(name: 'remember_token') String? rememberToken,
      @JsonKey(name: 'created_at') String? created,
      @JsonKey(name: 'updated_at') String? updated});
}

/// @nodoc
class _$UserBookCopyWithImpl<$Res, $Val extends UserBook>
    implements $UserBookCopyWith<$Res> {
  _$UserBookCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? cityId = freezed,
    Object? phone = freezed,
    Object? deviceToken = freezed,
    Object? name = freezed,
    Object? surname = freezed,
    Object? rememberToken = freezed,
    Object? created = freezed,
    Object? updated = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      cityId: freezed == cityId
          ? _value.cityId
          : cityId // ignore: cast_nullable_to_non_nullable
              as int?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as int?,
      deviceToken: freezed == deviceToken
          ? _value.deviceToken
          : deviceToken // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      surname: freezed == surname
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String?,
      rememberToken: freezed == rememberToken
          ? _value.rememberToken
          : rememberToken // ignore: cast_nullable_to_non_nullable
              as String?,
      created: freezed == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as String?,
      updated: freezed == updated
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserBookCopyWith<$Res> implements $UserBookCopyWith<$Res> {
  factory _$$_UserBookCopyWith(
          _$_UserBook value, $Res Function(_$_UserBook) then) =
      __$$_UserBookCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'city_id') int? cityId,
      int? phone,
      @JsonKey(name: 'device_token') String? deviceToken,
      String? name,
      String? surname,
      @JsonKey(name: 'remember_token') String? rememberToken,
      @JsonKey(name: 'created_at') String? created,
      @JsonKey(name: 'updated_at') String? updated});
}

/// @nodoc
class __$$_UserBookCopyWithImpl<$Res>
    extends _$UserBookCopyWithImpl<$Res, _$_UserBook>
    implements _$$_UserBookCopyWith<$Res> {
  __$$_UserBookCopyWithImpl(
      _$_UserBook _value, $Res Function(_$_UserBook) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? cityId = freezed,
    Object? phone = freezed,
    Object? deviceToken = freezed,
    Object? name = freezed,
    Object? surname = freezed,
    Object? rememberToken = freezed,
    Object? created = freezed,
    Object? updated = freezed,
  }) {
    return _then(_$_UserBook(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      cityId: freezed == cityId
          ? _value.cityId
          : cityId // ignore: cast_nullable_to_non_nullable
              as int?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as int?,
      deviceToken: freezed == deviceToken
          ? _value.deviceToken
          : deviceToken // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      surname: freezed == surname
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String?,
      rememberToken: freezed == rememberToken
          ? _value.rememberToken
          : rememberToken // ignore: cast_nullable_to_non_nullable
              as String?,
      created: freezed == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as String?,
      updated: freezed == updated
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserBook implements _UserBook {
  const _$_UserBook(
      {required this.id,
      @JsonKey(name: 'city_id') this.cityId,
      this.phone,
      @JsonKey(name: 'device_token') this.deviceToken,
      this.name,
      this.surname,
      @JsonKey(name: 'remember_token') this.rememberToken,
      @JsonKey(name: 'created_at') this.created,
      @JsonKey(name: 'updated_at') this.updated});

  factory _$_UserBook.fromJson(Map<String, dynamic> json) =>
      _$$_UserBookFromJson(json);

  @override
  final int id;
  @override
  @JsonKey(name: 'city_id')
  final int? cityId;
  @override
  final int? phone;
  @override
  @JsonKey(name: 'device_token')
  final String? deviceToken;
  @override
  final String? name;
  @override
  final String? surname;
  @override
  @JsonKey(name: 'remember_token')
  final String? rememberToken;
  @override
  @JsonKey(name: 'created_at')
  final String? created;
  @override
  @JsonKey(name: 'updated_at')
  final String? updated;

  @override
  String toString() {
    return 'UserBook(id: $id, cityId: $cityId, phone: $phone, deviceToken: $deviceToken, name: $name, surname: $surname, rememberToken: $rememberToken, created: $created, updated: $updated)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserBook &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.cityId, cityId) || other.cityId == cityId) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.deviceToken, deviceToken) ||
                other.deviceToken == deviceToken) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.surname, surname) || other.surname == surname) &&
            (identical(other.rememberToken, rememberToken) ||
                other.rememberToken == rememberToken) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.updated, updated) || other.updated == updated));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, cityId, phone, deviceToken,
      name, surname, rememberToken, created, updated);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserBookCopyWith<_$_UserBook> get copyWith =>
      __$$_UserBookCopyWithImpl<_$_UserBook>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserBookToJson(
      this,
    );
  }
}

abstract class _UserBook implements UserBook {
  const factory _UserBook(
      {required final int id,
      @JsonKey(name: 'city_id') final int? cityId,
      final int? phone,
      @JsonKey(name: 'device_token') final String? deviceToken,
      final String? name,
      final String? surname,
      @JsonKey(name: 'remember_token') final String? rememberToken,
      @JsonKey(name: 'created_at') final String? created,
      @JsonKey(name: 'updated_at') final String? updated}) = _$_UserBook;

  factory _UserBook.fromJson(Map<String, dynamic> json) = _$_UserBook.fromJson;

  @override
  int get id;
  @override
  @JsonKey(name: 'city_id')
  int? get cityId;
  @override
  int? get phone;
  @override
  @JsonKey(name: 'device_token')
  String? get deviceToken;
  @override
  String? get name;
  @override
  String? get surname;
  @override
  @JsonKey(name: 'remember_token')
  String? get rememberToken;
  @override
  @JsonKey(name: 'created_at')
  String? get created;
  @override
  @JsonKey(name: 'updated_at')
  String? get updated;
  @override
  @JsonKey(ignore: true)
  _$$_UserBookCopyWith<_$_UserBook> get copyWith =>
      throw _privateConstructorUsedError;
}
