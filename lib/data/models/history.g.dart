// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_History _$$_HistoryFromJson(Map<String, dynamic> json) => _$_History(
      orderId: json['order_id'] as int?,
      sanatoriumName: json['sanatorium_name'] as String?,
      sanatoriumRoomName: json['sanatorium_room_name'] as String?,
      countAdults: json['count_adults'] as int?,
      countChildren: json['count_children'] as int?,
      fromDate: json['from_date'] as String?,
      toDate: json['to_date'] as String?,
      totalPrice: json['total_price'] as int?,
    );

Map<String, dynamic> _$$_HistoryToJson(_$_History instance) =>
    <String, dynamic>{
      'order_id': instance.orderId,
      'sanatorium_name': instance.sanatoriumName,
      'sanatorium_room_name': instance.sanatoriumRoomName,
      'count_adults': instance.countAdults,
      'count_children': instance.countChildren,
      'from_date': instance.fromDate,
      'to_date': instance.toDate,
      'total_price': instance.totalPrice,
    };
