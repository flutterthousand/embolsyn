import 'package:embolsyn/data/models/show_sanatorium.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'index_show.freezed.dart';

part 'index_show.g.dart';
@freezed
 class IndexShow with _$IndexShow {
  const factory IndexShow({
    
    List<ShowSanatorium>? rooms,

    // City city,
  }) = _IndexShow;

  factory IndexShow.fromJson(Map<String, dynamic> json) =>
      _$IndexShowFromJson(json);
}
