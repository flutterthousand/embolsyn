// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sanatorium_room_booked.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SanatoriumRoomBooked _$$_SanatoriumRoomBookedFromJson(
        Map<String, dynamic> json) =>
    _$_SanatoriumRoomBooked(
      id: json['id'] as int,
      sanatorium: json['sanatorium'] as String?,
      price: json['price'] as int?,
      countAdults: json['count_adults'] as int?,
      countChildren: json['count_children'] as int?,
      images: (json['images'] as List<dynamic>?)
          ?.map((e) => ImagesRoom.fromJson(e as Map<String, dynamic>))
          .toList(),
      comforts: (json['comforts'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$$_SanatoriumRoomBookedToJson(
        _$_SanatoriumRoomBooked instance) =>
    <String, dynamic>{
      'id': instance.id,
      'sanatorium': instance.sanatorium,
      'price': instance.price,
      'count_adults': instance.countAdults,
      'count_children': instance.countChildren,
      'images': instance.images,
      'comforts': instance.comforts,
    };
