// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'images_room.freezed.dart';
part 'images_room.g.dart';

@freezed
 class ImagesRoom with _$ImagesRoom {
  const factory ImagesRoom({
    required int id,
    @JsonKey(name: 'sanatorium_room_id') int? sanatoriumRoomId,
    String? path,
  }) = _ImagesRoom;

  factory ImagesRoom.fromJson(Map<String, dynamic> json) =>
      _$ImagesRoomFromJson(json);
}
