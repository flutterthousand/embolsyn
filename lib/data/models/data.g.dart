// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Data _$$_DataFromJson(Map<String, dynamic> json) => _$_Data(
      status: json['pg_status'] as String?,
      paymentId: json['pg_payment_id'] as String?,
      salt: json['pg_salt'] as String?,
      url: json['pg_redirect_url'] as String?,
    );

Map<String, dynamic> _$$_DataToJson(_$_Data instance) => <String, dynamic>{
      'pg_status': instance.status,
      'pg_payment_id': instance.paymentId,
      'pg_salt': instance.salt,
      'pg_redirect_url': instance.url,
    };
