// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'contact.freezed.dart';
part 'contact.g.dart';

@freezed
 class Contacts with _$Contacts {
  const factory Contacts({
     int? id,
    String? name,
    @JsonKey(name: 'sub_title') String? subtitle,
    String? vk,
    String? facebook,
    String? instagram,
    String? twitter,
    String? phone,
    String? website,
    String? address,
    String? image,
    String? email,
   @JsonKey(name: 'created_at') String? createdAt,
   @JsonKey(name: 'updated_at') String? updatedAt,

    // City city,
  }) = _Contacts;

  factory Contacts.fromJson(Map<String, dynamic> json) =>
      _$ContactsFromJson(json);
}
