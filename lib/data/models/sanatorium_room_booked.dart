// ignore_for_file: invalid_annotation_target

import 'package:embolsyn/data/models/images_room.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sanatorium_room_booked.freezed.dart';
part 'sanatorium_room_booked.g.dart';

@freezed
 class SanatoriumRoomBooked with _$SanatoriumRoomBooked {
  const factory SanatoriumRoomBooked({
    required int id,
    String? sanatorium,
    int? price,
    @JsonKey(name: 'count_adults') int? countAdults,
    @JsonKey(name: 'count_children') int? countChildren,
    List<ImagesRoom>? images,
    List<String>? comforts,
  }) = _SanatoriumRoomBooked;

  factory SanatoriumRoomBooked.fromJson(Map<String, dynamic> json) =>
      _$SanatoriumRoomBookedFromJson(json);
}
