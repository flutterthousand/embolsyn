// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'rooms.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Rooms _$RoomsFromJson(Map<String, dynamic> json) {
  return _Rooms.fromJson(json);
}

/// @nodoc
mixin _$Rooms {
  int get id => throw _privateConstructorUsedError;
  set id(int value) =>
      throw _privateConstructorUsedError; // @JsonKey(name: 'sanatorium_id') required int sanatoriumId,
  String? get title =>
      throw _privateConstructorUsedError; // @JsonKey(name: 'sanatorium_id') required int sanatoriumId,
  set title(String? value) => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  set description(String? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'room_left')
  int? get roomLeft => throw _privateConstructorUsedError;
  @JsonKey(name: 'room_left')
  set roomLeft(int? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_busy')
  bool? get isBusy => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_busy')
  set isBusy(bool? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_adults')
  int? get countAdults => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_adults')
  set countAdults(int? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_children')
  int? get countChildren => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_children')
  set countChildren(int? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'total_price')
  int? get totalPrice => throw _privateConstructorUsedError;
  @JsonKey(name: 'total_price')
  set totalPrice(int? value) => throw _privateConstructorUsedError;
  int? get price => throw _privateConstructorUsedError;
  set price(int? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'free_places')
  int? get freePlaces => throw _privateConstructorUsedError;
  @JsonKey(name: 'free_places')
  set freePlaces(int? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'from_date')
  String? get fromDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'from_date')
  set fromDate(String? value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'to_date')
  String? get toDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'to_date')
  set toDate(String? value) => throw _privateConstructorUsedError;
  List<String>? get images => throw _privateConstructorUsedError;
  set images(List<String>? value) => throw _privateConstructorUsedError;
  List<Comforts>? get comforts => throw _privateConstructorUsedError;
  set comforts(List<Comforts>? value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false)
  bool? get showDetail => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false)
  set showDetail(bool? value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RoomsCopyWith<Rooms> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RoomsCopyWith<$Res> {
  factory $RoomsCopyWith(Rooms value, $Res Function(Rooms) then) =
      _$RoomsCopyWithImpl<$Res, Rooms>;
  @useResult
  $Res call(
      {int id,
      String? title,
      String? description,
      @JsonKey(name: 'room_left') int? roomLeft,
      @JsonKey(name: 'is_busy') bool? isBusy,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      @JsonKey(name: 'total_price') int? totalPrice,
      int? price,
      @JsonKey(name: 'free_places') int? freePlaces,
      @JsonKey(name: 'from_date') String? fromDate,
      @JsonKey(name: 'to_date') String? toDate,
      List<String>? images,
      List<Comforts>? comforts,
      @JsonKey(defaultValue: false) bool? showDetail});
}

/// @nodoc
class _$RoomsCopyWithImpl<$Res, $Val extends Rooms>
    implements $RoomsCopyWith<$Res> {
  _$RoomsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? title = freezed,
    Object? description = freezed,
    Object? roomLeft = freezed,
    Object? isBusy = freezed,
    Object? countAdults = freezed,
    Object? countChildren = freezed,
    Object? totalPrice = freezed,
    Object? price = freezed,
    Object? freePlaces = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? images = freezed,
    Object? comforts = freezed,
    Object? showDetail = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      roomLeft: freezed == roomLeft
          ? _value.roomLeft
          : roomLeft // ignore: cast_nullable_to_non_nullable
              as int?,
      isBusy: freezed == isBusy
          ? _value.isBusy
          : isBusy // ignore: cast_nullable_to_non_nullable
              as bool?,
      countAdults: freezed == countAdults
          ? _value.countAdults
          : countAdults // ignore: cast_nullable_to_non_nullable
              as int?,
      countChildren: freezed == countChildren
          ? _value.countChildren
          : countChildren // ignore: cast_nullable_to_non_nullable
              as int?,
      totalPrice: freezed == totalPrice
          ? _value.totalPrice
          : totalPrice // ignore: cast_nullable_to_non_nullable
              as int?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      freePlaces: freezed == freePlaces
          ? _value.freePlaces
          : freePlaces // ignore: cast_nullable_to_non_nullable
              as int?,
      fromDate: freezed == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String?,
      toDate: freezed == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String?,
      images: freezed == images
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      comforts: freezed == comforts
          ? _value.comforts
          : comforts // ignore: cast_nullable_to_non_nullable
              as List<Comforts>?,
      showDetail: freezed == showDetail
          ? _value.showDetail
          : showDetail // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_RoomsCopyWith<$Res> implements $RoomsCopyWith<$Res> {
  factory _$$_RoomsCopyWith(_$_Rooms value, $Res Function(_$_Rooms) then) =
      __$$_RoomsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String? title,
      String? description,
      @JsonKey(name: 'room_left') int? roomLeft,
      @JsonKey(name: 'is_busy') bool? isBusy,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      @JsonKey(name: 'total_price') int? totalPrice,
      int? price,
      @JsonKey(name: 'free_places') int? freePlaces,
      @JsonKey(name: 'from_date') String? fromDate,
      @JsonKey(name: 'to_date') String? toDate,
      List<String>? images,
      List<Comforts>? comforts,
      @JsonKey(defaultValue: false) bool? showDetail});
}

/// @nodoc
class __$$_RoomsCopyWithImpl<$Res> extends _$RoomsCopyWithImpl<$Res, _$_Rooms>
    implements _$$_RoomsCopyWith<$Res> {
  __$$_RoomsCopyWithImpl(_$_Rooms _value, $Res Function(_$_Rooms) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? title = freezed,
    Object? description = freezed,
    Object? roomLeft = freezed,
    Object? isBusy = freezed,
    Object? countAdults = freezed,
    Object? countChildren = freezed,
    Object? totalPrice = freezed,
    Object? price = freezed,
    Object? freePlaces = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? images = freezed,
    Object? comforts = freezed,
    Object? showDetail = freezed,
  }) {
    return _then(_$_Rooms(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      roomLeft: freezed == roomLeft
          ? _value.roomLeft
          : roomLeft // ignore: cast_nullable_to_non_nullable
              as int?,
      isBusy: freezed == isBusy
          ? _value.isBusy
          : isBusy // ignore: cast_nullable_to_non_nullable
              as bool?,
      countAdults: freezed == countAdults
          ? _value.countAdults
          : countAdults // ignore: cast_nullable_to_non_nullable
              as int?,
      countChildren: freezed == countChildren
          ? _value.countChildren
          : countChildren // ignore: cast_nullable_to_non_nullable
              as int?,
      totalPrice: freezed == totalPrice
          ? _value.totalPrice
          : totalPrice // ignore: cast_nullable_to_non_nullable
              as int?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      freePlaces: freezed == freePlaces
          ? _value.freePlaces
          : freePlaces // ignore: cast_nullable_to_non_nullable
              as int?,
      fromDate: freezed == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String?,
      toDate: freezed == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String?,
      images: freezed == images
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      comforts: freezed == comforts
          ? _value.comforts
          : comforts // ignore: cast_nullable_to_non_nullable
              as List<Comforts>?,
      showDetail: freezed == showDetail
          ? _value.showDetail
          : showDetail // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Rooms implements _Rooms {
  _$_Rooms(
      {required this.id,
      this.title,
      this.description,
      @JsonKey(name: 'room_left') this.roomLeft,
      @JsonKey(name: 'is_busy') this.isBusy,
      @JsonKey(name: 'count_adults') this.countAdults,
      @JsonKey(name: 'count_children') this.countChildren,
      @JsonKey(name: 'total_price') this.totalPrice,
      this.price,
      @JsonKey(name: 'free_places') this.freePlaces,
      @JsonKey(name: 'from_date') this.fromDate,
      @JsonKey(name: 'to_date') this.toDate,
      this.images,
      this.comforts,
      @JsonKey(defaultValue: false) this.showDetail});

  factory _$_Rooms.fromJson(Map<String, dynamic> json) =>
      _$$_RoomsFromJson(json);

  @override
  int id;
// @JsonKey(name: 'sanatorium_id') required int sanatoriumId,
  @override
  String? title;
  @override
  String? description;
  @override
  @JsonKey(name: 'room_left')
  int? roomLeft;
  @override
  @JsonKey(name: 'is_busy')
  bool? isBusy;
  @override
  @JsonKey(name: 'count_adults')
  int? countAdults;
  @override
  @JsonKey(name: 'count_children')
  int? countChildren;
  @override
  @JsonKey(name: 'total_price')
  int? totalPrice;
  @override
  int? price;
  @override
  @JsonKey(name: 'free_places')
  int? freePlaces;
  @override
  @JsonKey(name: 'from_date')
  String? fromDate;
  @override
  @JsonKey(name: 'to_date')
  String? toDate;
  @override
  List<String>? images;
  @override
  List<Comforts>? comforts;
  @override
  @JsonKey(defaultValue: false)
  bool? showDetail;

  @override
  String toString() {
    return 'Rooms(id: $id, title: $title, description: $description, roomLeft: $roomLeft, isBusy: $isBusy, countAdults: $countAdults, countChildren: $countChildren, totalPrice: $totalPrice, price: $price, freePlaces: $freePlaces, fromDate: $fromDate, toDate: $toDate, images: $images, comforts: $comforts, showDetail: $showDetail)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RoomsCopyWith<_$_Rooms> get copyWith =>
      __$$_RoomsCopyWithImpl<_$_Rooms>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RoomsToJson(
      this,
    );
  }
}

abstract class _Rooms implements Rooms {
  factory _Rooms(
      {required int id,
      String? title,
      String? description,
      @JsonKey(name: 'room_left') int? roomLeft,
      @JsonKey(name: 'is_busy') bool? isBusy,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      @JsonKey(name: 'total_price') int? totalPrice,
      int? price,
      @JsonKey(name: 'free_places') int? freePlaces,
      @JsonKey(name: 'from_date') String? fromDate,
      @JsonKey(name: 'to_date') String? toDate,
      List<String>? images,
      List<Comforts>? comforts,
      @JsonKey(defaultValue: false) bool? showDetail}) = _$_Rooms;

  factory _Rooms.fromJson(Map<String, dynamic> json) = _$_Rooms.fromJson;

  @override
  int get id;
  set id(int value);
  @override // @JsonKey(name: 'sanatorium_id') required int sanatoriumId,
  String?
      get title; // @JsonKey(name: 'sanatorium_id') required int sanatoriumId,
  set title(String? value);
  @override
  String? get description;
  set description(String? value);
  @override
  @JsonKey(name: 'room_left')
  int? get roomLeft;
  @JsonKey(name: 'room_left')
  set roomLeft(int? value);
  @override
  @JsonKey(name: 'is_busy')
  bool? get isBusy;
  @JsonKey(name: 'is_busy')
  set isBusy(bool? value);
  @override
  @JsonKey(name: 'count_adults')
  int? get countAdults;
  @JsonKey(name: 'count_adults')
  set countAdults(int? value);
  @override
  @JsonKey(name: 'count_children')
  int? get countChildren;
  @JsonKey(name: 'count_children')
  set countChildren(int? value);
  @override
  @JsonKey(name: 'total_price')
  int? get totalPrice;
  @JsonKey(name: 'total_price')
  set totalPrice(int? value);
  @override
  int? get price;
  set price(int? value);
  @override
  @JsonKey(name: 'free_places')
  int? get freePlaces;
  @JsonKey(name: 'free_places')
  set freePlaces(int? value);
  @override
  @JsonKey(name: 'from_date')
  String? get fromDate;
  @JsonKey(name: 'from_date')
  set fromDate(String? value);
  @override
  @JsonKey(name: 'to_date')
  String? get toDate;
  @JsonKey(name: 'to_date')
  set toDate(String? value);
  @override
  List<String>? get images;
  set images(List<String>? value);
  @override
  List<Comforts>? get comforts;
  set comforts(List<Comforts>? value);
  @override
  @JsonKey(defaultValue: false)
  bool? get showDetail;
  @JsonKey(defaultValue: false)
  set showDetail(bool? value);
  @override
  @JsonKey(ignore: true)
  _$$_RoomsCopyWith<_$_Rooms> get copyWith =>
      throw _privateConstructorUsedError;
}
