// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rooms.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Rooms _$$_RoomsFromJson(Map<String, dynamic> json) => _$_Rooms(
      id: json['id'] as int,
      title: json['title'] as String?,
      description: json['description'] as String?,
      roomLeft: json['room_left'] as int?,
      isBusy: json['is_busy'] as bool?,
      countAdults: json['count_adults'] as int?,
      countChildren: json['count_children'] as int?,
      totalPrice: json['total_price'] as int?,
      price: json['price'] as int?,
      freePlaces: json['free_places'] as int?,
      fromDate: json['from_date'] as String?,
      toDate: json['to_date'] as String?,
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
      comforts: (json['comforts'] as List<dynamic>?)
          ?.map((e) => Comforts.fromJson(e as Map<String, dynamic>))
          .toList(),
      showDetail: json['showDetail'] as bool? ?? false,
    );

Map<String, dynamic> _$$_RoomsToJson(_$_Rooms instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'room_left': instance.roomLeft,
      'is_busy': instance.isBusy,
      'count_adults': instance.countAdults,
      'count_children': instance.countChildren,
      'total_price': instance.totalPrice,
      'price': instance.price,
      'free_places': instance.freePlaces,
      'from_date': instance.fromDate,
      'to_date': instance.toDate,
      'images': instance.images,
      'comforts': instance.comforts,
      'showDetail': instance.showDetail,
    };
