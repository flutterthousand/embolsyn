// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'show_sanatorium.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ShowSanatorium _$ShowSanatoriumFromJson(Map<String, dynamic> json) {
  return _ShowSanatorium.fromJson(json);
}

/// @nodoc
mixin _$ShowSanatorium {
  int get id => throw _privateConstructorUsedError;
  String? get city => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  int? get price => throw _privateConstructorUsedError;
  String? get address => throw _privateConstructorUsedError;
  String? get phone => throw _privateConstructorUsedError;
  String? get site => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  List<String>? get images => throw _privateConstructorUsedError;
  List<Rooms>? get rooms => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ShowSanatoriumCopyWith<ShowSanatorium> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShowSanatoriumCopyWith<$Res> {
  factory $ShowSanatoriumCopyWith(
          ShowSanatorium value, $Res Function(ShowSanatorium) then) =
      _$ShowSanatoriumCopyWithImpl<$Res, ShowSanatorium>;
  @useResult
  $Res call(
      {int id,
      String? city,
      String? title,
      String? description,
      int? price,
      String? address,
      String? phone,
      String? site,
      String? email,
      List<String>? images,
      List<Rooms>? rooms});
}

/// @nodoc
class _$ShowSanatoriumCopyWithImpl<$Res, $Val extends ShowSanatorium>
    implements $ShowSanatoriumCopyWith<$Res> {
  _$ShowSanatoriumCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? city = freezed,
    Object? title = freezed,
    Object? description = freezed,
    Object? price = freezed,
    Object? address = freezed,
    Object? phone = freezed,
    Object? site = freezed,
    Object? email = freezed,
    Object? images = freezed,
    Object? rooms = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      city: freezed == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      site: freezed == site
          ? _value.site
          : site // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      images: freezed == images
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      rooms: freezed == rooms
          ? _value.rooms
          : rooms // ignore: cast_nullable_to_non_nullable
              as List<Rooms>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ShowSanatoriumCopyWith<$Res>
    implements $ShowSanatoriumCopyWith<$Res> {
  factory _$$_ShowSanatoriumCopyWith(
          _$_ShowSanatorium value, $Res Function(_$_ShowSanatorium) then) =
      __$$_ShowSanatoriumCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String? city,
      String? title,
      String? description,
      int? price,
      String? address,
      String? phone,
      String? site,
      String? email,
      List<String>? images,
      List<Rooms>? rooms});
}

/// @nodoc
class __$$_ShowSanatoriumCopyWithImpl<$Res>
    extends _$ShowSanatoriumCopyWithImpl<$Res, _$_ShowSanatorium>
    implements _$$_ShowSanatoriumCopyWith<$Res> {
  __$$_ShowSanatoriumCopyWithImpl(
      _$_ShowSanatorium _value, $Res Function(_$_ShowSanatorium) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? city = freezed,
    Object? title = freezed,
    Object? description = freezed,
    Object? price = freezed,
    Object? address = freezed,
    Object? phone = freezed,
    Object? site = freezed,
    Object? email = freezed,
    Object? images = freezed,
    Object? rooms = freezed,
  }) {
    return _then(_$_ShowSanatorium(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      city: freezed == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      site: freezed == site
          ? _value.site
          : site // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      images: freezed == images
          ? _value._images
          : images // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      rooms: freezed == rooms
          ? _value._rooms
          : rooms // ignore: cast_nullable_to_non_nullable
              as List<Rooms>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ShowSanatorium implements _ShowSanatorium {
  const _$_ShowSanatorium(
      {required this.id,
      this.city,
      this.title,
      this.description,
      this.price,
      this.address,
      this.phone,
      this.site,
      this.email,
      final List<String>? images,
      final List<Rooms>? rooms})
      : _images = images,
        _rooms = rooms;

  factory _$_ShowSanatorium.fromJson(Map<String, dynamic> json) =>
      _$$_ShowSanatoriumFromJson(json);

  @override
  final int id;
  @override
  final String? city;
  @override
  final String? title;
  @override
  final String? description;
  @override
  final int? price;
  @override
  final String? address;
  @override
  final String? phone;
  @override
  final String? site;
  @override
  final String? email;
  final List<String>? _images;
  @override
  List<String>? get images {
    final value = _images;
    if (value == null) return null;
    if (_images is EqualUnmodifiableListView) return _images;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Rooms>? _rooms;
  @override
  List<Rooms>? get rooms {
    final value = _rooms;
    if (value == null) return null;
    if (_rooms is EqualUnmodifiableListView) return _rooms;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'ShowSanatorium(id: $id, city: $city, title: $title, description: $description, price: $price, address: $address, phone: $phone, site: $site, email: $email, images: $images, rooms: $rooms)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShowSanatorium &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.price, price) || other.price == price) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.site, site) || other.site == site) &&
            (identical(other.email, email) || other.email == email) &&
            const DeepCollectionEquality().equals(other._images, _images) &&
            const DeepCollectionEquality().equals(other._rooms, _rooms));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      city,
      title,
      description,
      price,
      address,
      phone,
      site,
      email,
      const DeepCollectionEquality().hash(_images),
      const DeepCollectionEquality().hash(_rooms));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ShowSanatoriumCopyWith<_$_ShowSanatorium> get copyWith =>
      __$$_ShowSanatoriumCopyWithImpl<_$_ShowSanatorium>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ShowSanatoriumToJson(
      this,
    );
  }
}

abstract class _ShowSanatorium implements ShowSanatorium {
  const factory _ShowSanatorium(
      {required final int id,
      final String? city,
      final String? title,
      final String? description,
      final int? price,
      final String? address,
      final String? phone,
      final String? site,
      final String? email,
      final List<String>? images,
      final List<Rooms>? rooms}) = _$_ShowSanatorium;

  factory _ShowSanatorium.fromJson(Map<String, dynamic> json) =
      _$_ShowSanatorium.fromJson;

  @override
  int get id;
  @override
  String? get city;
  @override
  String? get title;
  @override
  String? get description;
  @override
  int? get price;
  @override
  String? get address;
  @override
  String? get phone;
  @override
  String? get site;
  @override
  String? get email;
  @override
  List<String>? get images;
  @override
  List<Rooms>? get rooms;
  @override
  @JsonKey(ignore: true)
  _$$_ShowSanatoriumCopyWith<_$_ShowSanatorium> get copyWith =>
      throw _privateConstructorUsedError;
}
