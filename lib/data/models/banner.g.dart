// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Banner _$$_BannerFromJson(Map<String, dynamic> json) => _$_Banner(
      id: json['id'] as int,
      cooperate: json['cooperate'] as String?,
      imagePath: json['imagePath'] as String?,
    );

Map<String, dynamic> _$$_BannerToJson(_$_Banner instance) => <String, dynamic>{
      'id': instance.id,
      'cooperate': instance.cooperate,
      'imagePath': instance.imagePath,
    };
