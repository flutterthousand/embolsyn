// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertising.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Advertising _$$_AdvertisingFromJson(Map<String, dynamic> json) =>
    _$_Advertising(
      id: json['id'] as int,
      sanatoriumId: json['sanatorium_id'] as int?,
      description: json['description'] as String?,
      image: json['image'] as String?,
      sanatorium: json['sanatorium'] as String?,
      price: json['price'] as int?,
      createdAt: json['created_at'] as String?,
      updateddAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_AdvertisingToJson(_$_Advertising instance) =>
    <String, dynamic>{
      'id': instance.id,
      'sanatorium_id': instance.sanatoriumId,
      'description': instance.description,
      'image': instance.image,
      'sanatorium': instance.sanatorium,
      'price': instance.price,
      'created_at': instance.createdAt,
      'updated_at': instance.updateddAt,
    };
