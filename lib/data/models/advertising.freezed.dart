// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'advertising.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Advertising _$AdvertisingFromJson(Map<String, dynamic> json) {
  return _Advertising.fromJson(json);
}

/// @nodoc
mixin _$Advertising {
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'sanatorium_id')
  int? get sanatoriumId => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;
  String? get sanatorium => throw _privateConstructorUsedError;
  int? get price => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updateddAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AdvertisingCopyWith<Advertising> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AdvertisingCopyWith<$Res> {
  factory $AdvertisingCopyWith(
          Advertising value, $Res Function(Advertising) then) =
      _$AdvertisingCopyWithImpl<$Res, Advertising>;
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'sanatorium_id') int? sanatoriumId,
      String? description,
      String? image,
      String? sanatorium,
      int? price,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updateddAt});
}

/// @nodoc
class _$AdvertisingCopyWithImpl<$Res, $Val extends Advertising>
    implements $AdvertisingCopyWith<$Res> {
  _$AdvertisingCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sanatoriumId = freezed,
    Object? description = freezed,
    Object? image = freezed,
    Object? sanatorium = freezed,
    Object? price = freezed,
    Object? createdAt = freezed,
    Object? updateddAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sanatoriumId: freezed == sanatoriumId
          ? _value.sanatoriumId
          : sanatoriumId // ignore: cast_nullable_to_non_nullable
              as int?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      sanatorium: freezed == sanatorium
          ? _value.sanatorium
          : sanatorium // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updateddAt: freezed == updateddAt
          ? _value.updateddAt
          : updateddAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AdvertisingCopyWith<$Res>
    implements $AdvertisingCopyWith<$Res> {
  factory _$$_AdvertisingCopyWith(
          _$_Advertising value, $Res Function(_$_Advertising) then) =
      __$$_AdvertisingCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'sanatorium_id') int? sanatoriumId,
      String? description,
      String? image,
      String? sanatorium,
      int? price,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updateddAt});
}

/// @nodoc
class __$$_AdvertisingCopyWithImpl<$Res>
    extends _$AdvertisingCopyWithImpl<$Res, _$_Advertising>
    implements _$$_AdvertisingCopyWith<$Res> {
  __$$_AdvertisingCopyWithImpl(
      _$_Advertising _value, $Res Function(_$_Advertising) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sanatoriumId = freezed,
    Object? description = freezed,
    Object? image = freezed,
    Object? sanatorium = freezed,
    Object? price = freezed,
    Object? createdAt = freezed,
    Object? updateddAt = freezed,
  }) {
    return _then(_$_Advertising(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sanatoriumId: freezed == sanatoriumId
          ? _value.sanatoriumId
          : sanatoriumId // ignore: cast_nullable_to_non_nullable
              as int?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      sanatorium: freezed == sanatorium
          ? _value.sanatorium
          : sanatorium // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updateddAt: freezed == updateddAt
          ? _value.updateddAt
          : updateddAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Advertising implements _Advertising {
  const _$_Advertising(
      {required this.id,
      @JsonKey(name: 'sanatorium_id') this.sanatoriumId,
      this.description,
      this.image,
      this.sanatorium,
      this.price,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updateddAt});

  factory _$_Advertising.fromJson(Map<String, dynamic> json) =>
      _$$_AdvertisingFromJson(json);

  @override
  final int id;
  @override
  @JsonKey(name: 'sanatorium_id')
  final int? sanatoriumId;
  @override
  final String? description;
  @override
  final String? image;
  @override
  final String? sanatorium;
  @override
  final int? price;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updateddAt;

  @override
  String toString() {
    return 'Advertising(id: $id, sanatoriumId: $sanatoriumId, description: $description, image: $image, sanatorium: $sanatorium, price: $price, createdAt: $createdAt, updateddAt: $updateddAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Advertising &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.sanatoriumId, sanatoriumId) ||
                other.sanatoriumId == sanatoriumId) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.sanatorium, sanatorium) ||
                other.sanatorium == sanatorium) &&
            (identical(other.price, price) || other.price == price) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updateddAt, updateddAt) ||
                other.updateddAt == updateddAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, sanatoriumId, description,
      image, sanatorium, price, createdAt, updateddAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AdvertisingCopyWith<_$_Advertising> get copyWith =>
      __$$_AdvertisingCopyWithImpl<_$_Advertising>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AdvertisingToJson(
      this,
    );
  }
}

abstract class _Advertising implements Advertising {
  const factory _Advertising(
      {required final int id,
      @JsonKey(name: 'sanatorium_id') final int? sanatoriumId,
      final String? description,
      final String? image,
      final String? sanatorium,
      final int? price,
      @JsonKey(name: 'created_at') final String? createdAt,
      @JsonKey(name: 'updated_at') final String? updateddAt}) = _$_Advertising;

  factory _Advertising.fromJson(Map<String, dynamic> json) =
      _$_Advertising.fromJson;

  @override
  int get id;
  @override
  @JsonKey(name: 'sanatorium_id')
  int? get sanatoriumId;
  @override
  String? get description;
  @override
  String? get image;
  @override
  String? get sanatorium;
  @override
  int? get price;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updateddAt;
  @override
  @JsonKey(ignore: true)
  _$$_AdvertisingCopyWith<_$_Advertising> get copyWith =>
      throw _privateConstructorUsedError;
}
