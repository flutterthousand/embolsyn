// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'data.freezed.dart';
part 'data.g.dart';

@freezed
 class Data with _$Data {
  const factory Data({
    @JsonKey(name: 'pg_status') String? status,
    @JsonKey(name: 'pg_payment_id') String? paymentId,
    @JsonKey(name: 'pg_salt') String? salt,
    @JsonKey(name: 'pg_redirect_url') String? url,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) =>
      _$DataFromJson(json);
}
