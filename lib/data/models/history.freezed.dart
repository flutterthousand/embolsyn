// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'history.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

History _$HistoryFromJson(Map<String, dynamic> json) {
  return _History.fromJson(json);
}

/// @nodoc
mixin _$History {
  @JsonKey(name: 'order_id')
  int? get orderId => throw _privateConstructorUsedError;
  @JsonKey(name: 'sanatorium_name')
  String? get sanatoriumName => throw _privateConstructorUsedError;
  @JsonKey(name: 'sanatorium_room_name')
  String? get sanatoriumRoomName => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_adults')
  int? get countAdults => throw _privateConstructorUsedError;
  @JsonKey(name: 'count_children')
  int? get countChildren => throw _privateConstructorUsedError;
  @JsonKey(name: 'from_date')
  String? get fromDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'to_date')
  String? get toDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'total_price')
  int? get totalPrice => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HistoryCopyWith<History> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HistoryCopyWith<$Res> {
  factory $HistoryCopyWith(History value, $Res Function(History) then) =
      _$HistoryCopyWithImpl<$Res, History>;
  @useResult
  $Res call(
      {@JsonKey(name: 'order_id') int? orderId,
      @JsonKey(name: 'sanatorium_name') String? sanatoriumName,
      @JsonKey(name: 'sanatorium_room_name') String? sanatoriumRoomName,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      @JsonKey(name: 'from_date') String? fromDate,
      @JsonKey(name: 'to_date') String? toDate,
      @JsonKey(name: 'total_price') int? totalPrice});
}

/// @nodoc
class _$HistoryCopyWithImpl<$Res, $Val extends History>
    implements $HistoryCopyWith<$Res> {
  _$HistoryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? orderId = freezed,
    Object? sanatoriumName = freezed,
    Object? sanatoriumRoomName = freezed,
    Object? countAdults = freezed,
    Object? countChildren = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? totalPrice = freezed,
  }) {
    return _then(_value.copyWith(
      orderId: freezed == orderId
          ? _value.orderId
          : orderId // ignore: cast_nullable_to_non_nullable
              as int?,
      sanatoriumName: freezed == sanatoriumName
          ? _value.sanatoriumName
          : sanatoriumName // ignore: cast_nullable_to_non_nullable
              as String?,
      sanatoriumRoomName: freezed == sanatoriumRoomName
          ? _value.sanatoriumRoomName
          : sanatoriumRoomName // ignore: cast_nullable_to_non_nullable
              as String?,
      countAdults: freezed == countAdults
          ? _value.countAdults
          : countAdults // ignore: cast_nullable_to_non_nullable
              as int?,
      countChildren: freezed == countChildren
          ? _value.countChildren
          : countChildren // ignore: cast_nullable_to_non_nullable
              as int?,
      fromDate: freezed == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String?,
      toDate: freezed == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String?,
      totalPrice: freezed == totalPrice
          ? _value.totalPrice
          : totalPrice // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_HistoryCopyWith<$Res> implements $HistoryCopyWith<$Res> {
  factory _$$_HistoryCopyWith(
          _$_History value, $Res Function(_$_History) then) =
      __$$_HistoryCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'order_id') int? orderId,
      @JsonKey(name: 'sanatorium_name') String? sanatoriumName,
      @JsonKey(name: 'sanatorium_room_name') String? sanatoriumRoomName,
      @JsonKey(name: 'count_adults') int? countAdults,
      @JsonKey(name: 'count_children') int? countChildren,
      @JsonKey(name: 'from_date') String? fromDate,
      @JsonKey(name: 'to_date') String? toDate,
      @JsonKey(name: 'total_price') int? totalPrice});
}

/// @nodoc
class __$$_HistoryCopyWithImpl<$Res>
    extends _$HistoryCopyWithImpl<$Res, _$_History>
    implements _$$_HistoryCopyWith<$Res> {
  __$$_HistoryCopyWithImpl(_$_History _value, $Res Function(_$_History) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? orderId = freezed,
    Object? sanatoriumName = freezed,
    Object? sanatoriumRoomName = freezed,
    Object? countAdults = freezed,
    Object? countChildren = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? totalPrice = freezed,
  }) {
    return _then(_$_History(
      orderId: freezed == orderId
          ? _value.orderId
          : orderId // ignore: cast_nullable_to_non_nullable
              as int?,
      sanatoriumName: freezed == sanatoriumName
          ? _value.sanatoriumName
          : sanatoriumName // ignore: cast_nullable_to_non_nullable
              as String?,
      sanatoriumRoomName: freezed == sanatoriumRoomName
          ? _value.sanatoriumRoomName
          : sanatoriumRoomName // ignore: cast_nullable_to_non_nullable
              as String?,
      countAdults: freezed == countAdults
          ? _value.countAdults
          : countAdults // ignore: cast_nullable_to_non_nullable
              as int?,
      countChildren: freezed == countChildren
          ? _value.countChildren
          : countChildren // ignore: cast_nullable_to_non_nullable
              as int?,
      fromDate: freezed == fromDate
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String?,
      toDate: freezed == toDate
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String?,
      totalPrice: freezed == totalPrice
          ? _value.totalPrice
          : totalPrice // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_History implements _History {
  const _$_History(
      {@JsonKey(name: 'order_id') this.orderId,
      @JsonKey(name: 'sanatorium_name') this.sanatoriumName,
      @JsonKey(name: 'sanatorium_room_name') this.sanatoriumRoomName,
      @JsonKey(name: 'count_adults') this.countAdults,
      @JsonKey(name: 'count_children') this.countChildren,
      @JsonKey(name: 'from_date') this.fromDate,
      @JsonKey(name: 'to_date') this.toDate,
      @JsonKey(name: 'total_price') this.totalPrice});

  factory _$_History.fromJson(Map<String, dynamic> json) =>
      _$$_HistoryFromJson(json);

  @override
  @JsonKey(name: 'order_id')
  final int? orderId;
  @override
  @JsonKey(name: 'sanatorium_name')
  final String? sanatoriumName;
  @override
  @JsonKey(name: 'sanatorium_room_name')
  final String? sanatoriumRoomName;
  @override
  @JsonKey(name: 'count_adults')
  final int? countAdults;
  @override
  @JsonKey(name: 'count_children')
  final int? countChildren;
  @override
  @JsonKey(name: 'from_date')
  final String? fromDate;
  @override
  @JsonKey(name: 'to_date')
  final String? toDate;
  @override
  @JsonKey(name: 'total_price')
  final int? totalPrice;

  @override
  String toString() {
    return 'History(orderId: $orderId, sanatoriumName: $sanatoriumName, sanatoriumRoomName: $sanatoriumRoomName, countAdults: $countAdults, countChildren: $countChildren, fromDate: $fromDate, toDate: $toDate, totalPrice: $totalPrice)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_History &&
            (identical(other.orderId, orderId) || other.orderId == orderId) &&
            (identical(other.sanatoriumName, sanatoriumName) ||
                other.sanatoriumName == sanatoriumName) &&
            (identical(other.sanatoriumRoomName, sanatoriumRoomName) ||
                other.sanatoriumRoomName == sanatoriumRoomName) &&
            (identical(other.countAdults, countAdults) ||
                other.countAdults == countAdults) &&
            (identical(other.countChildren, countChildren) ||
                other.countChildren == countChildren) &&
            (identical(other.fromDate, fromDate) ||
                other.fromDate == fromDate) &&
            (identical(other.toDate, toDate) || other.toDate == toDate) &&
            (identical(other.totalPrice, totalPrice) ||
                other.totalPrice == totalPrice));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      orderId,
      sanatoriumName,
      sanatoriumRoomName,
      countAdults,
      countChildren,
      fromDate,
      toDate,
      totalPrice);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_HistoryCopyWith<_$_History> get copyWith =>
      __$$_HistoryCopyWithImpl<_$_History>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_HistoryToJson(
      this,
    );
  }
}

abstract class _History implements History {
  const factory _History(
      {@JsonKey(name: 'order_id') final int? orderId,
      @JsonKey(name: 'sanatorium_name') final String? sanatoriumName,
      @JsonKey(name: 'sanatorium_room_name') final String? sanatoriumRoomName,
      @JsonKey(name: 'count_adults') final int? countAdults,
      @JsonKey(name: 'count_children') final int? countChildren,
      @JsonKey(name: 'from_date') final String? fromDate,
      @JsonKey(name: 'to_date') final String? toDate,
      @JsonKey(name: 'total_price') final int? totalPrice}) = _$_History;

  factory _History.fromJson(Map<String, dynamic> json) = _$_History.fromJson;

  @override
  @JsonKey(name: 'order_id')
  int? get orderId;
  @override
  @JsonKey(name: 'sanatorium_name')
  String? get sanatoriumName;
  @override
  @JsonKey(name: 'sanatorium_room_name')
  String? get sanatoriumRoomName;
  @override
  @JsonKey(name: 'count_adults')
  int? get countAdults;
  @override
  @JsonKey(name: 'count_children')
  int? get countChildren;
  @override
  @JsonKey(name: 'from_date')
  String? get fromDate;
  @override
  @JsonKey(name: 'to_date')
  String? get toDate;
  @override
  @JsonKey(name: 'total_price')
  int? get totalPrice;
  @override
  @JsonKey(ignore: true)
  _$$_HistoryCopyWith<_$_History> get copyWith =>
      throw _privateConstructorUsedError;
}
