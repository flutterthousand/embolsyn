// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booked_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookedUser _$$_BookedUserFromJson(Map<String, dynamic> json) =>
    _$_BookedUser(
      id: json['id'] as int,
      bookingId: json['booking_id'] as int?,
      name: json['name'] as String?,
      surname: json['surname'] as String?,
      gender: json['gender'] as String?,
      dateBirth: json['date_birth'] as String?,
      nationality: json['nationality'] as String?,
      validity: json['validity'] as String?,
      iin: json['iin'] as String?,
      type: json['type'] as String?,
      createdAt: json['created_at'] as String?,
      updateddAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_BookedUserToJson(_$_BookedUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'booking_id': instance.bookingId,
      'name': instance.name,
      'surname': instance.surname,
      'gender': instance.gender,
      'date_birth': instance.dateBirth,
      'nationality': instance.nationality,
      'validity': instance.validity,
      'iin': instance.iin,
      'type': instance.type,
      'created_at': instance.createdAt,
      'updated_at': instance.updateddAt,
    };
