// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'pdf.freezed.dart';
part 'pdf.g.dart';

@freezed
 class Pdf with _$Pdf {
  const factory Pdf({

    String? path,
    

    // City city,
  }) = _Pdf;

  factory Pdf.fromJson(Map<String, dynamic> json) =>
      _$PdfFromJson(json);
}
