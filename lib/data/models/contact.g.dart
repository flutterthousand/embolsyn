// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Contacts _$$_ContactsFromJson(Map<String, dynamic> json) => _$_Contacts(
      id: json['id'] as int?,
      name: json['name'] as String?,
      subtitle: json['sub_title'] as String?,
      vk: json['vk'] as String?,
      facebook: json['facebook'] as String?,
      instagram: json['instagram'] as String?,
      twitter: json['twitter'] as String?,
      phone: json['phone'] as String?,
      website: json['website'] as String?,
      address: json['address'] as String?,
      image: json['image'] as String?,
      email: json['email'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );

Map<String, dynamic> _$$_ContactsToJson(_$_Contacts instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'sub_title': instance.subtitle,
      'vk': instance.vk,
      'facebook': instance.facebook,
      'instagram': instance.instagram,
      'twitter': instance.twitter,
      'phone': instance.phone,
      'website': instance.website,
      'address': instance.address,
      'image': instance.image,
      'email': instance.email,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };
