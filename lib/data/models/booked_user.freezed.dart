// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'booked_user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookedUser _$BookedUserFromJson(Map<String, dynamic> json) {
  return _BookedUser.fromJson(json);
}

/// @nodoc
mixin _$BookedUser {
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'booking_id')
  int? get bookingId => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get surname => throw _privateConstructorUsedError;
  String? get gender => throw _privateConstructorUsedError;
  @JsonKey(name: 'date_birth')
  String? get dateBirth => throw _privateConstructorUsedError;
  String? get nationality => throw _privateConstructorUsedError;
  String? get validity => throw _privateConstructorUsedError;
  String? get iin => throw _privateConstructorUsedError;
  String? get type => throw _privateConstructorUsedError;
  @JsonKey(name: 'created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: 'updated_at')
  String? get updateddAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookedUserCopyWith<BookedUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookedUserCopyWith<$Res> {
  factory $BookedUserCopyWith(
          BookedUser value, $Res Function(BookedUser) then) =
      _$BookedUserCopyWithImpl<$Res, BookedUser>;
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'booking_id') int? bookingId,
      String? name,
      String? surname,
      String? gender,
      @JsonKey(name: 'date_birth') String? dateBirth,
      String? nationality,
      String? validity,
      String? iin,
      String? type,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updateddAt});
}

/// @nodoc
class _$BookedUserCopyWithImpl<$Res, $Val extends BookedUser>
    implements $BookedUserCopyWith<$Res> {
  _$BookedUserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? bookingId = freezed,
    Object? name = freezed,
    Object? surname = freezed,
    Object? gender = freezed,
    Object? dateBirth = freezed,
    Object? nationality = freezed,
    Object? validity = freezed,
    Object? iin = freezed,
    Object? type = freezed,
    Object? createdAt = freezed,
    Object? updateddAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      bookingId: freezed == bookingId
          ? _value.bookingId
          : bookingId // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      surname: freezed == surname
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      dateBirth: freezed == dateBirth
          ? _value.dateBirth
          : dateBirth // ignore: cast_nullable_to_non_nullable
              as String?,
      nationality: freezed == nationality
          ? _value.nationality
          : nationality // ignore: cast_nullable_to_non_nullable
              as String?,
      validity: freezed == validity
          ? _value.validity
          : validity // ignore: cast_nullable_to_non_nullable
              as String?,
      iin: freezed == iin
          ? _value.iin
          : iin // ignore: cast_nullable_to_non_nullable
              as String?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updateddAt: freezed == updateddAt
          ? _value.updateddAt
          : updateddAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BookedUserCopyWith<$Res>
    implements $BookedUserCopyWith<$Res> {
  factory _$$_BookedUserCopyWith(
          _$_BookedUser value, $Res Function(_$_BookedUser) then) =
      __$$_BookedUserCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: 'booking_id') int? bookingId,
      String? name,
      String? surname,
      String? gender,
      @JsonKey(name: 'date_birth') String? dateBirth,
      String? nationality,
      String? validity,
      String? iin,
      String? type,
      @JsonKey(name: 'created_at') String? createdAt,
      @JsonKey(name: 'updated_at') String? updateddAt});
}

/// @nodoc
class __$$_BookedUserCopyWithImpl<$Res>
    extends _$BookedUserCopyWithImpl<$Res, _$_BookedUser>
    implements _$$_BookedUserCopyWith<$Res> {
  __$$_BookedUserCopyWithImpl(
      _$_BookedUser _value, $Res Function(_$_BookedUser) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? bookingId = freezed,
    Object? name = freezed,
    Object? surname = freezed,
    Object? gender = freezed,
    Object? dateBirth = freezed,
    Object? nationality = freezed,
    Object? validity = freezed,
    Object? iin = freezed,
    Object? type = freezed,
    Object? createdAt = freezed,
    Object? updateddAt = freezed,
  }) {
    return _then(_$_BookedUser(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      bookingId: freezed == bookingId
          ? _value.bookingId
          : bookingId // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      surname: freezed == surname
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      dateBirth: freezed == dateBirth
          ? _value.dateBirth
          : dateBirth // ignore: cast_nullable_to_non_nullable
              as String?,
      nationality: freezed == nationality
          ? _value.nationality
          : nationality // ignore: cast_nullable_to_non_nullable
              as String?,
      validity: freezed == validity
          ? _value.validity
          : validity // ignore: cast_nullable_to_non_nullable
              as String?,
      iin: freezed == iin
          ? _value.iin
          : iin // ignore: cast_nullable_to_non_nullable
              as String?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updateddAt: freezed == updateddAt
          ? _value.updateddAt
          : updateddAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookedUser implements _BookedUser {
  const _$_BookedUser(
      {required this.id,
      @JsonKey(name: 'booking_id') this.bookingId,
      this.name,
      this.surname,
      this.gender,
      @JsonKey(name: 'date_birth') this.dateBirth,
      this.nationality,
      this.validity,
      this.iin,
      this.type,
      @JsonKey(name: 'created_at') this.createdAt,
      @JsonKey(name: 'updated_at') this.updateddAt});

  factory _$_BookedUser.fromJson(Map<String, dynamic> json) =>
      _$$_BookedUserFromJson(json);

  @override
  final int id;
  @override
  @JsonKey(name: 'booking_id')
  final int? bookingId;
  @override
  final String? name;
  @override
  final String? surname;
  @override
  final String? gender;
  @override
  @JsonKey(name: 'date_birth')
  final String? dateBirth;
  @override
  final String? nationality;
  @override
  final String? validity;
  @override
  final String? iin;
  @override
  final String? type;
  @override
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @override
  @JsonKey(name: 'updated_at')
  final String? updateddAt;

  @override
  String toString() {
    return 'BookedUser(id: $id, bookingId: $bookingId, name: $name, surname: $surname, gender: $gender, dateBirth: $dateBirth, nationality: $nationality, validity: $validity, iin: $iin, type: $type, createdAt: $createdAt, updateddAt: $updateddAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BookedUser &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.bookingId, bookingId) ||
                other.bookingId == bookingId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.surname, surname) || other.surname == surname) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.dateBirth, dateBirth) ||
                other.dateBirth == dateBirth) &&
            (identical(other.nationality, nationality) ||
                other.nationality == nationality) &&
            (identical(other.validity, validity) ||
                other.validity == validity) &&
            (identical(other.iin, iin) || other.iin == iin) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updateddAt, updateddAt) ||
                other.updateddAt == updateddAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      bookingId,
      name,
      surname,
      gender,
      dateBirth,
      nationality,
      validity,
      iin,
      type,
      createdAt,
      updateddAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BookedUserCopyWith<_$_BookedUser> get copyWith =>
      __$$_BookedUserCopyWithImpl<_$_BookedUser>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookedUserToJson(
      this,
    );
  }
}

abstract class _BookedUser implements BookedUser {
  const factory _BookedUser(
      {required final int id,
      @JsonKey(name: 'booking_id') final int? bookingId,
      final String? name,
      final String? surname,
      final String? gender,
      @JsonKey(name: 'date_birth') final String? dateBirth,
      final String? nationality,
      final String? validity,
      final String? iin,
      final String? type,
      @JsonKey(name: 'created_at') final String? createdAt,
      @JsonKey(name: 'updated_at') final String? updateddAt}) = _$_BookedUser;

  factory _BookedUser.fromJson(Map<String, dynamic> json) =
      _$_BookedUser.fromJson;

  @override
  int get id;
  @override
  @JsonKey(name: 'booking_id')
  int? get bookingId;
  @override
  String? get name;
  @override
  String? get surname;
  @override
  String? get gender;
  @override
  @JsonKey(name: 'date_birth')
  String? get dateBirth;
  @override
  String? get nationality;
  @override
  String? get validity;
  @override
  String? get iin;
  @override
  String? get type;
  @override
  @JsonKey(name: 'created_at')
  String? get createdAt;
  @override
  @JsonKey(name: 'updated_at')
  String? get updateddAt;
  @override
  @JsonKey(ignore: true)
  _$$_BookedUserCopyWith<_$_BookedUser> get copyWith =>
      throw _privateConstructorUsedError;
}
