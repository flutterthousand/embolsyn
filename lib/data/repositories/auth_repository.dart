// ignore_for_file: avoid_dynamic_calls, unused_local_variable, avoid_print

import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/user.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:embolsyn/data/repositories/notification.dart';

class AuthRepository {
  final Prefs _prefs = Prefs();

  Future<void> auth({required String phone}) async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    print('auth');
    try {
      final response =
          await dio.post('$SERVER_HOST/api/auth', data: {'phone': phone});
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<void> register({
    required String phone,
    required String name,
    required String surname,
  }) async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    print('auth');
    try {
      final response = await dio.post(
        'http://185.116.193.154/api/register',
        data: {
          'phone': phone,
          'name': name,
          'surname': surname,
        },
      );
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<User> verify(String code) async {
    final dio = Dio();
// final type = await _prefs.getType();
// final deviceToken = await _prefs.getDeviceToken();
    final String? deviceToken = await NotificationService().getDeviceToken();
    log('$deviceToken');
    dio.options.headers['Accept'] = "application/json";
    print('verify');
    try {
      final response = await dio.post(
        '$SERVER_HOST/api/verify',
        data: {
          'code': code,
          'device_token': deviceToken,
          // 'device_type': type,
        },
      );
      _prefs.saveToken(response.data['access_token'] as String);
      log('access_token ${response.data}');
      return User.fromJson(response.data['user'] as Map<String, dynamic>);
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      } else {
        log("${error.response!.data as Map<String, dynamic>}");
        throw ErrorException(
            message: (error.response!.data as Map<String, dynamic>)['message']
                as String,);
      }
      // final data = jsonDecode(error.response.toString());
    }
  }

  Future<void> logOut() async {
    final dio = Dio();
    try {
      final token = await _prefs.getToken();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Accept'] = "application/json";
      await Prefs().logOutUser();
      final response = await dio.post('$SERVER_HOST/api/logout');
      log(response.statusCode.toString());
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(
        message: data['message'].toString(),
      );
    }
  }

  Future<void> delete() async {
    final dio = Dio();
    try {
      final token = await _prefs.getToken();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Accept'] = "application/json";
      await Prefs().logOutUser();
      final response = await dio.post('$SERVER_HOST/api/profile/delete');
      log(response.statusCode.toString());
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(
        message: data['message'].toString(),
      );
    }
  }

  // Future<User> profile(String access) async {
  //   var dio = Dio();
  //   dio.options.headers['authorization'] = 'Bearer $access';
  //   dio.options.headers['Accept'] = "application/json";
  //   try {
  //     final response = await dio.post(SERVER_HOST + '/api/v1/profile');
  //     return User(
  //       id: response.data['id'] as int,
  //       name: response.data['name'] as String,
  //       phone: response.data['phone'] as String,
  //     );
  //   } on DioError catch (error) {
  //     final data = jsonDecode(error.response.toString());
  //     throw ErrorException(message: data['message'].toString());
  //   }
  // }

  // Future<String> getDebugToken() async {
  //   var dio = Dio();
  //   dio.options.headers['Accept'] = "application/json";
  //   try {
  //     final response = await dio.get(SERVER_HOST + '/api/debug');
  //     return response.data['token'] as String;
  //   } on DioError catch (error) {
  //     final data = jsonDecode(error.response.toString());
  //     throw ErrorException(message: data['message'].toString());
  //   }
  // }

  // Future passwordReset({required String phone}) async {
  //   var dio = Dio();
  //   dio.options.headers['Accept'] = "application/json";
  //   try {
  //     await dio
  //         .post(SERVER_HOST + '/api/v1/password-reset', data: {'phone': phone});
  //   } on DioError catch (error) {
  //     final data = jsonDecode(error.response.toString());
  //     throw ErrorException(message: data['message'].toString());
  //   }
  // }

  // Future passwordResetConfirmation(
  //     {required String phone, required String code}) async {
  //   var dio = Dio();
  //   dio.options.headers['Accept'] = "application/json";
  //   try {
  //     await dio.post(SERVER_HOST + '/api/v1/password-reset-confirmation',
  //         data: {'phone': phone, 'code': code});
  //   } on DioError catch (error) {
  //     final data = jsonDecode(error.response.toString());
  //     throw ErrorException(message: data['message'].toString());
  //   }
  // }

  // Future passwordResetUpdate(
  //     {required String phone,
  //     required String code,
  //     required String password,
  //     required String passwordConfirmation}) async {
  //   var dio = Dio();
  //   dio.options.headers['Accept'] = "application/json";
  //   try {
  //     await dio.post(SERVER_HOST + '/api/v1/password-reset-update', data: {
  //       'phone': phone,
  //       'code': code,
  //       'password': password,
  //       'password_confirmation': passwordConfirmation
  //     });
  //   } on DioError catch (error) {
  //     final data = jsonDecode(error.response.toString());
  //     throw ErrorException(message: data['message'].toString());
  //   }
  // }
}
