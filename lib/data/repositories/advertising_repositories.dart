// ignore_for_file: avoid_dynamic_calls

import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/advertising.dart';

class AdvertisingRepository {
  Future<List<Advertising>> getAdvertising() async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    try {
      final response = await dio.get(
        '$SERVER_HOST/api/advertising',
      );

      return (response.data as List)
          .map((x) => Advertising.fromJson(x as Map<String, dynamic>))
          .toList();
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}
