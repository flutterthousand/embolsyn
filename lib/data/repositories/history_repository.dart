// ignore_for_file: avoid_dynamic_calls, unused_local_variable

import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/history.dart';
import 'package:embolsyn/data/models/pdf.dart';
import 'package:embolsyn/data/models/user.dart';
import 'package:embolsyn/data/prefs.dart';

class HistoryRepositories {
  final Prefs _prefs = Prefs();

  Future<List<History>> getHistory() async {
    final dio = Dio();
    final User? user = await _prefs.getUserFromCacheNull();
    final int userId = user!.id;
    dio.options.headers['Accept'] = "application/json";
    try {
      final token = await _prefs.getToken();

      dio.options.headers['Authorization'] = 'Bearer $token';
      final response = await dio.get(
        '$SERVER_HOST/api/puchase-history?user_id=$userId',
      );
      return (response.data as List)
          .map((x) => History.fromJson(x as Map<String, dynamic>))
          .toList();
    } on DioError catch (error) {
      log(error.message);
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<Pdf> getPdf({
    required int id,
  }) async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    try {
      final response = await dio.get(
        '$SERVER_HOST/api/generate-pdf?order_id=$id',
      );

      return Pdf.fromJson(response.data as Map<String, dynamic>);
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}
