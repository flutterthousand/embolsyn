// import 'dart:convert';

// import 'package:dio/dio.dart';
// import 'package:embolsyn/data/exceptions.dart';
// import 'package:embolsyn/data/prefs.dart';

// class CalendarRepositories {
//   Future<List<DateTime>> getCalendar() async {
//     final dio = Dio();
//     final int? cityId = await Prefs().getCityId();
//     dio.options.headers['Accept'] = "application/json";
//     try {
//       final response = await dio.get(
//         'http://185.116.193.154/api/busy-date?city_id=$cityId',
//       );
//       return (response.data as List)
//           .map((x) => DateTime.parse(x as String))
//           .toList();
//     } on DioError catch (error) {
//       final data = jsonDecode(error.response.toString());
//       throw ErrorException(message: data['message'].toString());
//     }
//   }
// }
