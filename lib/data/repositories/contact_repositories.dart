// ignore_for_file: avoid_dynamic_calls

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/contact.dart';

class  ContactRepositories {
      

Future<Contacts> getContact() async {
     final dio = Dio();
        dio.options.headers['Accept'] = "application/json";
      try {
      final response = await dio.get('$SERVER_HOST/api/contact',);

      return Contacts.fromJson(response.data as Map<String, dynamic>);
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
    
   }

}
