// ignore_for_file: avoid_dynamic_calls

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/user.dart';

class ProfileRepository {

  Future<User> profile(String access) async {
    final dio = Dio();
    dio.options.headers['authorization'] = 'Bearer $access';
    dio.options.headers['Accept'] = "application/json";
    try {
      final response = await dio.post('$SERVER_HOST/api/v1/profile');
      return User(
        id: response.data['id'] as int,
        name: response.data['name'] as String,
        phone: response.data['phone'] as String,
      );
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}
