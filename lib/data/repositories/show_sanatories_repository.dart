// ignore_for_file: avoid_dynamic_calls, avoid_print

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/banner.dart';
import 'package:embolsyn/data/models/show_sanatorium.dart';

class ShowSanatoriesRepository {
  Future<ShowSanatorium> showSanatories({
    required int sanatoriumId,
    required DateTime startDate,
    required DateTime endDate,
    required int adultCount,
    required int childCount,
  }) async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    // print('ShowSanatori');
    try {
      // final int? cityId = await _prefs.getCityId();

      final response = await dio.get(
        '$SERVER_HOST/api/sanatorium/show/$sanatoriumId',
        queryParameters: {
          "start_date": startDate,
          "end_date": endDate,
          "count_adults": adultCount,
          "count_children": childCount,
        },
      );

      return ShowSanatorium.fromJson(response.data as Map<String, dynamic>);
      // return (response.data as List)
      //     .map((x) => ShowSanatorium.fromJson(x as Map<String, dynamic>))
      //     .toList();
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<ShowSanatorium> showSanatoriesNoneDate({
    required int sanatoriumId,
  }) async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    // print('ShowSanatori');
    try {
      // final int? cityId = await _prefs.getCityId();

      final response = await dio.get(
        '$SERVER_HOST/api/sanatorium/show/$sanatoriumId',
      );

      return ShowSanatorium.fromJson(response.data as Map<String, dynamic>);
      // return (response.data as List)
      //     .map((x) => ShowSanatorium.fromJson(x as Map<String, dynamic>))
      //     .toList();
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<List<Banner>> banner() async {
    final dio = Dio();
    try {
      dio.options.headers['Accept'] = "application/json";
      final response = await dio.get(
        '$SERVER_HOST/api/banners',
      );

      return (response.data as List).map((x) => Banner.fromJson(x as Map<String, dynamic>)).toList();
    } on DioError catch (error) {
      // log(error.toString());
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<List<ShowSanatorium>> searchSanatorium({
    required String sanatorium,
  }) async {
    final dio = Dio();
    try {
      dio.options.headers['Accept'] = "application/json";
      final response = await dio.post(
        '$SERVER_HOST/api/search-sanatorium',
        data: {
          'search': sanatorium,
        },
      );

      return (response.data as List).map((x) => ShowSanatorium.fromJson(x as Map<String, dynamic>)).toList();
    } on DioError catch (error) {
      // log(error.toString());
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<List<ShowSanatorium>> getAllSanatories({
    required int cityId,
    required int countAdults,
    required int countChildren,
    required DateTime startDate,
    required DateTime endDate,
  }) async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    print('Indexed');
    try {
      // final int? cityId = await _prefs.getCityId();
      // final int? countAdults = await _prefs.getCountAdults();
      // final int? countChildren = await _prefs.getCountChildren();
      final response = await dio.get(
        '$SERVER_HOST/api/sanatorium?city_id=$cityId&count_adults=$countAdults&count_children=$countChildren&start_date=$startDate&end_date=$endDate',
      );

      // final response = await dio.get(
      //   '$SERVER_HOST/api/sanatorium',
      // );

      return (response.data as List).map((x) => ShowSanatorium.fromJson(x as Map<String, dynamic>)).toList();
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}
