// ignore_for_file: avoid_dynamic_calls

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/notification.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationRepository {
  Future<List<Notification>> getNotification() async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    try {
      final response = await dio.get(
        '$SERVER_HOST/api/notification',
      );

      return (response.data as List).map((x) => Notification.fromJson(x as Map<String, dynamic>)).toList();
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}

class NotificationService {
  late FirebaseMessaging _messaging;
  Future<void> init() async {
    _messaging = FirebaseMessaging.instance;
    _messaging.getInitialMessage().then((value) => log('Message is $value'));
    if (Platform.isIOS) {
      _requestPermissonToNotifications();
    }

    _messaging.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    await getDeviceToken();
    const androiInit = AndroidInitializationSettings('@mipmap/ic_launcher'); //for logo
    const iosInit = DarwinInitializationSettings();
    const initSetting = InitializationSettings(android: androiInit, iOS: iosInit);
    final fltNotification = FlutterLocalNotificationsPlugin();
    fltNotification.initialize(initSetting);
    const androidDetails = AndroidNotificationDetails(
      '1',
      'channelName',
      channelDescription: 'channelDescription',
    );
    const iosDetails = DarwinNotificationDetails();
    const generalNotificationDetails = NotificationDetails(android: androidDetails, iOS: iosDetails);

    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      final RemoteNotification? notification = event.notification;
      final AndroidNotification? android = event.notification?.android;
      if (notification != null && android != null) {
        fltNotification.show(
          notification.hashCode,
          notification.title,
          notification.body,
          generalNotificationDetails,
        );
      }
    });
  }

  Future<String?> getDeviceToken() async {
    final String? deviceToken = await FirebaseMessaging.instance.getToken();
    log(deviceToken ?? "device tokena net");
    return deviceToken;
  }

  Future<void> _requestPermissonToNotifications() async {
    final NotificationSettings settings = await _messaging.requestPermission(
        // alert: true,
        // announcement: false,
        // badge: true,
        // carPlay: false,
        // criticalAlert: false,
        // provisional: false,
        // sound: true,
        );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      log('User granted permission');
    } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
      log('User granted provisional permission');
    } else {
      log('User declined or has not accepted permission');
    }
  }
}
