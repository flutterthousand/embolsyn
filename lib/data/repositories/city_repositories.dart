// ignore_for_file: avoid_dynamic_calls

import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/city.dart';

class CityRepositories {
  Future<List<City>> getCity() async {
    final dio = Dio();
    dio.options.headers['Accept'] = "application/json";
    try {
      final response = await dio.get(
        '$SERVER_HOST/api/city',
      );

      return (response.data as List)
          .map((x) => City.fromJson(x as Map<String, dynamic>))
          .toList();
    } on DioError catch (error) {
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<List<City>> searchCity({required String city}) async {
    final dio = Dio();
    try {
      dio.options.headers['Accept'] = "application/json";
      final response = await dio
          .post('$SERVER_HOST/api/search-city', data: {'search': city});

      return (response.data as List)
          .map((x) => City.fromJson(x as Map<String, dynamic>))
          .toList();
    } on DioError catch (error) {
      log(error.toString());
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}
