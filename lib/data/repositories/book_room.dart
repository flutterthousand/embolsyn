import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/booking.dart';
import 'package:embolsyn/data/models/data.dart';
import 'package:embolsyn/data/modelsDto/users_dto.dart';
import 'package:embolsyn/data/prefs.dart';

class BookRepository {
  Dio dio = Dio();

  Future<Booking> rentRoom({
    required int countChildren,
    required int countParents,
    required String toDate,
    required String fromDate,
    required int sanatoriumRoomId,
    required List<UsersAdultsDto> users,
  }) async {
    try {
      final Map<dynamic, dynamic> mapp = {
        'from_date': fromDate,
        'to_date': toDate,
        'sanatorium_room_id': sanatoriumRoomId,
        'count_children': countChildren,
        'count_adults': countParents,
        'users':users,
      };
      // parents.addAll(children);
      // mapp['users'] = parents;
      // print(mapp);
      final token = await Prefs().getToken();
      dio.options.headers['authorization'] = 'Bearer $token';
      dio.options.headers['Accept'] = "application/json";
      final response = await dio.post(
        '$SERVER_HOST/api/book/room',
        data: mapp,
      );

      log('##### rentRoom :: ${response.statusCode}, ${response.data}');
      final Booking booking =
          // ignore: avoid_dynamic_calls
          Booking.fromJson(response.data['booking'] as Map<String, dynamic>);
      return booking;
    } on DioError catch (error) {
      // print(error);
      log(error.toString());
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<Data> paymentTest({
    required String pgOrderId,
    required int pgMerchantId,
    required int pgAmount,
    required String pgDescription,
    required String pgSalt,
    required List<UsersAdultsDto> users,
  }) async {
    try {
      final Map<dynamic, dynamic> mapp = {
        'pg_order_id': pgOrderId,
        'pg_merchant_id': pgMerchantId,
        'pg_amount': pgAmount,
        'pg_description': pgDescription,
        'pg_salt': pgSalt,
        'users':users,
      };
      final token = await Prefs().getToken();
      dio.options.headers['authorization'] = 'Bearer $token';
      dio.options.headers['Accept'] = "application/json";
      final response = await dio.post(
        '$SERVER_HOST/api/payment-test',
        data: mapp,
      );

      log('##### rentRoom :: ${response.statusCode}, ${response.data}');
      final Data data =
          Data.fromJson(response.data['data'] as Map<String, dynamic>);

      return data;
    } on DioError catch (error) {
      print(error);
      log(error.toString());
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }

  Future<Data> checkStatus({
    required String pgOrderId,
    required String pgPaymentId,
    required String pgSalt,
  }) async {
    try {
      final token = await Prefs().getToken();
      dio.options.headers['authorization'] = 'Bearer $token';
      dio.options.headers['Accept'] = "application/json";
      final response = await dio.get(
        '$SERVER_HOST/api/get-status-payment?pg_order_id=$pgOrderId&pg_payment_id=$pgPaymentId&pg_salt=$pgSalt',
      );

      log('##### rentRoom :: ${response.statusCode}, ${response.data}');
      final Data data =
          Data.fromJson(response.data['data'] as Map<String, dynamic>);
      return data;
    } on DioError catch (error) {
      print(error);
      log(error.toString());
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw ErrorException(message: 'нет интернет соединения');
      }
      final data = jsonDecode(error.response.toString());
      throw ErrorException(message: data['message'].toString());
    }
  }
}
