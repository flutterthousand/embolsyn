import 'package:embolsyn/data/modelsDto/users_dto.dart';
import 'package:flutter/material.dart';

class ReserveProvider extends ChangeNotifier {
  List<UsersAdultsDto> usersAdultsDto = [];
  List<UsersAdultsDto> children = [];

  void init({
    required int countAdults,
    required int countChildren,
  }) {
    usersAdultsDto.clear();
    children.clear();

    for (int i = 0; i < countAdults; i++) {
      usersAdultsDto.add(
        UsersAdultsDto(
          name: '',
          surname: '',
          dateBirth: '',
          gender: '',
          type: '',
        ),
      );
    }

    for (int i = 0; i < countChildren; i++) {
      children.add(
        UsersAdultsDto(
          name: '',
          surname: '',
          dateBirth: '',
          gender: '',
          type: '',
        ),
      );
    }
  }

  bool chechIsAllFilled() {
    bool isFilled = true;
    final List<UsersAdultsDto> newList = usersAdultsDto + children;
    for (final UsersAdultsDto user in newList) {
      if (user.name.isEmpty ||
          user.surname.isEmpty ||
          user.dateBirth.isEmpty ||
          user.gender.isEmpty ||
          user.type.isEmpty) {
        isFilled = false;
        break;
      }
    }
    return isFilled;
  }

  void notify() {
    notifyListeners();
  }

  int? _countChildren = 0;
  int? get countChildren => _countChildren;
  set countChildren(int? value) {
    _countChildren = value;
    notifyListeners();
  }

  int? _countAdults = 0;
  int? get countAdults => _countAdults;
  set countAdults(int? value) {
    _countAdults = value;
    notifyListeners();
  }

  DateTime? _startDateTo;
  DateTime? get startDateTo => _startDateTo;
  set startDateTo(DateTime? value) {
    _startDateTo = value;
    notifyListeners();
  }

  DateTime? _endDateTo;
  DateTime? get endDateTo => _endDateTo;
  set endDateTo(DateTime? value) {
    _endDateTo = value;
    notifyListeners();
  }

  void resetAll() {
    _startDateTo = null;
    _endDateTo = null;
    _countAdults = null;
    _countChildren = null;
    notifyListeners();
  }
}
