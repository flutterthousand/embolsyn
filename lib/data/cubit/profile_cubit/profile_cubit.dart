import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/user.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_state.dart';
part 'profile_cubit.freezed.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit() : super(const InitialState());
  final Prefs _prefs = Prefs();
  Future<void> getProfile() async {
    emit(const LoadingState());
    try {
      final User? user = await _prefs.getUserFromCacheNull();

      emit(LoadedState(user: user!));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }
}
