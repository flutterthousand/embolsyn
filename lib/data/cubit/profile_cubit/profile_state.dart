part of 'profile_cubit.dart';

@freezed
class ProfileState with _$ProfileState {
    const factory ProfileState.initialState() = InitialState;
  const factory ProfileState.loadingState() = LoadingState;
  const factory ProfileState.loadedState( {required User user}) = LoadedState;
  const factory ProfileState.errorState({required String message}) = ErrorState;
}
