part of 'notification_cubit.dart';

@freezed
class NotificationState with _$NotificationState {
  const factory NotificationState.initialState() = InitialState;
  const factory NotificationState.loadingState() = LoadingState;
  const factory NotificationState.loadedState({
    required List<Notification> noti,
  }) = LoadedState;
  const factory NotificationState.errorState({required String message}) = ErrorState;
}
