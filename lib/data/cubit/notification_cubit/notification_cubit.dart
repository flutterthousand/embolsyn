import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/notification.dart';
import 'package:embolsyn/data/repositories/notification.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'notification_state.dart';
part 'notification_cubit.freezed.dart';

class NotificationCubit extends Cubit<NotificationState> {
  final NotificationRepository notificationRepository;

  NotificationCubit({required this.notificationRepository}) : super(const InitialState());

 Future<void> getNotification() async {
    emit(const LoadingState());
    try {
      final List<Notification> notification = await notificationRepository.getNotification();

      emit(LoadedState(noti: notification));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

}
