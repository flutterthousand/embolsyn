part of 'navigation_cubit.dart';

@freezed
abstract class NavigationState with _$NavigationState {
  const factory NavigationState.home() = HomeState;
  const factory NavigationState.hotDeal() = HotDealState;
  const factory NavigationState.contact() = ContactState;
  const factory NavigationState.profile() = ProfileState;
  const factory NavigationState.auth() = AuthState;
}
