import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'navigation_cubit.freezed.dart';

part 'navigation_state.dart';

class NavigationCubit extends Cubit<NavigationState> {
  NavigationCubit() : super(const HomeState());

  Future<void> getNavBarItem(NavigationState state) async {
    final bool auth = await checkToken();

    state.when(
      home: () => emit(const HomeState()),
      hotDeal: () => emit(const HotDealState()),
      contact: ()=>emit(const ContactState()),
      profile: () {
        if (auth) {
        emit(const ProfileState());
      } else {
        emit(const AuthState());
      }
      },
      auth: () => emit(const AuthState()),);
  }

  Future<bool> checkToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey('access_token');
  }
}
