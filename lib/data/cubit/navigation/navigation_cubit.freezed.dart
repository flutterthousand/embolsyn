// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'navigation_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$NavigationState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() home,
    required TResult Function() hotDeal,
    required TResult Function() contact,
    required TResult Function() profile,
    required TResult Function() auth,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? home,
    TResult? Function()? hotDeal,
    TResult? Function()? contact,
    TResult? Function()? profile,
    TResult? Function()? auth,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? home,
    TResult Function()? hotDeal,
    TResult Function()? contact,
    TResult Function()? profile,
    TResult Function()? auth,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeState value) home,
    required TResult Function(HotDealState value) hotDeal,
    required TResult Function(ContactState value) contact,
    required TResult Function(ProfileState value) profile,
    required TResult Function(AuthState value) auth,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeState value)? home,
    TResult? Function(HotDealState value)? hotDeal,
    TResult? Function(ContactState value)? contact,
    TResult? Function(ProfileState value)? profile,
    TResult? Function(AuthState value)? auth,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeState value)? home,
    TResult Function(HotDealState value)? hotDeal,
    TResult Function(ContactState value)? contact,
    TResult Function(ProfileState value)? profile,
    TResult Function(AuthState value)? auth,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NavigationStateCopyWith<$Res> {
  factory $NavigationStateCopyWith(
          NavigationState value, $Res Function(NavigationState) then) =
      _$NavigationStateCopyWithImpl<$Res, NavigationState>;
}

/// @nodoc
class _$NavigationStateCopyWithImpl<$Res, $Val extends NavigationState>
    implements $NavigationStateCopyWith<$Res> {
  _$NavigationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$HomeStateCopyWith<$Res> {
  factory _$$HomeStateCopyWith(
          _$HomeState value, $Res Function(_$HomeState) then) =
      __$$HomeStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HomeStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res, _$HomeState>
    implements _$$HomeStateCopyWith<$Res> {
  __$$HomeStateCopyWithImpl(
      _$HomeState _value, $Res Function(_$HomeState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$HomeState implements HomeState {
  const _$HomeState();

  @override
  String toString() {
    return 'NavigationState.home()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$HomeState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() home,
    required TResult Function() hotDeal,
    required TResult Function() contact,
    required TResult Function() profile,
    required TResult Function() auth,
  }) {
    return home();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? home,
    TResult? Function()? hotDeal,
    TResult? Function()? contact,
    TResult? Function()? profile,
    TResult? Function()? auth,
  }) {
    return home?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? home,
    TResult Function()? hotDeal,
    TResult Function()? contact,
    TResult Function()? profile,
    TResult Function()? auth,
    required TResult orElse(),
  }) {
    if (home != null) {
      return home();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeState value) home,
    required TResult Function(HotDealState value) hotDeal,
    required TResult Function(ContactState value) contact,
    required TResult Function(ProfileState value) profile,
    required TResult Function(AuthState value) auth,
  }) {
    return home(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeState value)? home,
    TResult? Function(HotDealState value)? hotDeal,
    TResult? Function(ContactState value)? contact,
    TResult? Function(ProfileState value)? profile,
    TResult? Function(AuthState value)? auth,
  }) {
    return home?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeState value)? home,
    TResult Function(HotDealState value)? hotDeal,
    TResult Function(ContactState value)? contact,
    TResult Function(ProfileState value)? profile,
    TResult Function(AuthState value)? auth,
    required TResult orElse(),
  }) {
    if (home != null) {
      return home(this);
    }
    return orElse();
  }
}

abstract class HomeState implements NavigationState {
  const factory HomeState() = _$HomeState;
}

/// @nodoc
abstract class _$$HotDealStateCopyWith<$Res> {
  factory _$$HotDealStateCopyWith(
          _$HotDealState value, $Res Function(_$HotDealState) then) =
      __$$HotDealStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HotDealStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res, _$HotDealState>
    implements _$$HotDealStateCopyWith<$Res> {
  __$$HotDealStateCopyWithImpl(
      _$HotDealState _value, $Res Function(_$HotDealState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$HotDealState implements HotDealState {
  const _$HotDealState();

  @override
  String toString() {
    return 'NavigationState.hotDeal()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$HotDealState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() home,
    required TResult Function() hotDeal,
    required TResult Function() contact,
    required TResult Function() profile,
    required TResult Function() auth,
  }) {
    return hotDeal();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? home,
    TResult? Function()? hotDeal,
    TResult? Function()? contact,
    TResult? Function()? profile,
    TResult? Function()? auth,
  }) {
    return hotDeal?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? home,
    TResult Function()? hotDeal,
    TResult Function()? contact,
    TResult Function()? profile,
    TResult Function()? auth,
    required TResult orElse(),
  }) {
    if (hotDeal != null) {
      return hotDeal();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeState value) home,
    required TResult Function(HotDealState value) hotDeal,
    required TResult Function(ContactState value) contact,
    required TResult Function(ProfileState value) profile,
    required TResult Function(AuthState value) auth,
  }) {
    return hotDeal(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeState value)? home,
    TResult? Function(HotDealState value)? hotDeal,
    TResult? Function(ContactState value)? contact,
    TResult? Function(ProfileState value)? profile,
    TResult? Function(AuthState value)? auth,
  }) {
    return hotDeal?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeState value)? home,
    TResult Function(HotDealState value)? hotDeal,
    TResult Function(ContactState value)? contact,
    TResult Function(ProfileState value)? profile,
    TResult Function(AuthState value)? auth,
    required TResult orElse(),
  }) {
    if (hotDeal != null) {
      return hotDeal(this);
    }
    return orElse();
  }
}

abstract class HotDealState implements NavigationState {
  const factory HotDealState() = _$HotDealState;
}

/// @nodoc
abstract class _$$ContactStateCopyWith<$Res> {
  factory _$$ContactStateCopyWith(
          _$ContactState value, $Res Function(_$ContactState) then) =
      __$$ContactStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ContactStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res, _$ContactState>
    implements _$$ContactStateCopyWith<$Res> {
  __$$ContactStateCopyWithImpl(
      _$ContactState _value, $Res Function(_$ContactState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ContactState implements ContactState {
  const _$ContactState();

  @override
  String toString() {
    return 'NavigationState.contact()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ContactState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() home,
    required TResult Function() hotDeal,
    required TResult Function() contact,
    required TResult Function() profile,
    required TResult Function() auth,
  }) {
    return contact();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? home,
    TResult? Function()? hotDeal,
    TResult? Function()? contact,
    TResult? Function()? profile,
    TResult? Function()? auth,
  }) {
    return contact?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? home,
    TResult Function()? hotDeal,
    TResult Function()? contact,
    TResult Function()? profile,
    TResult Function()? auth,
    required TResult orElse(),
  }) {
    if (contact != null) {
      return contact();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeState value) home,
    required TResult Function(HotDealState value) hotDeal,
    required TResult Function(ContactState value) contact,
    required TResult Function(ProfileState value) profile,
    required TResult Function(AuthState value) auth,
  }) {
    return contact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeState value)? home,
    TResult? Function(HotDealState value)? hotDeal,
    TResult? Function(ContactState value)? contact,
    TResult? Function(ProfileState value)? profile,
    TResult? Function(AuthState value)? auth,
  }) {
    return contact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeState value)? home,
    TResult Function(HotDealState value)? hotDeal,
    TResult Function(ContactState value)? contact,
    TResult Function(ProfileState value)? profile,
    TResult Function(AuthState value)? auth,
    required TResult orElse(),
  }) {
    if (contact != null) {
      return contact(this);
    }
    return orElse();
  }
}

abstract class ContactState implements NavigationState {
  const factory ContactState() = _$ContactState;
}

/// @nodoc
abstract class _$$ProfileStateCopyWith<$Res> {
  factory _$$ProfileStateCopyWith(
          _$ProfileState value, $Res Function(_$ProfileState) then) =
      __$$ProfileStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res, _$ProfileState>
    implements _$$ProfileStateCopyWith<$Res> {
  __$$ProfileStateCopyWithImpl(
      _$ProfileState _value, $Res Function(_$ProfileState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileState implements ProfileState {
  const _$ProfileState();

  @override
  String toString() {
    return 'NavigationState.profile()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() home,
    required TResult Function() hotDeal,
    required TResult Function() contact,
    required TResult Function() profile,
    required TResult Function() auth,
  }) {
    return profile();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? home,
    TResult? Function()? hotDeal,
    TResult? Function()? contact,
    TResult? Function()? profile,
    TResult? Function()? auth,
  }) {
    return profile?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? home,
    TResult Function()? hotDeal,
    TResult Function()? contact,
    TResult Function()? profile,
    TResult Function()? auth,
    required TResult orElse(),
  }) {
    if (profile != null) {
      return profile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeState value) home,
    required TResult Function(HotDealState value) hotDeal,
    required TResult Function(ContactState value) contact,
    required TResult Function(ProfileState value) profile,
    required TResult Function(AuthState value) auth,
  }) {
    return profile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeState value)? home,
    TResult? Function(HotDealState value)? hotDeal,
    TResult? Function(ContactState value)? contact,
    TResult? Function(ProfileState value)? profile,
    TResult? Function(AuthState value)? auth,
  }) {
    return profile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeState value)? home,
    TResult Function(HotDealState value)? hotDeal,
    TResult Function(ContactState value)? contact,
    TResult Function(ProfileState value)? profile,
    TResult Function(AuthState value)? auth,
    required TResult orElse(),
  }) {
    if (profile != null) {
      return profile(this);
    }
    return orElse();
  }
}

abstract class ProfileState implements NavigationState {
  const factory ProfileState() = _$ProfileState;
}

/// @nodoc
abstract class _$$AuthStateCopyWith<$Res> {
  factory _$$AuthStateCopyWith(
          _$AuthState value, $Res Function(_$AuthState) then) =
      __$$AuthStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res, _$AuthState>
    implements _$$AuthStateCopyWith<$Res> {
  __$$AuthStateCopyWithImpl(
      _$AuthState _value, $Res Function(_$AuthState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AuthState implements AuthState {
  const _$AuthState();

  @override
  String toString() {
    return 'NavigationState.auth()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AuthState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() home,
    required TResult Function() hotDeal,
    required TResult Function() contact,
    required TResult Function() profile,
    required TResult Function() auth,
  }) {
    return auth();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? home,
    TResult? Function()? hotDeal,
    TResult? Function()? contact,
    TResult? Function()? profile,
    TResult? Function()? auth,
  }) {
    return auth?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? home,
    TResult Function()? hotDeal,
    TResult Function()? contact,
    TResult Function()? profile,
    TResult Function()? auth,
    required TResult orElse(),
  }) {
    if (auth != null) {
      return auth();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HomeState value) home,
    required TResult Function(HotDealState value) hotDeal,
    required TResult Function(ContactState value) contact,
    required TResult Function(ProfileState value) profile,
    required TResult Function(AuthState value) auth,
  }) {
    return auth(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(HomeState value)? home,
    TResult? Function(HotDealState value)? hotDeal,
    TResult? Function(ContactState value)? contact,
    TResult? Function(ProfileState value)? profile,
    TResult? Function(AuthState value)? auth,
  }) {
    return auth?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HomeState value)? home,
    TResult Function(HotDealState value)? hotDeal,
    TResult Function(ContactState value)? contact,
    TResult Function(ProfileState value)? profile,
    TResult Function(AuthState value)? auth,
    required TResult orElse(),
  }) {
    if (auth != null) {
      return auth(this);
    }
    return orElse();
  }
}

abstract class AuthState implements NavigationState {
  const factory AuthState() = _$AuthState;
}
