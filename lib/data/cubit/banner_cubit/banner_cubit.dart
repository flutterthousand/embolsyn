import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/banner.dart';
import 'package:embolsyn/data/repositories/show_sanatories_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'banner_state.dart';
part 'banner_cubit.freezed.dart';

class BannerCubit extends Cubit<BannerState> {
  final ShowSanatoriesRepository sanatoriesRepository;
  BannerCubit({required this.sanatoriesRepository})
      : super(const InitialState());

  Future<void> banner() async {
    emit(const LoadingState());
    try {
      final List<Banner> banner = await sanatoriesRepository.banner();
      // SharedPreferences prefs = await SharedPreferences.getInstance();

      emit(LoadedState(banner: banner));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }
}
