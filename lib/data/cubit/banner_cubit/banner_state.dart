part of 'banner_cubit.dart';

@freezed
class BannerState with _$BannerState {
  const factory BannerState.initialState() = InitialState;
  const factory BannerState.loadingState() = LoadingState;
  const factory BannerState.loadedState({required List<Banner> banner}) =
      LoadedState;
  const factory BannerState.errorState({required String message}) = ErrorState;
}
