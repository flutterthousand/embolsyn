part of 'contact_cubit.dart';

@freezed
class ContactState with _$ContactState {
  const factory ContactState.initialState() = InitialState;
  const factory ContactState.loadingState() = LoadingState;
  const factory ContactState.loadedState({required Contacts contact}) =
      LoadedState;
  const factory ContactState.errorState({required String message}) = ErrorState;
}
