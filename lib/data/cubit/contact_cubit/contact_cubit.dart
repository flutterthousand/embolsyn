import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/contact.dart';
import 'package:embolsyn/data/repositories/contact_repositories.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'contact_state.dart';
part 'contact_cubit.freezed.dart';

class ContactCubit extends Cubit<ContactState> {
  final ContactRepositories contactRepositories;
  ContactCubit({required this.contactRepositories})
      : super(const InitialState());

  Future<void> getContact() async {
    emit(const LoadingState());
    try {
      final Contacts showContact = await contactRepositories.getContact();
      // SharedPreferences prefs = await SharedPreferences.getInstance();

      emit(LoadedState(contact: showContact));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }
}
