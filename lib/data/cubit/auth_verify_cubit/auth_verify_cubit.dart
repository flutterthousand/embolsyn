import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/user.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:embolsyn/data/repositories/auth_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_verify_cubit.freezed.dart';

part 'auth_verify_state.dart';

class AuthVerifyCubit extends Cubit<AuthVerifyState> {
  final AuthRepository authRepository;
 final Prefs _prefs = Prefs();

  AuthVerifyCubit({required this.authRepository}) : super(const InitialState());

  Future<void> send(String code) async {
    emit(const LoadingState());
    try {
      final User user = await authRepository.verify(code);
      await _prefs.saveUserToCache(user: user);
      log('$user', name: 'AuthVerifyCubit');
      emit(const LoadedState());
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }
}
