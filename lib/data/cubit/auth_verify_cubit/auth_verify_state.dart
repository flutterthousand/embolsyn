part of 'auth_verify_cubit.dart';

@freezed
abstract class AuthVerifyState with _$AuthVerifyState {
  const factory AuthVerifyState.initialState() = InitialState;
  const factory AuthVerifyState.loadingState() = LoadingState;
  const factory AuthVerifyState.loadedState() = LoadedState;
  const factory AuthVerifyState.errorState({required String message}) = ErrorState;
}
