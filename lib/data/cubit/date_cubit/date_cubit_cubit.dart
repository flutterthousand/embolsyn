// import 'package:bloc/bloc.dart';
// import 'package:embolsyn/data/exceptions.dart';
// import 'package:embolsyn/data/repositories/calendar_repositories.dart';
// import 'package:freezed_annotation/freezed_annotation.dart';

// part 'date_cubit_state.dart';
// part 'date_cubit_cubit.freezed.dart';

// class DateCubitCubit extends Cubit<DateCubitState> {
//   final CalendarRepositories calendarRepositories;
//   DateCubitCubit({required this.calendarRepositories})
//       : super(const DateCubitState.initialState());

//   Future<void> getDate() async {
//     emit(const DateCubitState.loadingState());
//     try {
//       final List<DateTime> busyDays = await calendarRepositories.getCalendar();

//       emit(DateCubitState.loadedState(busyDays: busyDays));
//     } on ErrorException catch (_) {
//       emit(DateCubitState.errorState(message: _.message));
//     }
//   }
// }
