import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/show_sanatorium.dart';
import 'package:embolsyn/data/repositories/show_sanatories_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'show_sanatories_state.dart';
part 'show_sanatories_cubit.freezed.dart';

class ShowSanatoriesCubit extends Cubit<ShowSanatoriesState> {
  final ShowSanatoriesRepository sanatoriesRepository;
  ShowSanatoriesCubit({required this.sanatoriesRepository})
      : super(const InitialState());

  Future<void> getSanatories({
    required int sanatoriumId,
    required DateTime startDate,
    required DateTime endDate,
    required int adultCount,
    required int childCount,
  }) async {
    emit(const LoadingState());
    try {
      final ShowSanatorium show = await sanatoriesRepository.showSanatories(
        sanatoriumId: sanatoriumId,
        startDate: startDate,
        endDate: endDate,
        adultCount: adultCount,
        childCount: childCount,
      );
      // SharedPreferences prefs = await SharedPreferences.getInstance();

      emit(LoadedState(show: show));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

  Future<void> getSanatoriesNoneDate({
    required int sanatoriumId,
  }) async {
    emit(const LoadingState());
    try {
      final ShowSanatorium show = await sanatoriesRepository
          .showSanatoriesNoneDate(sanatoriumId: sanatoriumId);
      // SharedPreferences prefs = await SharedPreferences.getInstance();

      emit(LoadedState(show: show));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

  // Future<void> getIndex(
  //   {
  //   required int indexId,
  //   required int countAdults,
  //   required int countChildren,

  // }
  // ) async {
  //   emit(const LoadingState());
  //   try {
  //     final List<IndexShow> show = await sanatoriesRepository.indexShowSanatories(countAdults: countAdults, countChildren: countChildren, indexId: indexId);
  //     // SharedPreferences prefs = await SharedPreferences.getInstance();
  //     log(show.toString());
  //     emit(LoadedState(show: List<IndexShow> show));
  //   } on ErrorException catch (_) {
  //     emit(ErrorState(message: _.message));
  //   }
  // }

}
