part of 'show_sanatories_cubit.dart';

@freezed
class ShowSanatoriesState with _$ShowSanatoriesState {
 const factory ShowSanatoriesState .initialState() = InitialState;
  const factory ShowSanatoriesState .loadingState() = LoadingState;
  const factory ShowSanatoriesState .loadedState({required ShowSanatorium show}) = LoadedState;
  const factory ShowSanatoriesState .errorState({required String message}) = ErrorState;
}
