// ignore_for_file: unused_local_variable

import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/data.dart';
import 'package:embolsyn/data/modelsDto/users_dto.dart';
import 'package:embolsyn/data/repositories/book_room.dart';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_state.dart';
part 'book_cubit.freezed.dart';

class BookCubit extends Cubit<BookState> {
  final BookRepository bookRepository;

  BookCubit({required this.bookRepository}) : super(const InitialState());

  Future<void> reset() async {
    emit(const InitialState());
  }

  Future<void> rentRoom({
    required int countChildren,
    required int countParents,
    required String toDate,
    required String fromDate,
    required int sanatoriumRoomId,
    required List<UsersAdultsDto> users,
  }) async {
    emit(const LoadingState());
    try {
      final booking = await bookRepository.rentRoom(
        countChildren: countChildren,
        countParents: countParents,
        toDate: toDate,
        fromDate: fromDate,
        sanatoriumRoomId: sanatoriumRoomId,
        users: users,
      );
      final data = await bookRepository.paymentTest(
        users: users,
        pgOrderId: '${booking.id}',
        pgMerchantId: 1,
        pgAmount: booking.price!,
        pgDescription: 'Em bolsin',
        pgSalt: 'Em bolsin',
      );
      emit(LoadedState(url: data));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

    Future<void> rentRoomWithoutPayment({
    required int countChildren,
    required int countParents,
    required String toDate,
    required String fromDate,
    required int sanatoriumRoomId,
    required List<UsersAdultsDto> users,
  }) async {
    try {
      final booking = await bookRepository.rentRoom(
        countChildren: countChildren,
        countParents: countParents,
        toDate: toDate,
        fromDate: fromDate,
        sanatoriumRoomId: sanatoriumRoomId,
        users: users,
      );
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

}
