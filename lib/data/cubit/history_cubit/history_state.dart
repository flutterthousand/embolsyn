part of 'history_cubit.dart';

@freezed
class HistoryState with _$HistoryState {
  const factory HistoryState.initialState() = InitialState;
  const factory HistoryState.loadingState() = LoadingState;
  const factory HistoryState.loadedState({
    required List<History> history,
  }) = LoadedState;
  const factory HistoryState.errorState({required String message}) = ErrorState;
}
