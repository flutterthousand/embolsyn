import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/history.dart';
import 'package:embolsyn/data/models/pdf.dart';
import 'package:embolsyn/data/repositories/history_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'history_state.dart';
part 'history_cubit.freezed.dart';

class HistoryCubit extends Cubit<HistoryState> {
  final HistoryRepositories historyRepositories;

  HistoryCubit({
    required this.historyRepositories,
  }) : super(const InitialState());
  Future<void> getHistory() async {
    emit(const LoadingState());
    try {
      final List<History> history = await historyRepositories.getHistory();

      emit(LoadedState(history: history));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

  Future<Pdf> getPdf({
    required int id,
  }) async {

    try {
      final Pdf showPdf = await historyRepositories.getPdf(
        id: id,
      );
      // print(showPdf);
      return showPdf;
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
      throw Exception(_);
    }
  }
}
