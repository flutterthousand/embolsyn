import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/city.dart';
import 'package:embolsyn/data/repositories/city_repositories.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'cities_state.dart';
part 'cities_cubit.freezed.dart';

class CitiesCubit extends Cubit<CitiesState> {
  final CityRepositories cityRepositories;
  CitiesCubit({required this.cityRepositories})
      : super(const CitiesState.initialState());

  Future<void> getCities() async {
    emit(const CitiesState.loadingState());
    try {
      final List<City> cities = await cityRepositories.getCity();

      emit(CitiesState.loadedState(cities: cities));
    } on ErrorException catch (_) {
      emit(CitiesState.errorState(message: _.message));
    }
  }

  Future<void> searchCity({
    required String city,
  }) async {
    emit(const CitiesState.loadingState());
    try {
      final List<City> cities = await cityRepositories.searchCity(city: city);

      emit(CitiesState.searchedState(cities: cities));
    } on ErrorException catch (_) {
      emit(CitiesState.errorState(message: _.message));
    }
  }
}
