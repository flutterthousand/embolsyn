part of 'cities_cubit.dart';

@freezed
class CitiesState with _$CitiesState {
  const factory CitiesState.initialState() = _InitialState;
  const factory CitiesState.loadingState() = _LoadingState;
  const factory CitiesState.loadedState({
    required List<City> cities,
  }) = _LoadedState;
  const factory CitiesState.searchedState({
    required List<City> cities,
  }) = _SearchedState;
  const factory CitiesState.errorState({
    required String message,
  }) = _ErrorState;
}
