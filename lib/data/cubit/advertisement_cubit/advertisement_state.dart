part of 'advertisement_cubit.dart';

@freezed
class AdvertisementState with _$AdvertisementState {
 const factory AdvertisementState.initialState() = InitialState;
  const factory AdvertisementState.loadingState() = LoadingState;
  const factory AdvertisementState.loadedState({
    required List<Advertising> advert,
  }) = LoadedState;
  const factory AdvertisementState.errorState({required String message}) = ErrorState;
}
