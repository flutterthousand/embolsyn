import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/advertising.dart';
import 'package:embolsyn/data/repositories/advertising_repositories.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'advertisement_state.dart';
part 'advertisement_cubit.freezed.dart';

class AdvertisementCubit extends Cubit<AdvertisementState> {
  final AdvertisingRepository advertisingRepository;

  AdvertisementCubit({required this.advertisingRepository}) : super(const InitialState());

  Future<void> getAdvertisement() async {
    emit(const LoadingState());
    try {
      final List<Advertising> advert = await advertisingRepository.getAdvertising();
      emit(LoadedState(advert: advert));
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

}
