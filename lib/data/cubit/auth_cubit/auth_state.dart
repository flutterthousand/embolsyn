part of 'auth_cubit.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState.initialState() = InitialState;
  const factory AuthState.loadingState() = LoadingState;
  const factory AuthState.loadedState() = LoadedState;
  const factory AuthState.errorState({required String message}) = ErrorState;
}
