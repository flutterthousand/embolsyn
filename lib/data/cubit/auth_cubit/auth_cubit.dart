import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/repositories/auth_repository.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_cubit.freezed.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository authRepository;
  AuthCubit({required this.authRepository}) : super(const InitialState());

  Future<void> auth(String phone, BuildContext context) async {
    emit(const LoadingState());
    try {
      await authRepository.auth(phone: phone);
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('phone', phone);
      emit(const LoadedState());
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }
  Future<void> register(String phone,String name,String surname, BuildContext context) async {
    emit(const LoadingState());
    try {
      await authRepository.register(phone: phone,name: name,surname: surname,);
      // final SharedPreferences prefs = await SharedPreferences.getInstance();
      // prefs.setString('phone', phone);
      emit(const LoadedState());
    } on ErrorException catch (_) {
      emit(ErrorState(message: _.message));
    }
  }

  Future<void> logOut() async {
    try {
      await authRepository.logOut();
    } on ErrorException catch (_) {
      log(_.message);
    }
  }

   Future<void> delete() async {
    try {
      await authRepository.delete();
    } on ErrorException catch (_) {
      log(_.message);
    }
  }
}
