import 'package:bloc/bloc.dart';
import 'package:embolsyn/data/exceptions.dart';
import 'package:embolsyn/data/models/show_sanatorium.dart';
import 'package:embolsyn/data/repositories/show_sanatories_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_state.dart';
part 'home_cubit.freezed.dart';

class HomeCubit extends Cubit<HomeState> {
  final ShowSanatoriesRepository sanatoriesRepository;
  HomeCubit({required this.sanatoriesRepository})
      : super(const HomeState.initialState());

  Future<void> getSanatories({
    required int cityId,
    required int countAdults,
    required int countChildren,
    required DateTime startDate,
    required DateTime endDate,
  }) async {
    emit(const HomeState.loadingState());
    try {
      final List<ShowSanatorium> sanatories =
          await sanatoriesRepository.getAllSanatories(
        cityId: cityId,
        countAdults: countAdults,
        countChildren: countChildren,
        startDate: startDate,
        endDate: endDate,
      );
      emit(HomeState.loadedState(sanatories: sanatories));
    } on ErrorException catch (_) {
      emit(HomeState.errorState(message: _.message));
    }
  }

  Future<void> searchSanatori({required String search}) async {
    emit(const HomeState.loadingState());
    try {
      final List<ShowSanatorium> sanatori =
          await sanatoriesRepository.searchSanatorium(sanatorium: search);

      // log(sanatori.toString());
      emit(HomeState.searchedState(sanatories: sanatori));
    } on ErrorException catch (_) {
      emit(HomeState.errorState(message: _.message));
    }
  }

  Future<void> goToInitialState() async {
    emit(const HomeState.initialState());
  }
}
