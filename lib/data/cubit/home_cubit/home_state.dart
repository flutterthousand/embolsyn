part of 'home_cubit.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState.initialState(
    // int? countAdults,
    // int? countChildren,
    // DateTime? startDateTo,
    // DateTime? endDateTo,
    // DateTime? howDaysTo,
  ) = _InitialState;
  const factory HomeState.loadingState() = _LoadingState;
  const factory HomeState.loadedState({
    required List<ShowSanatorium>sanatories,
  }) = _LoadedState;
  const factory HomeState.searchedState({
    required List<ShowSanatorium> sanatories,
  }) = _SearchedState;
  const factory HomeState.errorState({required String message}) = _ErrorState;
}
