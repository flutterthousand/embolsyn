import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/contact_cubit/contact_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({super.key});

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ContactCubit>(context).getContact();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Контакты'),
        ),
      ),
      body: BlocBuilder<ContactCubit, ContactState>(
        builder: (context, state) {
          if (state is LoadedState) {
            return ListView(
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  width: 144.0,
                  height: 144.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage(
                        '$SERVER_HOST/${state.contact.image}',
                      ),
                    ),
                  ),
                ),
                Text(
                  state.contact.name!,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 10),
                Text(
                  state.contact.subtitle!,
                  style: const TextStyle(fontSize: 14),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      child: Image.asset(
                        'assets/icons/vk.png',
                        width: 25,
                      ),
                      onTap: () => launch('${state.contact.vk}'),
                    ),
                    InkWell(
                      child: Image.asset(
                        'assets/icons/instagram.png',
                        width: 25,
                      ),
                      onTap: () => launch('${state.contact.instagram}'),
                    ),
                    InkWell(
                      child: Image.asset(
                        'assets/icons/facebook.png',
                        width: 25,
                      ),
                      onTap: () => launch('${state.contact.facebook}'),
                    ),
                    InkWell(
                      child: Image.asset(
                        'assets/icons/twitter.png',
                        width: 25,
                      ),
                      onTap: () => launch('${state.contact.twitter}'),
                    ),
                  ],
                ),
                const SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 3,
                          offset: const Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text('Телефон'),
                              InkWell(
                                child: Text(state.contact.phone!),
                                onTap: () {
                                  setState(() {
                                    _launched = _openUrl('tel:${state.contact.phone}');
                                  });
                                },
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(16),
                          color: Colors.grey[100],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text('Адрес'),
                              const SizedBox(
                                width: 45,
                              ),
                              Flexible(
                                child: InkWell(
                                    child: Text(
                                  '${state.contact.address}',
                                  overflow: TextOverflow.fade,
                                )),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text('Веб сайт'),
                              InkWell(
                                child: Text('${state.contact.website}'),
                                onTap: () => launch('${state.contact.website}'),
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(16),
                          color: Colors.grey[100],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text('Почта'),
                              InkWell(
                                child: Text(state.contact.email!),
                                onTap: () {
                                  setState(() {
                                    _launched = _openUrl(
                                      'mailto:${state.contact.email}',
                                    );
                                  });
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  late Future<void> _launched;

  Future<void> _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _callNumber() async {
    const number = '7004468696'; //set the number here
    final bool? res = await FlutterPhoneDirectCaller.callNumber(number);
  }
}
