// ignore_for_file: use_build_context_synchronously

import 'package:carousel_slider/carousel_slider.dart';
import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/banner_cubit/banner_cubit.dart' as banner;
import 'package:embolsyn/data/cubit/show_sanatories_cubit/show_sanatories_cubit.dart';
import 'package:embolsyn/data/models/comforts.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:embolsyn/presentation/base.dart';
import 'package:embolsyn/presentation/screens/reserve/reserve_page.dart';
import 'package:embolsyn/widgets/custom_snackbars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class SanatoriumPage extends StatefulWidget {
  const SanatoriumPage({
    required this.sanatoriumId,
    this.countAdults,
    this.countChildren,
    this.howDays,
    this.endDate,
    this.startDate,
    super.key,
  });
  final int sanatoriumId;
  final int? countAdults;
  final int? countChildren;
  final String? howDays;
  final DateTime? startDate;
  final DateTime? endDate;

  @override
  _SanatoriumPageState createState() => _SanatoriumPageState();
}

class _SanatoriumPageState extends State<SanatoriumPage> {
  final Prefs _prefs = Prefs();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<banner.BannerCubit>(context).banner();
    widget.startDate == null && widget.endDate == null
        ? BlocProvider.of<ShowSanatoriesCubit>(context).getSanatoriesNoneDate(
            sanatoriumId: widget.sanatoriumId,
          )
        : BlocProvider.of<ShowSanatoriesCubit>(context).getSanatories(
            sanatoriumId: widget.sanatoriumId,
            startDate: widget.startDate!,
            endDate: widget.endDate!,
            adultCount: widget.countAdults ?? 0,
            childCount: widget.countChildren ?? 0,
          );
  }

  List images = [
    'assets/images/test_hot_deal.png',
    'assets/images/test_hot_deal.png',
    'assets/images/test_hot_deal.png'
  ];
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ShowSanatoriesCubit, ShowSanatoriesState>(
      builder: (context, state) {
        if (state is LoadedState) {
          return Scaffold(
            appBar: AppBar(
              title: Text(state.show.title!),
            ),
            body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                child: ListView(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    BlocBuilder<banner.BannerCubit, banner.BannerState>(
                      builder: (context, er) {
                        if (er is banner.LoadedState) {
                          return CarouselSlider(
                            options: CarouselOptions(
                              viewportFraction: 1,
                              autoPlay: true,
                              autoPlayInterval: const Duration(seconds: 3),
                              enlargeCenterPage: true,
                            ),
                            items: er.banner.map((i) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return SizedBox(
                                    width: double.infinity,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Image.network(
                                        '$SERVER_HOST/${i.imagePath}',
                                        fit: BoxFit.cover,
                                        // errorBuilder: (
                                        //   BuildContext context,
                                        //   Object exception,
                                        //   StackTrace? stackTrace,
                                        // )m
                                        // ) {
                                        //   return Image.asset(
                                        //     "assets/images/test_hot_deal.png",
                                        //   );
                                        // },
                                      ),
                                    ),
                                  );
                                },
                              );
                            }).toList(),
                          );
                        }
                        return const Scaffold(
                          backgroundColor: Colors.white10,
                          body: Padding(
                            padding: EdgeInsets.all(80),
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        );
                      },
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      state.show.title!,
                      style: TextStyle(
                        color: Colors.grey[900],
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      state.show.address!,
                      style: TextStyle(color: Colors.grey[700]),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Немного про санаторий',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.grey[900],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      state.show.description!,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "Номера",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      reverse: true,
                      shrinkWrap: true,
                      itemCount: state.show.rooms!.length,
                      separatorBuilder: (BuildContext context, int index) => const Divider(),
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (state.show.rooms![index].images != null &&
                                  state.show.rooms![index].images!.isNotEmpty)
                                InkWell(
                                  onTap: () {
                                    open(
                                      context,
                                      index,
                                      state.show.rooms![index].images ?? [],
                                    );
                                  },
                                  child: CarouselSlider(
                                    options: CarouselOptions(
                                      viewportFraction: 1,
                                      // autoPlay: true,
                                      autoPlayInterval: const Duration(seconds: 3),
                                      enlargeCenterPage: true,
                                    ),
                                    items: state.show.rooms![index].images!.map((i) {
                                      return i == ''
                                          ? SizedBox(
                                              width: double.infinity,
                                              child: ClipRRect(
                                                borderRadius: const BorderRadius.only(
                                                  topLeft: Radius.circular(8),
                                                  topRight: Radius.circular(8),
                                                ),
                                                child: Image.asset(
                                                  'assets/missing-image.png',
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            )
                                          : Builder(
                                              builder: (BuildContext context) {
                                                return SizedBox(
                                                  width: double.infinity,
                                                  child: ClipRRect(
                                                    borderRadius: const BorderRadius.only(
                                                      topLeft: Radius.circular(8),
                                                      topRight: Radius.circular(8),
                                                    ),
                                                    child: Image.network(
                                                      '$SERVER_HOST/$i',
                                                      fit: BoxFit.cover,
                                                      errorBuilder: (
                                                        BuildContext context,
                                                        Object exception,
                                                        StackTrace? stackTrace,
                                                      ) {
                                                        return Image.asset(
                                                          'assets/avatar.png',
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                    }).toList(),
                                  ),
                                )
                              else
                                const SizedBox(
                                  height: 100,
                                  child: Center(
                                    child: Text('Нету картинок '),
                                  ),
                                ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Text(
                                      '${state.show.rooms![index].title}',
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    if (state.show.rooms![index].isBusy == true)
                                      const Text(
                                        'Санаторий Занята',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.red,
                                        ),
                                      )
                                    else
                                      const SizedBox(),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        SizedBox(
                                          child: Row(
                                            children: [
                                              Row(
                                                children: [
                                                  SvgPicture.asset(
                                                    'assets/svg/user-2.svg',
                                                  ),
                                                  const SizedBox(
                                                    width: 3,
                                                  ),
                                                  Text(
                                                    'x${state.show.rooms![index].countAdults.toString()}',
                                                  )
                                                ],
                                              ),
                                              const SizedBox(
                                                width: 12,
                                              ),
                                              Row(
                                                children: [
                                                  SvgPicture.asset(
                                                    'assets/svg/bear.svg',
                                                  ),
                                                  const SizedBox(
                                                    width: 3,
                                                  ),
                                                  Text(
                                                    'x${state.show.rooms![index].countChildren.toString()}',
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        RichText(
                                          textAlign: TextAlign.end,
                                          text: TextSpan(
                                            text: 'за ночь ',
                                            style: const TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 13,
                                              color: Colors.black,
                                            ),
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: state.show.rooms![index].price.toString(),
                                                style: const TextStyle(
                                                  fontSize: 18,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 7,
                                    ),
                                    if (state.show.rooms![index].roomLeft == null)
                                      const SizedBox()
                                    else
                                      Text(
                                        'Остались номера : ${state.show.rooms![index].roomLeft}',
                                        style: const TextStyle(
                                          color: Colors.red,
                                          fontSize: 16,
                                        ),
                                      ),
                                    const SizedBox(
                                      height: 7,
                                    ),
                                    Text(
                                      '${state.show.rooms![index].description}',
                                      style: TextStyle(
                                        color: Colors.grey[700],
                                        fontSize: 14,
                                      ),
                                    ),
                                    if (state.show.rooms![index].showDetail! &&
                                        state.show.rooms![index].comforts != null &&
                                        state.show.rooms![index].comforts!.isNotEmpty)
                                      Column(
                                        children: [
                                          Wrap(
                                            spacing: 5,
                                            children: state.show.rooms![index].comforts!.map((Comforts? comforts) {
                                              return Chip(
                                                // shape: const StadiumBorder(
                                                //   side: BorderSide(
                                                //     color: Color(0xFF03001E),
                                                //     width: 1,
                                                //   ),
                                                // ),
                                                backgroundColor: Colors.white,
                                                labelPadding: const EdgeInsets.only(
                                                  right: 6,
                                                  top: 2,
                                                  bottom: 2,
                                                ),
                                                avatar: Image.network(
                                                  '$SERVER_HOST/${comforts!.image!}',
                                                  errorBuilder: (
                                                    BuildContext context,
                                                    Object exception,
                                                    StackTrace? stackTrace,
                                                  ) {
                                                    return const Text(
                                                      'Нету картинок ',
                                                    );
                                                  },
                                                ),
                                                label: Text(
                                                  comforts.name,
                                                  style: const TextStyle(
                                                    color: Color(0xFF03001E),
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                          TextButton(
                                            onPressed: () {
                                              state.show.rooms![index].showDetail = false;
                                              setState(() {});
                                            },
                                            child: const Text("Показать меньше"),
                                          )
                                        ],
                                      )
                                    else
                                      TextButton(
                                        onPressed: () {
                                          state.show.rooms![index].showDetail = true;
                                          setState(() {});
                                        },
                                        child: const Text("Показать больше"),
                                      ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      child: TextButton(
                                        onPressed: () async {
                                          final token = await _prefs.getToken();
                                          if (token == null) {
                                            Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => const Base(
                                                  index: 3,
                                                ),
                                              ),
                                              (route) => false,
                                            );
                                            buildErrorCustomSnackBar(
                                              context,
                                              'Необходимо войти в аккаунт ',
                                            );
                                          } else {
                                            if (state.show.rooms![index].isBusy == true) {
                                              buildErrorCustomSnackBar(
                                                context,
                                                'На данный момент номер занят',
                                              );
                                              return;
                                            }
                                            if (widget.countAdults == null) {
                                              buildErrorCustomSnackBar(
                                                context,
                                                'Забронируйте комнату',
                                              );
                                            }

                                            widget.countChildren == null
                                                ? Navigator.pushAndRemoveUntil(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) => const Base(
                                                        index: 0,
                                                      ),
                                                    ),
                                                    (route) => false,
                                                  )
                                                : Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) => ReservePage(
                                                        countAdults: widget.countAdults!,
                                                        title: state.show.title!,
                                                        countChildren: widget.countChildren!,
                                                        startDate: widget.startDate!,
                                                        endDate: widget.endDate!,
                                                        sanatoriumRoomId: state.show.rooms![index].id,
                                                        howDays: widget.howDays!,
                                                      ),
                                                    ),
                                                  );
                                          }
                                        },
                                        style: TextButton.styleFrom(
                                          backgroundColor: Colors.orange[500],
                                        ),
                                        child: Text(
                                          widget.howDays == null
                                              ? 'Бронировать за ${state.show.rooms![index].price!} т'
                                              : 'Бронировать за ${state.show.rooms![index].totalPrice!} т',
                                          style: const TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          );
        }
        return const Scaffold(
          backgroundColor: Colors.white10,
          body: Padding(
            padding: EdgeInsets.all(80),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );
  }

  void open(BuildContext context, int index, List<String> images) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          galleryItems: images.map((e) => '$SERVER_HOST/$e').toList(),
          backgroundDecoration: const BoxDecoration(
            color: Colors.black,
          ),
          initialIndex: index,
          minScale: PhotoViewComputedScale.contained,
          maxScale: PhotoViewComputedScale.contained * 4,
        ),
      ),
    );
  }
}

class GalleryPhotoViewWrapper extends StatefulWidget {
  GalleryPhotoViewWrapper({
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialIndex = 0,
    required this.galleryItems,
    this.scrollDirection = Axis.horizontal,
  }) : pageController = PageController(initialPage: initialIndex);

  final LoadingBuilder? loadingBuilder;
  final BoxDecoration? backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final PageController pageController;
  final List<String> galleryItems;
  final Axis scrollDirection;

  @override
  State<StatefulWidget> createState() {
    return _GalleryPhotoViewWrapperState();
  }
}

class _GalleryPhotoViewWrapperState extends State<GalleryPhotoViewWrapper> {
  late int currentIndex = widget.initialIndex;

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        appBar: AppBar(),
        body: Container(
          decoration: widget.backgroundDecoration,
          constraints: BoxConstraints.expand(
            height: MediaQuery.of(context).size.height,
          ),
          child: Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              PhotoViewGallery.builder(
                scrollPhysics: const BouncingScrollPhysics(),
                builder: _buildItem,
                itemCount: widget.galleryItems.length,
                loadingBuilder: widget.loadingBuilder,
                backgroundDecoration: widget.backgroundDecoration,
                pageController: widget.pageController,
                onPageChanged: onPageChanged,
                scrollDirection: widget.scrollDirection,
              ),
              Container(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  " ${currentIndex + 1}",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 17.0,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    final String item = widget.galleryItems[index];
    return PhotoViewGalleryPageOptions(
      imageProvider: NetworkImage(item),
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained,
      maxScale: PhotoViewComputedScale.covered * 4.1,
      onScaleEnd: (context, details, controllerValue) {
        if (controllerValue.scale! < PhotoViewComputedScale.contained.multiplier * 0.4) {
          // context.router.pop();
        }
      },
      heroAttributes: PhotoViewHeroAttributes(tag: index),
    );
  }
}
