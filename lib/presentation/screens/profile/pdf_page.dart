import 'dart:io';

import 'package:embolsyn/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PdfPage extends StatefulWidget {
  final String url;
  const PdfPage({required this.url, super.key});

  @override
  State<PdfPage> createState() => _PdfPageState();
}

class _PdfPageState extends State<PdfPage> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  Future<void> share() async {
    await FlutterShare.share(
      title: 'Скачать билет',
      text: 'Скачать билет',
      linkUrl: '$SERVER_HOST/${widget.url}',
      chooserTitle: 'Скачать билет',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backGroundColor,
      appBar: AppBar(
        title: const Text(
          'Скачать билет',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        actions: [IconButton(onPressed: share, icon: const Icon(Icons.share))],
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
            // Navigator.pop(context);

            // Navigator.pushAndRemoveUntil(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) => const Base(
            //       index: 0,
            //     ),
            //   ),
            //   (route) => false,
            // );
          },
        ),
      ),
      body: SafeArea(
        child: WebView(
          initialUrl: '$SERVER_HOST/${widget.url}',
          javascriptMode: JavascriptMode.unrestricted,
        ),
      ),
    );
  }
}
