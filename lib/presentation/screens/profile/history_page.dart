import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/history_cubit/history_cubit.dart';

import 'package:embolsyn/data/models/pdf.dart';
import 'package:embolsyn/presentation/screens/profile/pdf_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({
    super.key,
  });
//  int id;
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  void initState() {
    Intl.systemLocale = 'ru';
    super.initState();
    BlocProvider.of<HistoryCubit>(context).getHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backGroundColor,
      appBar: AppBar(
        title: const Text('История заказов'),
      ),
      body: BlocBuilder<HistoryCubit, HistoryState>(
        builder: (context, state) {
          if (state is LoadedState) {
            return ListView.separated(
              shrinkWrap: true,
              itemCount: state.history.length,
              separatorBuilder: (BuildContext context, int index) => const SizedBox(),
              itemBuilder: (BuildContext context, int index) {
                final String days1 = DateFormat('dd').format(
                  DateTime.parse(state.history[index].fromDate.toString()),
                );
                final String days2 = DateFormat('dd').format(
                  DateTime.parse(state.history[index].toDate.toString()),
                );
                final int days = int.parse(days2) - int.parse(days1);
                final String day = days.toString();
                return Container(
                  padding: const EdgeInsets.only(left: 15, top: 16, right: 9),
                  child: Stack(
                    children: [
                      Container(
                        height: 213,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: const [
                            BoxShadow(
                              blurRadius: 24,
                              offset: Offset(0, 2),
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                            ),
                            BoxShadow(
                              blurRadius: 2,
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16, top: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(right: 20),
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image: AssetImage('assets/avatar.png'),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Санаторий "${state.history[index].sanatoriumName}"',
                                        style: TextStyle(
                                          color: Colors.grey[900],
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        'Номер Suit+',
                                        style: TextStyle(
                                          color: Colors.grey[700],
                                          fontSize: 14,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/svg/user-2.svg',
                                      ),
                                      const SizedBox(
                                        width: 3,
                                      ),
                                      Text(
                                        'x${state.history[index].countAdults}',
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/svg/bear.svg',
                                      ),
                                      const SizedBox(
                                        width: 3,
                                      ),
                                      Text(
                                        'x${state.history[index].countChildren}',
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  RichText(
                                    text: TextSpan(
                                      text:
                                          '${DateFormat('dd').format(DateTime.parse(state.history[index].fromDate!))}- ${DateFormat('dd MMMM').format(DateTime.parse(state.history[index].toDate!))}  .  ',
                                      style: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 13,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: '$day ночей ',
                                          style: TextStyle(
                                            color: Colors.grey[500],
                                            fontSize: 13,
                                          ),
                                        ),
                                        TextSpan(
                                          text: ' .  за ночь ',
                                          style: TextStyle(
                                            color: Colors.grey[500],
                                            fontSize: 13,
                                          ),
                                        ),
                                        TextSpan(
                                          text: '${state.history[index].totalPrice} Tт',
                                          style: const TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 17,
                              ),
                              SizedBox(
                                width: 170,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: AppColors.indigo50,
                                  ),
                                  onPressed: () async {
                                    final Pdf pdf = await BlocProvider.of<HistoryCubit>(
                                      context,
                                    ).getPdf(
                                      id: state.history[index].orderId!,
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => PdfPage(
                                          url: pdf.path!,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      SvgPicture.asset(
                                        'assets/svg/pdf.svg',
                                      ),
                                      Text(
                                        'Скачать билет',
                                        style: TextStyle(
                                          color: Colors.indigo[500],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        top: 76,
                        left: -15,
                        child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            color: AppColors.backGroundColor,
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 76,
                        right: -15,
                        child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            color: AppColors.backGroundColor,
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
