import 'dart:developer';

import 'package:embolsyn/config.dart';
import 'package:embolsyn/presentation/screens/reserve/data_picker/custom_calendar_view.dart';
import 'package:embolsyn/widgets/custom_snackbars.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CalendarPopupView extends StatefulWidget {
  const CalendarPopupView({
    super.key,
    this.initialStartDate,
    this.initialEndDate,
    this.onApplyClick,
    this.barrierDismissible = true,
    this.minimumDate,
    required this.callback,
    this.maximumDate,
  });

  final Function({
    required DateTime startDate,
    required DateTime endDate,
    required DateTime howDays,
  }) callback;

  final DateTime? minimumDate;
  final DateTime? maximumDate;
  final bool barrierDismissible;
  final DateTime? initialStartDate;
  final DateTime? initialEndDate;

  final Function(DateTime, DateTime)? onApplyClick;

  @override
  _CalendarPopupViewState createState() => _CalendarPopupViewState();
}

class _CalendarPopupViewState extends State<CalendarPopupView> with TickerProviderStateMixin {
  AnimationController? animationController;
  DateTime? startDate;
  DateTime? endDate;

  @override
  void initState() {
    Intl.systemLocale = 'ru';
    animationController = AnimationController(
      duration: const Duration(milliseconds: 400),
      vsync: this,
    );
    if (widget.initialStartDate != null) {
      startDate = widget.initialStartDate;
    }
    if (widget.initialEndDate != null) {
      endDate = widget.initialEndDate;
    }
    animationController?.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.transparent,
      body: InkWell(
        splashColor: Colors.transparent,
        focusColor: Colors.transparent,
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        onTap: () {
          if (widget.barrierDismissible) {
            Navigator.pop(context);
          }
        },
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    offset: const Offset(4, 4),
                    blurRadius: 8.0,
                  ),
                ],
              ),
              child: InkWell(
                borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                onTap: () {},
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    UIHelper.verticalSpace(32),
                    Center(
                      child: Text(
                        'Дата выселения',
                        style: TextStyle(color: AppColors.gray900, fontSize: 18, fontWeight: FontWeight.w700),
                      ),
                    ),
                    UIHelper.verticalSpace(17),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(
                                color: Colors.grey.shade600,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Заезд:',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16,
                                      color: AppColors.gray900,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    startDate != null ? DateFormat(' dd MMM').format(startDate!) : '--/--',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(
                                color: const Color(0xFF6172F3),
                                width: 3,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    endDate != null ? DateFormat(' dd MMM,EE').format(endDate!) : '--/-- ',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    const Divider(
                      height: 1,
                    ),
                    CustomCalendarView(
                      minimumDate: widget.minimumDate,
                      maximumDate: widget.maximumDate,
                      initialEndDate: widget.initialEndDate,
                      initialStartDate: widget.initialStartDate,
                      startEndDateChange: (DateTime startDateData, DateTime endDateData) {
                        setState(() {
                          startDate = startDateData;
                          endDate = endDateData;
                        });
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 16,
                        right: 16,
                        bottom: 16,
                        top: 8,
                      ),
                      child: Container(
                        height: 38,
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.6),
                              blurRadius: 8,
                              offset: const Offset(4, 4),
                            ),
                          ],
                        ),
                        child: Material(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color(0xFFFB6514),
                          child: InkWell(
                            onTap: () {
                              final difference = endDate!.difference(startDate!).inDays;
                              log(difference.toString());
                              if (difference < 5) {
                                buildErrorCustomSnackBar(
                                  context,
                                  'Выберите минимум 5 дней',
                                );
                                return;
                              }
                              // int howDays = DateFormat('dd').format(DateTime.parse(startDate)) - DateFormat('dd').format(DateTime.parse(endDate));
                              widget.callback(
                                startDate: startDate!,
                                endDate: endDate!,
                                howDays: startDate!,
                              );
                              Navigator.pop(
                                context,
                              );
                            },
                            child: const Center(
                              child: Text(
                                'Готово',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
