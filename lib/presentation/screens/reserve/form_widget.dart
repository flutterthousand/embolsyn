// ignore_for_file: type_annotate_public_apis, avoid_multiple_declarations_per_line, always_declare_return_types, unnecessary_statements, avoid_redundant_argument_values, non_constant_identifier_names

import 'dart:developer';

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/provider/reserve_provider.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class FormWidgetToAdults extends StatefulWidget {
  const FormWidgetToAdults({
    // required this.allList,
    required this.type,
    required this.indexFromList,
    //  required this.function,
    super.key,
  });
  final int indexFromList;
  final String type;
  // final List<UsersAdultsDto> allList;

  // final Function({
  //   required UsersAdultsDto userDto,
  //   required int indexFromList,
  // })? function;

  @override
  _FormWidgetToAdultsState createState() => _FormWidgetToAdultsState();
}

class _FormWidgetToAdultsState extends State<FormWidgetToAdults> {
  final nameController = TextEditingController();
  final surNameController = TextEditingController();
  final inController = TextEditingController();
  final dateController = TextEditingController();
  final termDateController = TextEditingController();
  TextEditingController nationalityController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String buttonText = 'Сохранить';
  String typeText = 'male';
  int toggleIndex = 0;

  // UsersAdultsDto? usersAdultsDto;

  bool selectedButton = false;
  final List<bool> _selectedBotton = [true, false];

  final iinFormatter = MaskTextInputFormatter(mask: '##############');

  var maskFormatter = MaskTextInputFormatter(
    mask: '####-##-##',
  );

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final prv = Provider.of<ReserveProvider>(context, listen: false);
      if (widget.type == 'parent') {
        prv.usersAdultsDto[widget.indexFromList].type = 'parent';
        prv.usersAdultsDto[widget.indexFromList].gender = typeText;
      } else {
        prv.children[widget.indexFromList].type = 'children';
        prv.usersAdultsDto[widget.indexFromList].gender = typeText;
      }
      prv.notify();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ReserveProvider>(
      builder: (context, model, child) {
        return Container(
          margin: const EdgeInsets.only(
            // left: MediaQuery.of(context).size.width * 0.004,
            left: 14, right: 14,
            top: 14,
            // bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(
              12,
            ),
            boxShadow: const [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(0, 4),
              ),
            ],
          ),
          // width: 346,
          // height: 696,
          child: Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.type == 'parent' ? 'Взрослый' : 'Ребeнок',
                  style: TextStyle(
                    color: AppColors.gray900,
                    fontWeight: FontWeight.w700,
                    fontSize: 16,
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper.verticalSpace(16),
                      Text(
                        'Фамилия',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColors.gray900,
                        ),
                      ),
                      UIHelper.verticalSpace(4),
                      TextFormField(
                        textCapitalization: TextCapitalization.sentences,
                        controller: nameController,
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: 'Фамилия',
                        ),
                        onChanged: (String value) {
                          if (widget.type == 'parent') {
                            model.usersAdultsDto[widget.indexFromList].surname = value;
                          } else {
                            model.children[widget.indexFromList].surname = value;
                          }
                          model.notify();
                        },
                        onFieldSubmitted: (String value) {
                          if (widget.type == 'parent') {
                            model.usersAdultsDto[widget.indexFromList].surname = value;
                          } else {
                            model.children[widget.indexFromList].surname = value;
                          }
                          model.notify();
                        },
                      ),
                      UIHelper.verticalSpace(16),
                      Text(
                        'Имя',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColors.gray900,
                        ),
                      ),
                      UIHelper.verticalSpace(4),
                      TextFormField(
                        textCapitalization: TextCapitalization.sentences,
                        controller: surNameController,
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: 'Имя',
                        ),
                        onChanged: (String value) {
                          if (widget.type == 'parent') {
                            model.usersAdultsDto[widget.indexFromList].name = value;
                          } else {
                            model.children[widget.indexFromList].name = value;
                          }
                          model.notify();
                        },
                        onFieldSubmitted: (String value) {
                          if (widget.type == 'parent') {
                            model.usersAdultsDto[widget.indexFromList].name = value;
                          } else {
                            model.children[widget.indexFromList].name = value;
                          }
                          model.notify();
                        },
                      ),
                      UIHelper.verticalSpace(16),
                      Row(
                        children: [
                          ToggleButtons(
                            borderRadius: BorderRadius.circular(7),
                            color: Colors.white,
                            fillColor: AppColors.indigo500,
                            selectedColor: Colors.white,
                            // ignore: sort_child_properties_last
                            children: [
                              pad_name_togle_buttons(
                                widget.type == 'parent' ? "Мужчина" : 'Мальчик ',
                              ),
                              pad_name_togle_buttons(
                                widget.type == 'parent' ? "Женщина" : 'Девочка',
                              ),
                            ],
                            isSelected: _selectedBotton,
                            onPressed: (int index) {
                              setState(
                                () {
                                  toggleIndex = index;
                                  if (index == 0) {
                                    typeText = 'male';
                                  } else {
                                    typeText = 'female';
                                  }
                                  for (int i = 0; i < _selectedBotton.length; i++) {
                                    _selectedBotton[i] = i == index;
                                  }
                                  log(typeText);
                                  if (widget.type == 'parent') {
                                    model.usersAdultsDto[widget.indexFromList].gender = typeText;
                                  } else {
                                    model.children[widget.indexFromList].gender = typeText;
                                  }
                                  model.notify();
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      UIHelper.verticalSpace(16),
                      Text(
                        'Дата рождения',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColors.gray900,
                        ),
                      ),
                      UIHelper.verticalSpace(4),
                      TextFormField(
                        onTap: () async {
                          DateTime date = DateTime(1900);
                          FocusScope.of(context).requestFocus(FocusNode());

                          date = (await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100),
                          ))!;

                          dateController.text =
                              '${date.year.toString()}-${date.month.toString()}-${date.day.toString()}';

                          if (widget.type == 'parent') {
                            model.usersAdultsDto[widget.indexFromList].dateBirth = dateController.text;
                          } else {
                            model.children[widget.indexFromList].dateBirth = dateController.text;
                          }
                          model.notify();
                        },
                        inputFormatters: [maskFormatter],
                        controller: dateController,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: 'ГГГГ-MM-ДД',
                        ),
                      ),
                      UIHelper.verticalSpace(10),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  SizedBox pad_name_togle_buttons(String nameButtonTogle) {
    return SizedBox(
      width: (MediaQuery.of(context).size.width - 64) / 2,
      // height: MediaQuery.of(context).size.height * 0.9,
      child: Text(
        nameButtonTogle,
        style: const TextStyle(color: Colors.black),
        textAlign: TextAlign.center,
      ),
    );
  }
}
