// ignore_for_file: avoid_redundant_argument_values

import 'dart:io';

import 'package:embolsyn/config.dart';

import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class OnlinePayment extends StatefulWidget {
  final String url;
  const OnlinePayment({required this.url, super.key});

  @override
  State<OnlinePayment> createState() => _OnlinePaymentState();
}

class _OnlinePaymentState extends State<OnlinePayment> {
  bool isCvvFocused = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backGroundColor,
      appBar: AppBar(
        title: const Text(
          'Онлайн оплата',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
            // Navigator.pop(context);
            Navigator.pop(context);
            // Navigator.pushAndRemoveUntil(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) => const Base(
            //       index: 0,
            //     ),
            //   ),
            //   (route) => false,
            // );
          },
        ),
      ),
      body: SafeArea(
        child: WebView(
          initialUrl: widget.url,
          javascriptMode: JavascriptMode.unrestricted,
        ),
      ),
    );
  }
}
