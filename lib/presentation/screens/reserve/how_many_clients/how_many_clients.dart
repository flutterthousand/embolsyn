// // ignore_for_file: always_use_package_imports, must_be_immutable

// import 'dart:developer';

// import 'package:embolsyn/presentation/screens/reserve/reserve_page.dart';
// import 'package:embolsyn/widgets/default_button.dart';
// import 'package:flutter/material.dart';

// import '../../../../config.dart';

// class HowManyClientsPage extends StatefulWidget {
//   DateTime startDate;
//   DateTime endDate;
//   String title;
//   int sanatoriumRoomId;
//   DateTime howDays;

//   HowManyClientsPage({
//     Key? key,
//     required this.startDate,
//     required this.endDate,
//     required this.title,
//     required this.sanatoriumRoomId,
//     required this.howDays,
//   }) : super(key: key);

//   @override
//   State<HowManyClientsPage> createState() => _HowManyClientsPageState();
// }

// class _HowManyClientsPageState extends State<HowManyClientsPage> {
//   int countAdults = 0;
//   int countChildren = 0;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: ListView(
//         children: [
//           const Padding(
//             padding: EdgeInsets.only(top: 32),
//             child: Center(
//               child: Text(
//                 'Сколько вас?',
//                 style: TextStyle(
//                   color: Color(0xFF101828),
//                   fontSize: 18,
//                   fontWeight: FontWeight.w700,
//                 ),
//               ),
//             ),
//           ),
//           ListTile(
//             title: Text(
//               'Взрослые',
//               style: TextStyle(
//                 color: AppColors.gray900,
//                 fontSize: 16,
//                 fontWeight: FontWeight.w700,
//               ),
//             ),
//             subtitle: const Text('От полных 15 лет'),
//             trailing: SizedBox(
//               width: 110,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   ClipOval(
//                     child: Material(
//                       color: Colors.grey[300], // Button color
//                       child: InkWell(
//                         splashColor: AppColors.darkColor, // Splash color
//                         onTap: () {
//                           setState(() {
//                             if (countAdults > 0) countAdults--;
//                           });
//                         },
//                         child: const SizedBox(
//                           width: 40,
//                           height: 40,
//                           child: Icon(Icons.remove),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Text('$countAdults'),
//                   ClipOval(
//                     child: Material(
//                       color: AppColors.indigo500, // Button color
//                       child: InkWell(
//                         splashColor: AppColors.darkColor, // Splash color
//                         onTap: () {
//                           setState(() {
//                             countAdults++;
//                           });
//                         },
//                         child: const SizedBox(
//                           width: 40,
//                           height: 40,
//                           child: Icon(
//                             Icons.add,
//                             color: Colors.white,
//                           ),
//                         ),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           ListTile(
//             title: Text(
//               'Дети',
//               style: TextStyle(
//                 color: AppColors.gray900,
//                 fontSize: 16,
//                 fontWeight: FontWeight.w700,
//               ),
//             ),
//             subtitle: const Text('От 2 до 15 лет'),
//             trailing: SizedBox(
//               width: 110,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   ClipOval(
//                     child: Material(
//                       color: Colors.grey[300], // Button color
//                       child: InkWell(
//                         splashColor: AppColors.darkColor, // Splash color
//                         onTap: () {
//                           setState(() {
//                             if (countChildren > 0) countChildren--;
//                           });
//                         },
//                         child: const SizedBox(
//                           width: 40,
//                           height: 40,
//                           child: Icon(Icons.remove),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Text('$countChildren'),
//                   ClipOval(
//                     child: Material(
//                       color: AppColors.indigo500, // Button color
//                       child: InkWell(
//                         splashColor: AppColors.darkColor, // Splash color
//                         onTap: () {
//                           setState(() {
//                             countChildren++;
//                           });
//                         },
//                         child: const SizedBox(
//                           width: 40,
//                           height: 40,
//                           child: Icon(
//                             Icons.add,
//                             color: Colors.white,
//                           ),
//                         ),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           ListTile(
//             title: Text(
//               'Младенцы',
//               style: TextStyle(
//                 color: AppColors.gray900,
//                 fontSize: 16,
//                 fontWeight: FontWeight.w700,
//               ),
//             ),
//             subtitle: const Text('До 2 лет, без места'),
//             trailing: SizedBox(
//               width: 110,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   ClipOval(
//                     child: Material(
//                       color: Colors.grey[300], // Button color
//                       child: InkWell(
//                         splashColor: AppColors.darkColor, // Splash color
//                         onTap: () {
//                           setState(() {
//                             if (countInfants > 0) countInfants--;
//                           });
//                         },
//                         child: const SizedBox(
//                           width: 40,
//                           height: 40,
//                           child: Icon(Icons.remove),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Text('$countInfants'),
//                   ClipOval(
//                     child: Material(
//                       color: AppColors.indigo500, // Button color
//                       child: InkWell(
//                         splashColor: AppColors.darkColor, // Splash color
//                         onTap: () {
//                           setState(() {
//                             countInfants++;
//                           });
//                         },
//                         child: const SizedBox(
//                           width: 40,
//                           height: 40,
//                           child: Icon(
//                             Icons.add,
//                             color: Colors.white,
//                           ),
//                         ),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           Padding(
//             padding:
//                 const EdgeInsets.only(left: 13, right: 13, top: 24, bottom: 36),
//             child: DefaultButton(
//               text: 'Готово',
//               press: () {
//                 log('hello');
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) => ReservePage(
//                       countAdults: countAdults,
//                       countChildren: countChildren,

//                       startDate: widget.startDate,
//                       endDate: widget.endDate,
//                       title: widget.title,
//                       sanatoriumRoomId: widget.sanatoriumRoomId,
//                       howDays: widget.howDays,
//                     ),
//                   ),
//                 );
//               },
//               color: AppColors.orange,
//               width: 343,
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
