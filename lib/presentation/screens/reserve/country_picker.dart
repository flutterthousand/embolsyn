import 'dart:developer';

import 'package:flutter/material.dart';

class NationalPicker extends StatefulWidget {
  const NationalPicker({super.key});

  @override
  State<NationalPicker> createState() => _NationalPickerState();
}

class _NationalPickerState extends State<NationalPicker> {
  final _controller = TextEditingController();

  // @override
  // void initState() {
  //   super.initState();
  //   BlocProvider.of<CitiesCubit>(context).getCity();
  // }
  List<String> nationality = [
    'Казахстан',
    'Россия',
    'Узбекстан',
    'Китай',
    'Киргизстан',
    'Монголия',
    'Украина',
    'Турция'
  ];
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String natiName = '';
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Гражданство'),
      ),
      body: ListView(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
            child: TextField(
              controller: _controller,
              onTap: () async {
                // _buildCities(context);
                // // should show search screen here
                // showSearch(
                //   context: context,
                //   // we haven't created AddressSearch class
                //   // this should be extending SearchDelegate
                //   delegate: AddressSearch(),
                // );
              },
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                filled: true,
                fillColor: const Color(0xFFF2F4F7),
                hintText: 'Гражданство',
                contentPadding: const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(8),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
          ),
          ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: nationality.length,
                separatorBuilder: (BuildContext context, int index) => const Divider(),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      // final Prefs _prefs = Prefs();
                      // _prefs.saveCityId(state.cities[index].id);
                      natiName = nationality[index];
                      log(natiName);
                      Navigator.pop(
                        context,
                        natiName,
                      );
                    },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(
                        vertical: 17,
                        horizontal: 19,
                      ),
                      child: Text(
                        nationality[index],
                        // state.cities[index].name,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
