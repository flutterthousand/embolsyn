// ignore_for_file: always_use_package_imports, avoid_redundant_argument_values, non_constant_identifier_names, unused_local_variable

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/book_cubit/book_cubit.dart';

import 'package:embolsyn/data/provider/reserve_provider.dart';
import 'package:embolsyn/presentation/screens/reserve/form_widget.dart';
import 'package:embolsyn/presentation/screens/reserve/online/online_payment_page.dart';
import 'package:embolsyn/widgets/custom_snackbars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ReservePage extends StatefulWidget {
  final int countAdults;
  final int countChildren;
  final DateTime startDate;
  final DateTime endDate;
  final String title;
  final int sanatoriumRoomId;
  final String howDays;
  
  const ReservePage({
    super.key,
    required this.countAdults,
    required this.title,
    required this.countChildren,
    required this.startDate,
    required this.endDate,
    required this.sanatoriumRoomId,
    required this.howDays,
  });

  @override
  State<ReservePage> createState() => _ReservePageState();
}

class _ReservePageState extends State<ReservePage> {
  int inNumber = 0;
  int date = 0;
  int termDate = 0;
  String name = '';
  String surName = '';

  @override
  void initState() {
    Provider.of<ReserveProvider>(context, listen: false).init(
      countAdults: widget.countAdults,
      countChildren: widget.countChildren,
    );
    BlocProvider.of<BookCubit>(context).reset();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ReserveProvider>(
      builder: (context, model, child) {
        return BlocConsumer<BookCubit, BookState>(
          listener: (context, state) {
            if (state is LoadedState) {
              // model.resetAll();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OnlinePayment(
                    url: state.url.url!,
                  ),
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is InitialState) {
              return GestureDetector(
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  appBar: AppBar(
                    title: const Text(
                      'Бронирование номера',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                  body: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                            // left: MediaQuery.of(context).size.width * 0.004,
                            left: 14, right: 14,
                            top: 14,
                          ),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12.0),
                              topRight: Radius.circular(12.0),
                              bottomLeft: Radius.circular(8.0),
                              bottomRight: Radius.circular(8.0),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                blurRadius: 1,
                                spreadRadius: 1,
                                offset: Offset(0, 4),
                              ),
                            ],
                          ),
                          //width: 346,
                          height: 140,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Text(
                                            'Suit+',
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              color: AppColors.gray900,
                                            ),
                                          ),
                                          Text('${widget.howDays} ночей'),
                                          Text(
                                            '${widget.startDate.day}-${widget.endDate.day} ${DateFormat.MMMM().format(widget.startDate)}',
                                          ),
                                          const CircleAvatar(
                                            backgroundImage: AssetImage('assets/avatar.png'),
                                          )
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 16),
                                        child: Row(
                                          children: [
                                            Row(
                                              children: [
                                                SvgPicture.asset(
                                                  'assets/svg/user-2.svg',
                                                ),
                                                const SizedBox(
                                                  width: 3,
                                                ),
                                                Text(
                                                  'x${widget.countAdults}',
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600,
                                                    color: AppColors.gray900,
                                                  ),
                                                )
                                              ],
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            Row(
                                              children: [
                                                SvgPicture.asset(
                                                  'assets/svg/bear.svg',
                                                ),
                                                const SizedBox(
                                                  width: 3,
                                                ),
                                                Text(
                                                  'x${widget.countChildren}',
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600,
                                                    color: AppColors.gray900,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      UIHelper.verticalSpace(10),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 18),
                                        child: Text(
                                          'Санаторий ${widget.title}',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            color: AppColors.gray900,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              // Container(
                              //   decoration: BoxDecoration(
                              //     color: AppColors.indigo50,
                              //     borderRadius: const BorderRadius.only(
                              //       bottomLeft: Radius.circular(12.0),
                              //       bottomRight: Radius.circular(12.0),
                              //     ),
                              //   ),
                              //   height: 36,
                              // child: Center(
                              //   child: Text(
                              //     'Детали номера',
                              //     style: TextStyle(
                              //       fontSize: 12,
                              //       color: AppColors.indigo500,
                              //       fontWeight: FontWeight.w600,
                              //     ),
                              //   ),
                              // ),
                              // ),
                            ],
                          ),
                        ),
                        UIHelper.verticalSpace(16),
                        ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          separatorBuilder: (context, index) {
                            return const SizedBox(
                              height: 16,
                            );
                          },
                          itemBuilder: (BuildContext context, int index) {
                            return FormWidgetToAdults(
                              type: "parent",
                              indexFromList: index,
                            );
                          },
                          itemCount: widget.countAdults,
                        ),
                        const SizedBox(
                          height: 32,
                        ),
                        ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          separatorBuilder: (context, index) {
                            return const SizedBox(
                              height: 16,
                            );
                          },
                          itemBuilder: (BuildContext context, int index) {
                            return FormWidgetToAdults(
                              type: "children",
                              indexFromList: index,
                            );
                          },
                          itemCount: widget.countChildren,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 16,
                            right: 16,
                            top: 16,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.height * 0.2,
                                height: 50,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    backgroundColor: Colors.orange,
                                  ),
                                  onPressed: () {
                                    if (model.chechIsAllFilled()) {
                                      BlocProvider.of<BookCubit>(context).rentRoom(
                                        users: model.children + model.usersAdultsDto,
                                        countChildren: widget.countChildren,
                                        countParents: widget.countAdults,
                                        toDate: DateFormat('y-M-d').format(widget.endDate),
                                        fromDate: DateFormat('y-M-d').format(widget.startDate),
                                        sanatoriumRoomId: widget.sanatoriumRoomId,
                                      );
                                    } else {
                                      buildErrorCustomSnackBar(
                                        context,
                                        "Заполните все поля!",
                                      );
                                    }
                                  },
                                  child: const Text(
                                    'Оплатить картой',
                                    style: TextStyle(
                                      fontFamily: 'Inter',
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.height * 0.21,
                                height: 50,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    backgroundColor: Colors.red,
                                  ),
                                  onPressed: () {
                                    if (model.chechIsAllFilled()) {
                                      BlocProvider.of<BookCubit>(context).rentRoomWithoutPayment(
                                        users: model.children + model.usersAdultsDto,
                                        countChildren: widget.countChildren,
                                        countParents: widget.countAdults,
                                        toDate: DateFormat('y-M-d').format(widget.endDate),
                                        fromDate: DateFormat('y-M-d').format(widget.startDate),
                                        sanatoriumRoomId: widget.sanatoriumRoomId,
                                      );
                                      launch(
                                        'https://pay.kaspi.kz/pay/bkyzfxao',
                                      );
                                    }
                                  },
                                  child: const Text(
                                    'Оплатить c Kaspi',
                                    style: TextStyle(
                                      fontFamily: 'Inter',
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        UIHelper.verticalSpace(30),
                      ],
                    ),
                  ),
                ),
              );
            } else if (state is LoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.white,
              ),
            );
          },
        );
      },
    );
  }
}
