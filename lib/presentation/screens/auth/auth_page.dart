// ignore_for_file: must_be_immutable, unnecessary_statements, non_constant_identifier_names, unnecessary_string_interpolations

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/auth_cubit/auth_cubit.dart';
import 'package:embolsyn/presentation/screens/auth/auth_verify.dart';
import 'package:embolsyn/widgets/custom_snackbars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class AuthPage extends StatefulWidget {
  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  Widget text = Text.rich(
    TextSpan(
      // with no TextStyle it will have default text style
      text: 'Нажимая на кнопку я соглашаюсь с ',
      style: const TextStyle(
        fontSize: 12.5,
        // color: Colors.black,
        fontFamily: 'Montserrat',
      ),
      children: <TextSpan>[
        TextSpan(
          text: 'публичной офертой',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: AppColors.orange,
          ),
        ),
      ],
    ),
  );

  final maskFormatter = MaskTextInputFormatter(mask: '+7(###)-###-##-##');

  TextEditingController phoneController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  int currentIndex = 0;

  int cityId = -1;
  String cityName = 'Город';

  final List<bool> selectedBotton = [true, false];
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final authCubit = BlocProvider.of<AuthCubit>(context);
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is ErrorState) {
          buildErrorCustomSnackBar(context, state.message);
        }
        if (state is LoadedState) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AuthVerify(
                number: phoneController.text,
              ),
            ),
          );
        }
      },
      builder: (context, state) {
        if (state is LoadingState) {
          return const Scaffold(
            backgroundColor: Colors.white10,
            body: Padding(
              padding: EdgeInsets.all(80),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
        return Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(300.0),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 240,
                  decoration: BoxDecoration(
                    image: const DecorationImage(
                      alignment: Alignment.bottomCenter,
                      image: AssetImage("assets/images/header.png"),
                    ),
                    color: AppColors.darkColor,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: screenSize.height * 0.04,
                  ),
                  child: ToggleButtons(
                    borderRadius: BorderRadius.circular(7),
                    color: Colors.black,
                    fillColor: AppColors.gray600,
                    selectedColor: Colors.white,
                    // ignore: sort_child_properties_last
                    children: [
                      pad_name_togle_buttons('  Вход     '),
                      pad_name_togle_buttons('Регистрация'),
                    ],
                    isSelected: selectedBotton,
                    onPressed: (int index) {
                      setState(
                        () {
                          for (int i = 0; i < selectedBotton.length; i++) {
                            selectedBotton[i] = i == index;
                          }
                          currentIndex = index;
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          body: IndexedStack(
            index: currentIndex,
            children: [
              authPage(context, authCubit),
              registerPage(context, authCubit),
            ],
          ),
        );
      },
    );
  }

  ListView authPage(BuildContext context, AuthCubit authCubit) {
    const text = Text.rich(
      TextSpan(
        // with no TextStyle it will have default text style
        text: 'Нажимая на кнопку я соглашаюсь с ',
        style: TextStyle(
          fontSize: 12.5,
          // color: Colors.black,
          fontFamily: 'Montserrat',
        ),
        children: <TextSpan>[
          TextSpan(
            text: 'публичной офертой',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.orange,
              fontSize: 12.5,
            ),
          ),
        ],
      ),
    );
    return ListView(
      children: [
        InkWell(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: [
                const SizedBox(height: 30),
                const Text(
                  'Вход в кабинет',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                ),
                const SizedBox(height: 5),
                Text(
                  "Общайтесь в чате с поддержкой, первыми бронируйте номера в санаторий без переплат",
                  style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                  textAlign: TextAlign.center,
                ),
                TextField(
                  inputFormatters: [maskFormatter],
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.phone,
                  controller: phoneController,
                  decoration: const InputDecoration(
                    hintText: "Номер телефон",
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  width: MediaQuery.of(context).size.width,
                  child: TextButton(
                    onPressed: () {
                      final String phone = phoneController.text;
                      phoneController.value.text.isNotEmpty
                          ? authCubit.auth(phone, context)
                          : null;
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.orange),
                    ),
                    child: const Text(
                      'Продолжить',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                text,
              ],
            ),
          ),
        )
      ],
    );
  }

  ListView registerPage(BuildContext context, AuthCubit authCubit) {
    const text = Text.rich(
      TextSpan(
        // with no TextStyle it will have default text style
        text: 'Нажимая на кнопку я соглашаюсь с ',
        style: TextStyle(
          fontSize: 12.5,
          // color: Colors.black,
          fontFamily: 'Montserrat',
        ),
        children: <TextSpan>[
          TextSpan(
            text: 'публичной офертой',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.orange,
              fontSize: 12.5,
            ),
          ),
        ],
      ),
    );
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              const SizedBox(height: 30),
              const Text(
                'Регистрация',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
              const SizedBox(height: 5),
              Text(
                "Общайтесь в чате с поддержкой, первыми бронируйте номера в санаторий без переплат",
                style: TextStyle(fontSize: 14, color: Colors.grey[500]),
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpace(14),
              Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: Offset(0, 1),
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 15,
                    right: 15,
                    top: 15,
                    bottom: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Имя',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColors.gray900,
                        ),
                      ),
                      UIHelper.verticalSpace(4),
                      TextFormField(
                        controller: nameController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: 'Имя',
                        ),
                      ),
                      UIHelper.verticalSpace(16),
                      Text(
                        'Фамилия',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColors.gray900,
                        ),
                      ),
                      UIHelper.verticalSpace(4),
                      TextFormField(
                        controller: surnameController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: 'Фамилия',
                        ),
                      ),
                      UIHelper.verticalSpace(16),
                      Text(
                        'Номер телефона',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColors.gray900,
                        ),
                      ),
                      UIHelper.verticalSpace(4),
                      TextFormField(
                        inputFormatters: [maskFormatter],
                        controller: phoneController,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: '+7(777) 777-77-77',
                        ),
                      ),
                      UIHelper.verticalSpace(16),
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 20),
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                  onPressed: () {
                    authCubit.register(
                      phoneController.text,
                      nameController.text,
                      surnameController.text,
                      context,
                    );
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.orange),
                  ),
                  child: const Text(
                    'Создать аккаунт',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              text,
              UIHelper.verticalSpace(22)
            ],
          ),
        )
      ],
    );
  }

  Padding pad_name_togle_buttons(String nameButtonTogle) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: Text(nameButtonTogle),
    );
  }
}
