// ignore_for_file: avoid_redundant_argument_values, avoid_print, must_be_immutable

import 'dart:async';

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/auth_verify_cubit/auth_verify_cubit.dart';
import 'package:embolsyn/data/cubit/navigation/navigation_cubit.dart';
import 'package:embolsyn/presentation/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class AuthVerify extends StatefulWidget {
  String number;
  AuthVerify({Key? key, required this.number}) : super(key: key);

  @override
  _AuthVerifyState createState() => _AuthVerifyState();
}

class _AuthVerifyState extends State<AuthVerify> {
  TextEditingController textEditingController = TextEditingController();
  StreamController<ErrorAnimationType>? errorController;

  bool hasError = false;
  final formKey = GlobalKey<FormState>();

  int _start = 59;
  bool enableTimer = false;
  bool onReady = false;

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    Timer.periodic(
      oneSec,
      (Timer timer) {
        if (!mounted) return;
        if (_start == 0) {
          setState(() {
            timer.cancel();
            enableTimer = true;
            onReady = true;
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    startTimer();
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController!.close();

    super.dispose();
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> snackBar(
    String? message,
  ) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message!),
        duration: const Duration(seconds: 2),
      ),
    );
  }

  List<bool> selectedBotton = [true, false];
  @override
  Widget build(BuildContext context) {
    final navigationCubit = BlocProvider.of<NavigationCubit>(context);

    return BlocConsumer<AuthVerifyCubit, AuthVerifyState>(
      listener: (context, state) {
        if (state is ErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(state.message),
            ),
          );
        }
        if (state is LoadedState) {
          navigationCubit.getNavBarItem(const NavigationState.profile());
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const Base(
                index: 0,
              ),
            ),
            (Route<dynamic> route) => false,
          );
        }
      },
      builder: (context, state) {
        if (state is LoadingState) {
          return const Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        return Scaffold(
          appBar: AppBar(
            toolbarHeight: 90,
            title: const Text('Подтверждение'),
          ),
          body: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  children: [
                    const SizedBox(height: 30),
                    const Text(
                      'Введите код из SMS',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    ),
                    const SizedBox(height: 12),
                    Text(
                      'На ${widget.number} отправлен SMS-код',
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Form(
                      key: formKey,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 8.0,
                          horizontal: 30,
                        ),
                        child: PinCodeTextField(
                          length: 4,
                          obscureText: false,
                          keyboardType: TextInputType.phone,
                          pinTheme: PinTheme(
                            activeColor: Colors.green,
                            inactiveColor: AppColors.orange,
                            activeFillColor: Colors.white,
                          ),
                          onChanged: (value) {
                            print(value);
                          },
                          onCompleted: (value) async {
                            if (value.length == 4) {
                              await BlocProvider.of<AuthVerifyCubit>(context)
                                  .send(value);
                            }
                          },
                          appContext: context,
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        if (onReady) {
                          setState(() {
                            enableTimer = false;
                            _start = 59;
                            onReady = false;
                          });
                          startTimer();
                        }
                      },
                      child: Center(
                        child: Text(
                          'Вы можете отправить код снова через ${_start < 10 ? '0$_start' : _start.toString()} секунд',
                          style: const TextStyle(color: Colors.black),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
