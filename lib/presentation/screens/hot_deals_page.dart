// ignore_for_file: sort_child_properties_last

import 'dart:developer';

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/advertisement_cubit/advertisement_cubit.dart';
import 'package:embolsyn/presentation/screens/sanatorium_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HotDealsPage extends StatefulWidget {
  const HotDealsPage({Key? key}) : super(key: key);

  @override
  _HotDealsPageState createState() => _HotDealsPageState();
}

class _HotDealsPageState extends State<HotDealsPage> {
  @override
  void initState() {
    // Intl.systemLocale = 'ru';
    super.initState();

    BlocProvider.of<AdvertisementCubit>(context).getAdvertisement();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Горячие предложения'),
        ),
      ),
      body: BlocBuilder<AdvertisementCubit, AdvertisementState>(
        builder: (context, state) {
          if (state is LoadedState) {
            return ListView.separated(
              itemCount: state.advert.length,
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    log('${ state.advert[index].sanatoriumId!}');
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SanatoriumPage(
                          sanatoriumId: state.advert[index].sanatoriumId!,
                        ),
                      ),
                    );
                    // Get.to( () =>  SanatoriumPage());
                  },
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Image.network(
                          '$SERVER_HOST/${state.advert[index].image}',
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                          left: 11,
                          right: 11,
                          bottom: 30,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                if (state.advert[index].sanatorium == null)
                                  const SizedBox()
                                else
                                  Text(
                                    'Санаторий \n${state.advert[index].sanatorium}',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.grey[900],
                                    ),
                                  ),
                                TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    '${state.advert[index].price} Tт',
                                    style: TextStyle(color: Colors.indigo[500]),
                                  ),
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Colors.indigo[100],
                                    ),
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Text(
                              '${state.advert[index].description}',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.grey[500],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
