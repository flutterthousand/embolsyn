import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/cities_cubit/cities_cubit.dart';
import 'package:embolsyn/data/models/city.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AdressSearch extends StatefulWidget {
  const AdressSearch({super.key});

  @override
  State<AdressSearch> createState() => _AdressSearchState();
}

class _AdressSearchState extends State<AdressSearch> {
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<CitiesCubit>(context).getCities();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Поиск города'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
            child: TextField(
              controller: _controller,
              onChanged: (value) {
                if (_controller.text.isEmpty) {
                  BlocProvider.of<CitiesCubit>(context).getCities();
                } else {
                  BlocProvider.of<CitiesCubit>(context).searchCity(city: value);
                }
              },
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                filled: true,
                fillColor: const Color(0xFFF2F4F7),
                hintText: 'Город',
                contentPadding: const EdgeInsets.only(left: 14.0, top: 16.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(8),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 278),
            color: AppColors.indigo50,
            child: Text(
              'Вы уже искали',
              style: TextStyle(
                color: AppColors.indigo500,
                fontWeight: FontWeight.w600,
                fontSize: 12,
              ),
            ),
          ),
          ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              BlocBuilder<CitiesCubit, CitiesState>(
                builder: (context, state) {
                  return state.maybeWhen(
                    searchedState: (cities) {
                      return _CitiiesListView(
                        cities: cities,
                      );
                    },
                    loadedState: (cities) {
                      return _CitiiesListView(
                        cities: cities,
                      );
                    },
                    orElse: () {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _CitiiesListView extends StatelessWidget {
  final List<City> cities;
  const _CitiiesListView({
    required this.cities,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: cities.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            final Prefs prefs = Prefs();
            prefs.saveCityId(cities[index].id);
            Navigator.pop(context, cities[index]);
          },
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: 17,
              horizontal: 19,
            ),
            child: Text(
              cities[index].name,
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
            ),
          ),
        );
      },
    );
  }
}
