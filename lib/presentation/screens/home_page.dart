// ignore_for_file: avoid_print, unnecessary_brace_in_string_interps

import 'dart:developer';

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/cities_cubit/cities_cubit.dart' as city;

import 'package:embolsyn/data/cubit/home_cubit/home_cubit.dart';
import 'package:embolsyn/data/models/city.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:embolsyn/data/provider/reserve_provider.dart';
import 'package:embolsyn/presentation/screens/adress_search/adress_search.dart';
import 'package:embolsyn/presentation/screens/reserve/data_picker/calendar_popup_view.dart';
import 'package:embolsyn/presentation/screens/sanatorium_page.dart';
import 'package:embolsyn/widgets/custom_snackbars.dart';
import 'package:embolsyn/widgets/default_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String cityName = 'Город';

  final _controller = TextEditingController();
  final _search = TextEditingController();

  DateTime? howDaysTo;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<city.CitiesCubit>(context).getCities();
  }

  @override
  void dispose() {
    _controller.dispose();
    _search.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ReserveProvider>(
      builder: (context, model, state) {
        return BlocBuilder<HomeCubit, HomeState>(
          builder: (context, state) {
            return state.maybeWhen(
              initialState: () => Scaffold(
                backgroundColor: Colors.grey.shade200,
                body: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: Container(
                        height: 310,
                        decoration: BoxDecoration(
                          color: AppColors.darkColor,
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const SizedBox(
                                    height: 40,
                                  ),
                                  Center(
                                    child: SvgPicture.asset(
                                      'assets/logo.svg',
                                      fit: BoxFit.cover,
                                      height: 150,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 50,
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                                child: Container(
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.vertical(
                                  top: Radius.elliptical(350, 40),
                                ),
                                color: Colors.grey.shade200,
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                    Positioned.fill(
                      child: ListView(
                        padding: const EdgeInsets.all(16),
                        children: [
                          const SizedBox(
                            height: 260,
                          ),
                          SizedBox(
                            height: 56,
                            width: double.infinity,
                            child: GestureDetector(
                              onTap: () async {
                                final City? city = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => const AdressSearch(),
                                  ),
                                );
                                if (city != null) {
                                  setState(() {
                                    cityName = city.name;
                                  });
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                // borderRadius: BorderRadius.circular(6.0),

                                child: ListTile(
                                  leading: cityName != null
                                      ? Text(
                                          cityName,
                                          style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF667085),
                                          ),
                                        )
                                      : const Text(
                                          'Город',
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF667085),
                                          ),
                                        ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Container(
                            height: 76,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: ListTile(
                              title: Text(
                                'Взрослые',
                                style: TextStyle(
                                  color: AppColors.gray900,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              subtitle: const Text('От полных 15 лет'),
                              trailing: SizedBox(
                                width: 110,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ClipOval(
                                      child: Material(
                                        color: Colors.grey[300], // Button color
                                        child: InkWell(
                                          splashColor: AppColors
                                              .darkColor, // Splash color
                                          onTap: () {
                                            if (model.countAdults! > 0) {
                                              model.countAdults =
                                                  model.countAdults! - 1;
                                            }
                                          },
                                          child: const SizedBox(
                                            width: 40,
                                            height: 40,
                                            child: Icon(Icons.remove),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '${model.countAdults == null ? model.countAdults ?? 0 : model.countAdults!}',
                                    ),
                                    ClipOval(
                                      child: Material(
                                        color:
                                            AppColors.indigo500, // Button color
                                        child: InkWell(
                                          splashColor: AppColors
                                              .darkColor, // Splash color
                                          onTap: () {
                                            model.countAdults ??= 0;
                                            model.countAdults =
                                                model.countAdults! + 1;
                                          },
                                          child: const SizedBox(
                                            width: 40,
                                            height: 40,
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Container(
                            height: 76,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: ListTile(
                              title: Text(
                                'Дети',
                                style: TextStyle(
                                  color: AppColors.gray900,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              subtitle: const Text('От 2 до 15 лет'),
                              trailing: SizedBox(
                                width: 110,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ClipOval(
                                      child: Material(
                                        color: Colors.grey[300], // Button color
                                        child: InkWell(
                                          splashColor: AppColors
                                              .darkColor, // Splash color
                                          onTap: () {
                                            if (model.countChildren! > 0) {
                                              model.countChildren =
                                                  model.countChildren! - 1;
                                            }
                                          },
                                          child: const SizedBox(
                                            width: 40,
                                            height: 40,
                                            child: Icon(Icons.remove),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '${model.countChildren == null ? model.countChildren ?? 0 : model.countChildren!}',
                                    ),
                                    ClipOval(
                                      child: Material(
                                        color:
                                            AppColors.indigo500, // Button color
                                        child: InkWell(
                                          splashColor: AppColors
                                              .darkColor, // Splash color
                                          onTap: () {
                                            model.countChildren ??= 0;
                                            model.countChildren =
                                                model.countChildren! + 1;
                                          },
                                          child: const SizedBox(
                                            width: 40,
                                            height: 40,
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Container(
                            height: 76,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8, right: 8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        top: 8.0,
                                        bottom: 8,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Padding(
                                            padding: EdgeInsets.only(left: 7.0),
                                            child: Text(
                                              "Дата начала",
                                              style: TextStyle(
                                                color: Color(0xFF475467),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 10.0,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              showModalBottomSheet(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    12.0,
                                                  ),
                                                ),
                                                isScrollControlled: true,
                                                context: context,
                                                builder: (context) {
                                                  {}
                                                  return SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.69,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: CalendarPopupView(
                                                        minimumDate: DateTime.now(),
                                                        callback: ({
                                                          required DateTime
                                                              startDate,
                                                          required DateTime
                                                              endDate,
                                                          required DateTime
                                                              howDays,
                                                        }) {
                                                          setState(() {
                                                            model.startDateTo =
                                                                startDate;
                                                            model.endDateTo =
                                                                endDate;
                                                            howDaysTo = howDays;
                                                          });
                                                        },
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                                left: 7,
                                              ),
                                              child: Container(
                                                width: double.infinity,
                                                decoration: const BoxDecoration(
                                                  border: Border(
                                                    bottom: BorderSide(),
                                                  ),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      model.startDateTo == null
                                                          ? 'Выберите дату'
                                                          : DateFormat(
                                                              'd MMM y ',
                                                            ).format(
                                                              model
                                                                  .startDateTo!,
                                                            ),
                                                    ),
                                                    const Icon(
                                                      Icons.calendar_today,
                                                      color: Colors.blue,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 45.0,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        top: 8.0,
                                        bottom: 8,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            "Дата окончания",
                                            style: TextStyle(
                                              color: Color(0xFF475467),
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 10.0,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              showModalBottomSheet(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    12.0,
                                                  ),
                                                ),
                                                isScrollControlled: true,
                                                context: context,
                                                builder: (context) {
                                                  {}
                                                  return SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.69,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: CalendarPopupView(
                                                        callback: ({
                                                          required DateTime
                                                              startDate,
                                                          required DateTime
                                                              endDate,
                                                          required DateTime
                                                              howDays,
                                                        }) {
                                                          setState(() {
                                                            model.startDateTo =
                                                                startDate;
                                                            model.endDateTo =
                                                                endDate;
                                                            howDaysTo = howDays;
                                                          });
                                                        },
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                right: 14,
                                              ),
                                              child: Container(
                                                width: double.infinity,
                                                decoration: const BoxDecoration(
                                                  border: Border(
                                                    bottom: BorderSide(),
                                                  ),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      model.endDateTo == null
                                                          ? 'Выберите дату'
                                                          : DateFormat(
                                                              'd MMM y ',
                                                            ).format(
                                                              model.endDateTo!,
                                                            ),
                                                    ),
                                                    // const SizedBox(
                                                    //   width: 15,
                                                    // ),
                                                    const Icon(
                                                      Icons.calendar_today,
                                                      color: Colors.blue,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          DefaultButton(
                            text: 'Искать санатории',
                            press: () async {
                              if (model.startDateTo == null) {
                                buildErrorCustomSnackBar(
                                  context,
                                  'Выберите дату',
                                );
                                return;
                              } else if (model.countAdults == 0) {
                                buildErrorCustomSnackBar(
                                  context,
                                  'Выберите количество людей',
                                );
                                return;
                              }
                              final int? cityId = await Prefs().getCityId();
                              if (cityId != null) {
                                if (!mounted) return;
                                BlocProvider.of<HomeCubit>(context)
                                    .getSanatories(
                                  cityId: cityId,
                                  countAdults: model.countAdults!,
                                  countChildren: model.countChildren!,
                                  startDate: model.startDateTo!,
                                  endDate: model.endDateTo!,
                                );
                              }
                            },
                            color: AppColors.orange,
                            width: double.infinity,
                          )
                        ],
                      ),
                    )
                  ],
                ),
                // ///
                ///////
                ///
                ///////
                ///
                ///
                ///
                ///
              ),
              searchedState: (sanatories) {
                return Scaffold(
                  backgroundColor: AppColors.backGroundColor,
                  appBar: AppBar(
                    title: sanatories.isNotEmpty
                        ? Text(' ${sanatories.length} санаториев по поиску')
                        : const Text('Санаториев по поиску не найден'),
                    leading: BackButton(
                      onPressed: () {
                        BlocProvider.of<HomeCubit>(context).goToInitialState();
                      },
                    ),
                  ),
                  body: SafeArea(
                    child: ListView(
                      children: [
                        SizedBox(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 13,
                              left: 8,
                              right: 8,
                            ),
                            child: Row(
                              children: [
                                Container(
                                  color: AppColors.backGroundColor,
                                  width:
                                      MediaQuery.of(context).size.width * 0.69,
                                  child: TextField(
                                    controller: _search,
                                    onChanged: (value) async {
                                      // if (_search.text.isEmpty) {
                                      //   final int? cityId =
                                      //       await Prefs().getCityId();
                                      //   if (cityId != null) {
                                      //     if (!mounted) return;
                                      //     BlocProvider.of<HomeCubit>(context)
                                      //         .getSanatories(
                                      //       cityId: cityId,
                                      //       countAdults: model.countAdults!,
                                      //       countChildren: model.countChildren!,
                                      //       startDate: model.startDateTo!,
                                      //       endDate: model.endDateTo!,
                                      //     );
                                      //   }
                                      // } else {
                                      //   BlocProvider.of<HomeCubit>(
                                      //     context,
                                      //   ).searchSanatori(search: value);
                                      // }
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      hintText: 'Поиск санаториев',
                                      contentPadding: const EdgeInsets.only(
                                        left: 16.0,
                                        bottom: 8.0,
                                        top: 8.0,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius: BorderRadius.circular(3),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: const BorderSide(
                                          color: Colors.white,
                                        ),
                                        borderRadius: BorderRadius.circular(3),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    BlocProvider.of<HomeCubit>(
                                      context,
                                    ).searchSanatori(search: _search.text);
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: Colors.white,
                                    ),
                                    padding: const EdgeInsets.all(14),
                                    child: const Text(
                                      'Искать',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: sanatories.length,
                          shrinkWrap: true,
                          separatorBuilder: (BuildContext context, int index) =>
                              const Divider(),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () {
                                print("tapped on container");
                                final String days1 =
                                    DateFormat('dd').format(model.startDateTo!);
                                // model.startDateTo.toString();;
                                final String days2 =
                                    DateFormat('dd').format(model.endDateTo!);
                                // model.startDateTo.toString();
                                final int days =
                                    int.parse(days2) - int.parse(days1);
                                log('DAYS::::: ${days}');
                                final String day = days.toString();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SanatoriumPage(
                                      sanatoriumId: sanatories[index].id,
                                      countAdults: model.countAdults,
                                      countChildren: model.countChildren,
                                      startDate: model.startDateTo,
                                      endDate: model.endDateTo,
                                      howDays: day,
                                    ),
                                  ),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(12.0),
                                      topRight: Radius.circular(12.0),
                                      bottomLeft: Radius.circular(8.0),
                                      bottomRight: Radius.circular(8.0),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black12,
                                        blurRadius: 1,
                                        spreadRadius: 1,
                                        offset: Offset(0, 4),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Container(
                                        height: 234,
                                        width: double.infinity,
                                        clipBehavior: Clip.antiAlias,
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(12.0),
                                            topRight: Radius.circular(12.0),
                                          ),
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                              '$SERVER_HOST/${sanatories[index].images!.first}',
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(9),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Flexible(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  ListTile(
                                                    title: Text(
                                                      sanatories[index].title!,
                                                      style: const TextStyle(
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                    subtitle: Text(
                                                      sanatories[index]
                                                          .address!,
                                                      style: const TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                    trailing: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        bottom: 20,
                                                      ),
                                                      child: Text(
                                                        'от ${sanatories[index].price ?? 'ERROR PRICE'} ₸',
                                                        style: const TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                );
              },
              loadedState: (sanatories) {
                return Scaffold(
                  backgroundColor: AppColors.backGroundColor,
                  appBar: AppBar(
                    title: sanatories.isNotEmpty
                        ? Text(' ${sanatories.length} санаториев по поиску')
                        : const Text('Санаториев по поиску не найден'),
                    leading: BackButton(
                      onPressed: () {
                        BlocProvider.of<HomeCubit>(context).goToInitialState();
                      },
                    ),
                  ),
                  body: sanatories.isEmpty
                      ? const Padding(
                          padding: EdgeInsets.all(28.0),
                          child: Center(
                            child: Text(
                              'Санаториев по поиску не найден, выберите другую дату',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        )
                      : SafeArea(
                          child: ListView(
                            children: [
                              SizedBox(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    top: 13,
                                    left: 8,
                                    right: 8,
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        color: AppColors.backGroundColor,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.69,
                                        child: TextField(
                                          controller: _search,
                                          onChanged: (value) async {
                                            // if (_search.text.isEmpty) {
                                            //   final int? cityId =
                                            //       await Prefs().getCityId();
                                            //   if (cityId != null) {
                                            //     if (!mounted) return;
                                            //     BlocProvider.of<HomeCubit>(context)
                                            //         .getSanatories(
                                            //       cityId: cityId,
                                            //       countAdults: model.countAdults!,
                                            //       countChildren: model.countChildren!,
                                            //       startDate: model.startDateTo!,
                                            //       endDate: model.endDateTo!,
                                            //     );
                                            //   }
                                            // } else {
                                            //   BlocProvider.of<HomeCubit>(
                                            //     context,
                                            //   ).searchSanatori(search: value);
                                            // }
                                          },
                                          decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.white,
                                            hintText: 'Поиск санаториев',
                                            contentPadding:
                                                const EdgeInsets.only(
                                              left: 16.0,
                                              bottom: 8.0,
                                              top: 8.0,
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                color: Colors.white,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(3),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: const BorderSide(
                                                color: Colors.white,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(3),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          BlocProvider.of<HomeCubit>(
                                            context,
                                          ).searchSanatori(
                                            search: _search.text,
                                          );
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(7),
                                            color: Colors.white,
                                          ),
                                          padding: const EdgeInsets.all(14),
                                          child: const Text(
                                            'Искать',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              ListView.separated(
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: sanatories.length,
                                shrinkWrap: true,
                                separatorBuilder:
                                    (BuildContext context, int index) =>
                                        const Divider(),
                                itemBuilder: (BuildContext context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      if (model.startDateTo == null) {
                                        buildErrorCustomSnackBar(
                                          context,
                                          'Выберите дату',
                                        );
                                      }
                                      print("tapped on container");
                                      final String days1 = DateFormat(
                                        'dd',
                                      ).format(model.startDateTo!);
                                      final String days2 = DateFormat(
                                        'dd',
                                      ).format(model.endDateTo!);
                                      final int days =
                                          int.parse(days2) - int.parse(days1);
                                      log('DAYS::::: ${days}');
                                      final String day = days.toString();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => SanatoriumPage(
                                            sanatoriumId: sanatories[index].id,
                                            countAdults: model.countAdults,
                                            countChildren: model.countChildren,
                                            startDate: model.startDateTo,
                                            endDate: model.endDateTo,
                                            howDays: day,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        decoration: const BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(12.0),
                                            topRight: Radius.circular(12.0),
                                            bottomLeft: Radius.circular(8.0),
                                            bottomRight: Radius.circular(8.0),
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 1,
                                              spreadRadius: 1,
                                              offset: Offset(0, 4),
                                            ),
                                          ],
                                        ),
                                        child: Column(
                                          children: [
                                            Container(
                                              height: 234,
                                              width: double.infinity,
                                              clipBehavior: Clip.antiAlias,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    const BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(12.0),
                                                  topRight:
                                                      Radius.circular(12.0),
                                                ),
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: NetworkImage(
                                                    '$SERVER_HOST/${sanatories[index].images!.first}',
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.all(9),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Flexible(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        ListTile(
                                                          title: Text(
                                                            sanatories[index]
                                                                .title!,
                                                            style:
                                                                const TextStyle(
                                                              fontSize: 20,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                          ),
                                                          subtitle: Text(
                                                            sanatories[index]
                                                                .address!,
                                                            style:
                                                                const TextStyle(
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                          trailing: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .only(
                                                              bottom: 20,
                                                            ),
                                                            child: Text(
                                                              'от ${sanatories[index].price ?? 'ERROR PRICE'} ₸',
                                                              style:
                                                                  const TextStyle(
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                );
              },
              orElse: () {
                return const Scaffold(
                  backgroundColor: Colors.white,
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
