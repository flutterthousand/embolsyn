// ignore_for_file: must_be_immutable, non_constant_identifier_names, require_trailing_commas

import 'package:embolsyn/data/cubit/auth_cubit/auth_cubit.dart';
import 'package:embolsyn/data/cubit/profile_cubit/profile_cubit.dart' as profile;
import 'package:embolsyn/data/prefs.dart';
import 'package:embolsyn/presentation/base.dart';
import 'package:embolsyn/presentation/screens/personal_account/document_page.dart';
import 'package:embolsyn/presentation/screens/personal_account/settings_page.dart';
import 'package:embolsyn/presentation/screens/profile/history_page.dart';
import 'package:embolsyn/presentation/screens/profile/notification_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final Prefs _prefs = Prefs();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<profile.ProfileCubit>(context).getProfile();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return BlocBuilder<profile.ProfileCubit, profile.ProfileState>(
      builder: (context, state) {
        if (state is profile.LoadedState) {
          return Scaffold(
            appBar: AppBar(
              title: const Text(
                'Личный кабинет',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
            body: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.grey.withOpacity(0.2),
                                ),
                                height: 70,
                                width: 70,
                                child: Center(
                                  child: Text(
                                    state.user.name![0].toUpperCase(),
                                    // state.user.surname[0].toUpperCase(),
                                    style: const TextStyle(
                                      fontSize: 34,
                                      fontWeight: FontWeight.w100,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${state.user.name!} ${state.user.surname!}',
                                    style: TextStyle(
                                        color: Colors.grey.shade900, fontSize: 16, fontWeight: FontWeight.w600),
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    state.user.phone,
                                    style: const TextStyle(
                                        color: Color(0xFFFB6514), fontSize: 14, fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          ListileCustom(
                            title: 'История заказов',
                            icon: SvgPicture.asset(
                              'assets/svg/Vector.svg',
                              height: 25,
                            ),
                            ontap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const HistoryPage(),
                                ),
                              );
                            },
                          ),
                          const Divider(),
                          ListileCustom(
                            title: 'Уведомления',
                            icon: SvgPicture.asset(
                              'assets/svg/vec2.svg',
                              height: 25,
                            ),
                            ontap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const NotificationPage(),
                                ),
                              );
                            },
                          ),
                          const Divider(),
                          ListileCustom(
                            title: 'Документы',
                            icon: SvgPicture.asset(
                              'assets/svg/vec3.svg',
                              height: 25,
                            ),
                            ontap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DocumentPage(),
                                ),
                              );
                            },
                          ),
                          const Divider(),
                          ListileCustom(
                            title: 'Настройки',
                            icon: SvgPicture.asset(
                              'assets/svg/vec4.svg',
                              height: 25,
                            ),
                            ontap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SettingsPage(),
                                ),
                              );
                            },
                          ),
                          const Divider(),
                          ListileCustom(
                            title: 'Выйти',
                            icon: SvgPicture.asset(
                              'assets/svg/vec5.svg',
                              height: 25,
                            ),
                            ontap: () async {
                              await BlocProvider.of<AuthCubit>(context).logOut();
                              if (!mounted) return;
                              // Navigator.of(context).pop();
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Base(
                                    index: 3,
                                  ),
                                ),
                                (route) => false,
                              );
                            },
                          ),
                          const Divider(),
                          ListTile(
                            title: const Text(
                              'Удалить аккаунт',
                              style: TextStyle(fontSize: 16),
                            ),
                            leading: SvgPicture.asset(
                              'assets/svg/delete.svg',
                              height: 25,
                            ),
                            onTap: () async {
                              await BlocProvider.of<AuthCubit>(context).delete();
                              if (!mounted) return;
                              // Navigator.of(context).pop();
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Base(
                                    index: 0,
                                  ),
                                ),
                                (route) => false,
                              );
                            },
                          ),
                          const Divider(),
                        ],
                      ),
                      ButtonToChats(screenSize, 'Написать администрации', Colors.green),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else {
          return const Scaffold(
            backgroundColor: Colors.white10,
            body: Padding(
              padding: EdgeInsets.all(80),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
      },
    );
  }

  InkWell ButtonToChats(Size screenSize, String text, Color color) {
    return InkWell(
      onTap: () => launch('https://wa.me/+7071743494'),
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: const BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        margin: EdgeInsets.symmetric(
          vertical: screenSize.height * 0.02,
          horizontal: screenSize.width * 0.02,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: screenSize.width * 0.08,
        ),
        child: ListTile(
          leading: const Icon(
            FontAwesomeIcons.whatsapp,
            color: Colors.white,
          ),
          title: Text(
            text,
            style: const TextStyle(
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}

class ListileCustom extends StatelessWidget {
  final String title;
  Widget icon;
  final Function() ontap;
  ListileCustom({required this.title, required this.ontap, required this.icon});

  @override
  Widget build(
    BuildContext context,
  ) {
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(fontSize: 16),
      ),
      leading: icon,
      trailing: SvgPicture.asset(
        'assets/svg/vec6.svg',
        height: 14,
      ),
      onTap: ontap,
    );
  }
}

Divider divider() {
  return const Divider(
    height: 10,
    thickness: 3,
    indent: 20,
    endIndent: 20,
    color: Color(0xFFE4E4E4),
  );
}
