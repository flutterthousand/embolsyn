import 'package:embolsyn/config.dart';
import 'package:embolsyn/presentation/screens/personal_account/politic_page.dart';
import 'package:flutter/material.dart';

class DocumentPage extends StatefulWidget {
  const DocumentPage({super.key});

  @override
  State<DocumentPage> createState() => _DocumentPageState();
}

class _DocumentPageState extends State<DocumentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backGroundColor,
      appBar: AppBar(
        title: const Text('Документы'),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 18, top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTileCustom(
              text: 'Политика конфиденциальности',
              ontap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PoliticPage(),
                  ),
                );
              },
            ),
            // ListTileCustom(
            //   text: 'Условия использования',
            //   ontap: () {},
            // ),
            // ListTileCustom(
            //   text: 'Публичная оферта',
            //   ontap: () {},
            // ),
            // ListTileCustom(
            //   text: 'Партнерское соглашение',
            //   ontap: () {},
            // ),
          ],
        ),
      ),
    );
  }
}

class ListTileCustom extends StatelessWidget {
  final String text;
  final Function() ontap;
  const ListTileCustom({
    super.key,
    required this.ontap,
    required this.text,
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 20),
      child: InkWell(
        onTap: ontap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: const TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
            ),
            const Icon(
              Icons.arrow_forward_ios,
              size: 18,
              color: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
}
