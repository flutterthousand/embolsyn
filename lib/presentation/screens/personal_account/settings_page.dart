import 'package:embolsyn/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _switchValue = true;
  bool _switchValue2 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backGroundColor,
      appBar: AppBar(
        title: const Text('Настройки'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     const Text(
            //       'Язык',
            //       style: TextStyle(
            //         color: Colors.black,
            //         fontSize: 18,
            //         fontWeight: FontWeight.w400,
            //       ),
            //     ),
            //     Row(
            //       children: [
            //         Text(
            //           'Русский',
            //           style: TextStyle(
            //             color: Colors.grey.shade500,
            //             fontSize: 16,
            //             fontWeight: FontWeight.w400,
            //           ),
            //         ),
            //         const SizedBox(
            //           width: 10,
            //         ),
            //         const Icon(
            //           Icons.arrow_forward_ios,
            //           size: 16,
            //           color: Colors.black,
            //         )
            //       ],
            //     )
            //   ],
            // ),
            // Divider(
            //   color: Colors.grey.shade400,
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset('assets/svg/noti.svg'),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      'Уведомления',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                Switch(
                  inactiveTrackColor: Colors.black,
                  activeTrackColor: Colors.black,
                  value: _switchValue,
                  onChanged: (value) {
                    setState(() {
                      _switchValue = value;
                    });
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     Row(
            //       children: [
            //         SvgPicture.asset('assets/svg/moon.svg'),
            //         const SizedBox(
            //           width: 10,
            //         ),
            //         const Text(
            //           'Ночной режим',
            //           style: TextStyle(
            //             color: Colors.black,
            //             fontSize: 16,
            //             fontWeight: FontWeight.w600,
            //           ),
            //         ),
            //       ],
            //     ),
            //     Switch(
            //       inactiveTrackColor: Colors.black,
            //       activeTrackColor: Colors.black,
            //       value: _switchValue2,
            //       onChanged: (value) {
            //         setState(() {
            //           _switchValue2 = value;
            //         });
            //       },
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
