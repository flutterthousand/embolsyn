import 'dart:convert';

import 'package:embolsyn/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PoliticPage extends StatefulWidget {
  const PoliticPage({super.key});

  @override
  State<PoliticPage> createState() => _PoliticPageState();
}

class _PoliticPageState extends State<PoliticPage> {
  late WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backGroundColor,
      appBar: AppBar(
        title: const Text('Документы'),
      ),
      body: WebView(
        initialUrl: 'about:blank',
        onWebViewCreated: (WebViewController webViewController) {
          _controller = webViewController;
          _loadHtmlFromAssets();
        },
      ),
    );
  }

  Future<void> _loadHtmlFromAssets() async {
    final String fileText = await rootBundle.loadString('assets/files/politic.html');
    _controller.loadUrl(
      Uri.dataFromString(
        fileText,
        mimeType: 'text/html',
        encoding: Encoding.getByName('utf-8'),
      ).toString(),
    );
  }
}
