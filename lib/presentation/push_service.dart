import 'dart:developer';

import 'package:embolsyn/presentation/base.dart';
import 'package:embolsyn/presentation/screens/profile/notification_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushNoti {

void listen(BuildContext context) {
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage event) {
      if (event.data['type'] == '1') {
        log('TYPE:::${event.data}');
        //type : offer || destroy
        //  try {
        //   int? id = int.tryParse(event.data['type'].toString());
        //   log('##### initState::: type - $id', name: _tag);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const NotificationPage()),
        );
        // BlocProvider.of<AppRoleCubit>(context).changeToClientMyOrders();
        // } catch (e) {
        //   log('$e', name: _tag);
        // }
      } else {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => const Base(
              index: 2,
            ),
          ),
          (route) => false,
        );
        // BlocProvider.of<AppRoleCubit>(context).changeToPerformerHistory();
        // try {
        //   int? id = int.tryParse(event.data['type'].toString());
        //   log('##### initState::: type - $id', name: _tag);

        // } catch (e) {
        //   log('$e', name: _tag);
        // }
      }
    });
  }
}
