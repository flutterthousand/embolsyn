// ignore_for_file: avoid_print

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/navigation/navigation_cubit.dart';
import 'package:embolsyn/presentation/screens/auth/auth_page.dart';
import 'package:embolsyn/presentation/screens/contact_page.dart';
import 'package:embolsyn/presentation/screens/home_page.dart';
import 'package:embolsyn/presentation/screens/hot_deals_page.dart';
import 'package:embolsyn/presentation/screens/personal_account/profile__page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Base extends StatefulWidget {
  final int? index;
  const Base({this.index, Key? key}) : super(key: key);

  @override
  _BaseState createState() => _BaseState();
}

class _BaseState extends State<Base> {
  @override
  void initState() {
    if (widget.index != null) {
      basePageIndex = widget.index!;

      if (basePageIndex == 0) {
        BlocProvider.of<NavigationCubit>(context)
            .getNavBarItem(const NavigationState.home());
      } else if (basePageIndex == 3) {
        BlocProvider.of<NavigationCubit>(context)
            .getNavBarItem(const NavigationState.profile());
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: BlocBuilder<NavigationCubit, NavigationState>(
        builder: (context, state) {
          return BottomNavigationBar(
            currentIndex: basePageIndex,
            onTap: (int index) {
              switch (index) {
                case 0:
                  BlocProvider.of<NavigationCubit>(context)
                      .getNavBarItem(const NavigationState.home());
                  break;
                case 1:
                  BlocProvider.of<NavigationCubit>(context)
                      .getNavBarItem(const NavigationState.hotDeal());
                  break;
                case 2:
                  BlocProvider.of<NavigationCubit>(context)
                      .getNavBarItem(const NavigationState.contact());
                  break;
                case 3:
                  BlocProvider.of<NavigationCubit>(context)
                      .getNavBarItem(const NavigationState.profile());
                  break;
              }
              print(index);
              setState(() {
                basePageIndex = index;
              });
            },
            selectedItemColor: AppColors.darkColor,
            unselectedItemColor: const Color(0xFF99A2AD),
            selectedFontSize: 12,
            elevation: 4,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            items: [
              BottomNavigationBarItem(
                icon: const Icon(Icons.home),
                label: '',
                activeIcon: ClipOval(
                  child: Material(
                    color: AppColors.darkColor, // Button color
                    child: const SizedBox(
                      width: 42,
                      height: 42,
                      child: Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.local_fire_department),
                label: '',
                activeIcon: ClipOval(
                  child: Material(
                    color: AppColors.darkColor, // Button lor
                    child: const SizedBox(
                      width: 42,
                      height: 42,
                      child: Icon(
                        Icons.local_fire_department,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.phone),
                label: '',
                activeIcon: ClipOval(
                  child: Material(
                    color: AppColors.darkColor, // Button , // Button color
                    child: const SizedBox(
                      width: 42,
                      height: 42,
                      child: Icon(
                        Icons.phone,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.account_circle),
                label: '',
                activeIcon: ClipOval(
                  child: Material(
                    color: AppColors.darkColor, // Button , // Button color
                    child: const SizedBox(
                      width: 42,
                      height: 42,
                      child: Icon(
                        Icons.account_circle,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
      body: BlocBuilder<NavigationCubit, NavigationState>(
        builder: (context, state) {
          if (state is HomeState) {
            return const HomePage();
          } else if (state is HotDealState) {
            return const HotDealsPage();
          } else if (state is ContactState) {
            return const ContactPage();
          } else if (state is ProfileState) {
            return const ProfilePage();
          } else if (state is AuthState) {
            return AuthPage();
          }
          return Container();
        },
      ),
    );
  }
}
