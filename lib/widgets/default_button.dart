// ignore_for_file: always_use_package_imports, prefer_if_null_operators, unnecessary_null_comparison

import 'package:flutter/material.dart';

import '../config.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    required this.text,
    required this.press,
    required this.color,
    required this.width,
  });
  final String text;
  final Function? press;
  final Color color;
  final double width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width == null ? null : width,
      height: 50,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          backgroundColor: color == null ? AppColors.orange : color,
        ),
        onPressed: press as void Function()?,
        child: Text(
          text,
          style: const TextStyle(
            fontFamily: 'Inter',
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
