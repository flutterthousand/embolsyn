import 'dart:developer';
import 'dart:io';

import 'package:embolsyn/config.dart';
import 'package:embolsyn/data/cubit/advertisement_cubit/advertisement_cubit.dart';
import 'package:embolsyn/data/cubit/auth_cubit/auth_cubit.dart';
import 'package:embolsyn/data/cubit/auth_verify_cubit/auth_verify_cubit.dart';
import 'package:embolsyn/data/cubit/banner_cubit/banner_cubit.dart';
import 'package:embolsyn/data/cubit/book_cubit/book_cubit.dart';
import 'package:embolsyn/data/cubit/cities_cubit/cities_cubit.dart';
import 'package:embolsyn/data/cubit/contact_cubit/contact_cubit.dart';
import 'package:embolsyn/data/cubit/history_cubit/history_cubit.dart';
import 'package:embolsyn/data/cubit/home_cubit/home_cubit.dart';
import 'package:embolsyn/data/cubit/navigation/navigation_cubit.dart';
import 'package:embolsyn/data/cubit/notification_cubit/notification_cubit.dart';
import 'package:embolsyn/data/cubit/profile_cubit/profile_cubit.dart';
import 'package:embolsyn/data/cubit/show_sanatories_cubit/show_sanatories_cubit.dart';
import 'package:embolsyn/data/prefs.dart';
import 'package:embolsyn/data/provider/reserve_provider.dart';
import 'package:embolsyn/data/repositories/advertising_repositories.dart';
import 'package:embolsyn/data/repositories/auth_repository.dart';
import 'package:embolsyn/data/repositories/book_room.dart';
import 'package:embolsyn/data/repositories/city_repositories.dart';
import 'package:embolsyn/data/repositories/contact_repositories.dart';
import 'package:embolsyn/data/repositories/history_repository.dart';
import 'package:embolsyn/data/repositories/notification.dart';
import 'package:embolsyn/data/repositories/show_sanatories_repository.dart';
import 'package:embolsyn/presentation/base.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart' as fcm;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_api_availability/google_api_availability.dart';
import 'package:huawei_push/huawei_push.dart' as push;
import 'package:provider/provider.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  description:
      'This channel is used for important notifications.', // description
  importance: Importance.high,
  // playSound: true,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(
  message,
) async {
  await Firebase.initializeApp();
  log('A bg message just showed up: ${message.messageId}');
}
//388564dd-3dc2-4074-9393-5fb371b008ed

Future<void> getAAID() async {
  // final String? result = await push.Push.getAAID();
  final String? result1 = await push.Push.getId();
  final String creationTime = await push.Push.getCreationTime();
  push.Push.getToken(result1!);

  log('HUAWEI DEVICE AAID::::: $result1, CREATION TIME:: $creationTime');
}

final Prefs _prefs = Prefs();
Future<String?> getDeviceToken() async {
  final String? deviceToken = await fcm.FirebaseMessaging.instance.getToken();
  _prefs.saveDeviceToken(deviceToken.toString());
  log('deviceToken ::: $deviceToken');
  return deviceToken;
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GooglePlayServicesAvailability playStoreAvailability;
  try {
    playStoreAvailability = await GoogleApiAvailability.instance
        .checkGooglePlayServicesAvailability();
  } on PlatformException {
    playStoreAvailability = GooglePlayServicesAvailability.unknown;
  }

  if (playStoreAvailability ==
          GooglePlayServicesAvailability
              .success || //FIXME && <- we use when testing huawei device
      Platform.isIOS) {
    await Prefs().saveDeviceType(deviceType: false);
    await Firebase.initializeApp();

    if (Platform.isIOS) {
      final NotificationService notificationService = NotificationService();
      notificationService.init();
      await getDeviceToken();
    }
    if (Platform.isAndroid) {
      fcm.FirebaseMessaging.onBackgroundMessage(
        _firebaseMessagingBackgroundHandler,
      );

      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      await fcm.FirebaseMessaging.instance.requestPermission();

      await fcm.FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );
    }
    final fltNotification = FlutterLocalNotificationsPlugin();
    FirebaseMessaging.onMessage.listen((RemoteMessage event) async {
      final RemoteNotification? notification = event.notification;

      final AndroidNotification? android =
          Platform.isAndroid ? event.notification?.android : null;

      if (notification != null) {
        if (Platform.isAndroid && android != null) {
          fltNotification.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                icon: '@mipmap/ic_launcher',
              ),
            ),
          );
        }
      }
    });

    await getDeviceToken();
  } else {
    //######
    //######
    //######
    //######
    //######
    //###### PUSHKIT
    //######
    //######
    //######
    await Prefs().saveDeviceType(deviceType: true);

    await getAAID();
  }
  //####
  //######
  //######
  //#### FIREBASE SETUP END
  //####
  //######
  //######

  // WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();

  // final NotificationService notificationService = NotificationService();
  // await notificationService.init();
  // await getDeviceToken();

  runApp(MyApp(playStoreAvailability: playStoreAvailability));
}

class MyApp extends StatefulWidget {
  final GooglePlayServicesAvailability playStoreAvailability;
  const MyApp({required this.playStoreAvailability});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ///
  ///
  ///
  ///
  ///
  ///    HUAWEI PUSHKIT
  ///
  ///
  ///
  ///
  String _token = '';
  void _onTokenEvent(String event) {
    // Requested tokens can be obtained here
    _token = event;
    log("TokenEvent: $_token");
    if (_token != '') _saveToken(_token);
  }

  Future<void> _saveToken(String event) async {
    // final prefs = await SharedPreferences.getInstance();
    log('token from prefs saveHuaweiDeviceToken:: $event');
    await Prefs().saveDeviceToken(event);

    // prefs.setString('deviceToken', event);
  }

  void _onTokenError(Object error) {
    // PlatformException e = error;
    log("_onTokenError:: TokenErrorEvent: error $error");
  }

  void _onMessageReceiveError(Object error) {
    // Called when an error occurs while receiving the data message
    final String data = error.toString();
    log('_onMessageReceiveError:: log from main.dart:: $data');
  }

  void _onRemoteMessageSendStatus(String event) {
    log("_onRemoteMessageSendStatus:: RemoteMessageSendStatus, Status: $event");
  }

  void _onRemoteMessageSendError(Object error) {
    final PlatformException e = error as PlatformException;
    log("_onRemoteMessageSendError:: RemoteMessageSendError, Error: $e");
  }

  void _onNewIntent(String? intentString) {
    // For navigating to the custom intent page (deep link) the custom
    // intent that sent from the push kit console is:
    // app://app2
    // intentString = intentString ?? '';
    if (intentString != null) {
      log('_onNewIntent:: CustomIntentEvent: $intentString');
      // List parsedString = intentString.split("://");
      // if (parsedString[1] == "app2") {
      //   SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
      //     Navigator.of(context).push(
      //         MaterialPageRoute(builder: (context) => CustomIntentPage()));
      //   });
      // }
    } else {
      log('_onNewIntent:: intentString is null');
    }
  }

  void _onIntentError(Object err) {
    final PlatformException e = err as PlatformException;
    log("Error on intent stream: $e");
  }

  Future<void> initPlatformState() async {
    if (!mounted) return;
    push.Push.getTokenStream.listen(_onTokenEvent, onError: _onTokenError);
    push.Push.getIntentStream.listen(_onNewIntent, onError: _onIntentError);
    // push.Push.onNotificationOpenedApp.listen(_onNotificationOpenedApp);
    // final initialNotification = await push.Push.getInitialNotification();
    // _onNotificationOpenedApp(initialNotification);
    final String? intent = await push.Push.getInitialIntent();
    _onNewIntent(intent);
    // push.Push.onMessageReceivedStream
    //     .listen(_onMessageReceived, onError: _onMessageReceiveError);
    push.Push.getRemoteMsgSendStatusStream
        .listen(_onRemoteMessageSendStatus, onError: _onRemoteMessageSendError);
    // final bool backgroundMessageHandler =
    //     await push.Push.registerBackgroundMessageHandler(
    //   backgroundMessageCallback,
    // );
    // log("backgroundMessageHandler registered: $backgroundMessageHandler");
  }

  //  PushNoti? pushNoti;

  @override
  void initState() {
    if (widget.playStoreAvailability ==
            GooglePlayServicesAvailability
                .success || //FIXME && <- we use when testing huawei device
        Platform.isIOS) {
    } else {
      initPlatformState();
    }
    // pushNoti!.listen(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => AuthCubit(authRepository: AuthRepository()),
        ),
        BlocProvider(
          create: (_) => AuthVerifyCubit(authRepository: AuthRepository()),
        ),
        BlocProvider(create: (_) => NavigationCubit()),
        BlocProvider(create: (_) => ProfileCubit()),
        BlocProvider(
          create: (_) => CitiesCubit(cityRepositories: CityRepositories()),
        ),
        BlocProvider(
          create: (_) => ShowSanatoriesCubit(
            sanatoriesRepository: ShowSanatoriesRepository(),
          ),
        ),
        BlocProvider(
          create: (_) =>
              ContactCubit(contactRepositories: ContactRepositories()),
        ),
        BlocProvider(
          create: (_) => HomeCubit(
            sanatoriesRepository: ShowSanatoriesRepository(),
          ),
        ),
        BlocProvider(
          create: (_) => BannerCubit(
            sanatoriesRepository: ShowSanatoriesRepository(),
          ),
        ),
        BlocProvider(
          create: (_) =>
              HistoryCubit(historyRepositories: HistoryRepositories()),
        ),
        BlocProvider(
          create: (_) => NotificationCubit(
            notificationRepository: NotificationRepository(),
          ),
        ),
        BlocProvider(
          create: (_) => AdvertisementCubit(
            advertisingRepository: AdvertisingRepository(),
          ),
        ),
        BlocProvider(
          create: (_) => BookCubit(bookRepository: BookRepository()),
        ),
      ],
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<ReserveProvider>(
            create: (context) => ReserveProvider(),
          ),
          //  ChangeNotifierProvider<HomePageProvider>(
          //   create: (context) => HomePageProvider(),

          // ),
        ],
        child: MaterialApp(
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [Locale('en'), Locale('ru')],
          locale: const Locale('ru'),
          title: 'embolsyn',
          // home: App(),
          home: const Base(),
          theme: ThemeData(
            fontFamily: 'Raleway',
            backgroundColor: Colors.white,
            appBarTheme: AppBarTheme(
              backgroundColor: AppColors.darkColor,
              centerTitle: true,
              titleTextStyle:
                  const TextStyle(color: Colors.white, fontSize: 18),
            ),
            bottomNavigationBarTheme: const BottomNavigationBarThemeData(
              backgroundColor: Colors.white,
            ),
          ),
          debugShowCheckedModeBanner: false,
        ),
      ),
    );
  }
}
